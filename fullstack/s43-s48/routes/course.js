const express = require("express");
const courseController = require("../controllers/course.js");

const auth = require("../auth.js");

// destructing of verify and verifyAdmin

const {verify, verifyAdmin} = auth;

const router = express.Router();

// Create a course POST
router.post("/", verify, verifyAdmin, courseController.addCourse);

// Get all courses
router.get("/all", courseController.getAllCourses);

// Get all "active" course
router.get("/", courseController.getAllActive);

// Get 1 specific course using its ID
router.get("/:courseId", courseController.getCourse);

// Updating a Course (Admin Only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// Archive a course
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

// Handle the PUT request to archive a course
// Handle the PUT request to archive a course
// router.put("/:courseId/archive", verify, verifyAdmin, async (req, res) => {
//   try {
//     const courseId = req.params.courseId;
//     console.log("Received courseId:", courseId); // Log the received courseId for debugging

//     // Check if courseId is a valid ObjectId
//     if (!mongoose.isValidObjectId(courseId)) {
//       return res.status(400).send("Invalid ObjectId");
//     }

//     // Pass the courseId to the controller for processing
//     const result = await courseController.archiveCourse(courseId);

//     if (result) {
//       return res.send(true);
//     } else {
//       return res.send(false);
//     }
//   } catch (error) {
//     console.error("Error in archiveCourse route handler:", error);
//     return res.status(500).send(error); // Handle errors gracefully
//   }
// });


router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

router.post("/archives", verify, verifyAdmin, courseController.archives);

module.exports = router;