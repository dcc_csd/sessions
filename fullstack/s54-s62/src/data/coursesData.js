const coursesData = [
    {
    	id : "wdc001",
    	name: "PHP - Laravel",
    	description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut gravida libero. Aenean fermentum diam arcu, ut sodales quam rhoncus non. Maecenas pulvinar, lectus quis fringilla molestie, dolor odio lacinia dui, quis ultrices ex est non turpis. Quisque erat magna, imperdiet quis placerat eu, scelerisque et libero.",
    	price: 45000,
    	onOffer: true
    },
    {
    	id : "wdc002",
    	name: "Pyton - Dyango",
    	description: "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec in felis molestie, ornare augue at, lobortis turpis. Suspendisse sit amet faucibus nulla. Praesent elit ante, vestibulum ut tempus ac, lacinia eu dui. Donec elementum elementum lectus, vitae fringilla ante aliquam et. In hac habitasse platea dictumst. In hac habitasse platea dictumst. Praesent pharetra convallis ligula, nec vehicula tellus auctor eu. ",
    	price: 50000,
    	onOffer: true
    },    
    {
    	id : "wdc003",
    	name: "Java - Springboot",
    	description: "Nulla ac lacus et arcu porta cursus eget vel mi. Vivamus laoreet ultrices orci. Phasellus eu ipsum felis. Curabitur at enim a orci rhoncus scelerisque. Duis porta augue neque, sed porta libero mollis in. Ut vel mauris posuere, iaculis libero sit amet, bibendum justo. Fusce ultrices libero lacus, in finibus magna feugiat ut",
    	price: 55000,
    	onOffer: true
    },    


	]

export default coursesData;