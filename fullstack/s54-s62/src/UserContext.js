import React from 'react';
// import {createContext} from 'react';

//Creates a context object for sharing between components
const UserContext = React.createContext();

//UserProvide allow to share data to User Context, for components/pages to use.

export const UserProvider = UserContext.Provider;

export default UserContext;