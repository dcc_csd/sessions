import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import Home from './pages/Home';
import UserProfile from './components/UserProfile';

// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

//BrowserRouter, Routes , Routes word together to setup components endpoints - React-Dom
import {BrowserRouter as Router } from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';

//Bootstrap
import {Container} from 'react-bootstrap';

import './App.css';
import Register from './pages/Register';
import CourseView from './pages/CourseView';
import AdminView from './pages/AdminView';




import Login from './pages/Login';
import Logout from './pages/Logout';


import {UserProvider} from './UserContext';

// import Pagenotfound from './pages/Pagenotfound';

function App() {

//Gets the token from local storage and assigns the tolem to user state

 const[user, setUser] = useState({
  // token: localStorage.getItem('token')
    id: null,
    isAdmin: null,
    access: localStorage.getItem('token') || null,
});

//unsetUser is a function for clearing local storage
const unsetUser =() => {
  localStorage.clear()
}

//Check if user info is properly stored upon login and localStorage if cleared upon logout

  useEffect(() => {
  console.log(user);
  console.log(localStorage);
   
 }, [user]) 


  return (
   
 //The user state, setuUser (setter function) and unsetUser function is provided in the UerContext to be shared to other pages/compone.

   <UserProvider value ={{user, setUser, unsetUser}}>
    <Router>
      <Container>
        <AppNavbar />
            <Routes>
              
                  <Route path="/" element ={<Home />} />
                  <Route path="/courses" element ={<Courses />} />
                  <Route path="/login" element ={<Login />} />
                  <Route path="/register" element ={<Register />} />
                  <Route path="/courses/:courseId" element ={<CourseView />} />
                  <Route path="/adminView" element ={<AdminView />} />
                  <Route path="/logout" element ={<Logout />} />
                  <Route path="/*" element ={<Home />} />
                  <Route path ="/profile" element ={<UserProfile />} />
              
            </Routes>

      </Container>
    
    </Router>
    </UserProvider>
    /*// <>
    //   <AppNavbar />

    //   <Container>
        
    //     <Home />
    //     <Courses />
    //     <Register />
    //     <Login />
        
    //   </Container>

    // </>*/
    
  )
};

export default App;
