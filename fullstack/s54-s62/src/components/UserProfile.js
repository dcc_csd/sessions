//import {Row, Col} from 'react-bootstrap';
// import React, {useContext} from 'react';

import React, { useEffect, useState } from 'react';

export default function UserProfile() {
  const [userData, setUserData] = useState({});
  const [isUpdate, setIsUpdating] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');

  useEffect(() => {
    // kunin ang token
    const token = localStorage.getItem('token');

    if (token) {
      fetch('http://localhost:4000/users/details', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUserData(data);
          setFirstName(data.firstName);
          setLastName(data.lastName);
          setEmail(data.email);
          setMobileNo(data.mobileNo);
        });
    }
  }, []);

  const handleEditClick = () => {
    setIsUpdating(true);
  };

  const handleCancelClick = () => {
    setIsUpdating(false);
    setFirstName(userData.firstName);
    setLastName(userData.lastName);
    setEmail(userData.email);
    setMobileNo(userData.mobileNo);
  };

  const handleSaveClick = () => {
  //nakakabaliw ito
  };

  return (
    <div className="bg-primary text-white p-5 align-self-center">
      <h1>Profile</h1>
      {!isUpdate ? (
        <>
          <h3 className="pt-5 mt-2 pb-3 border-white border-bottom">
            {userData.firstName} {userData.lastName}
          </h3>
          <div className="pt-2">
            <h4>Contacts</h4>
            <ul className="pt-2">
              <li>
                <p>Email: {userData.email}</p>
              </li>
              <li>
                <p>Mobile No: {userData.mobileNo}</p>
              </li>
            </ul>
          </div>
          <button onClick={handleEditClick}>Edit Profile</button>
        </>
      ) : (
        <form>
          <div>
            <label htmlFor="firstName">First Name:</label>
            <input
              type="text"
              id="firstName"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="lastName">Last Name:</label>
            <input
              type="text"
              id="lastName"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="mobileNo">Mobile No:</label>
            <input
              type="text"
              id="mobileNo"
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
            />
          </div>
          <button type="button" onClick={handleCancelClick}>
            Cancel
          </button>
          <button type="button" onClick={handleSaveClick}>
            Save
          </button>
        </form>
      )}
    </div>
  );
}

