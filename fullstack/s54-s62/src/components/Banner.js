// import Button from 'react-bootstrap/Button';

//Bootstrap grid system components

// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col'; 


// import{ Button, Row, Col } from 'react-bootstrap';

// export default function Banner(){
// 	return (
//  	<Row>
//  		<Col className = "p-5 text-center">
//  			<h1> Zuitt Coding Bootcamp</h1>
//  			<p> Opportunities for everyone, everywhere </p>
//  			<Button variant ="primary"> Enroll now!</Button>
//  		</Col>
//  	</Row>

// 		)
// }


import { Button, Row, Col } from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';


export default function Banner({ showError }) {

   const navigate = useNavigate();

   const backToHomePage = () => {
   	navigate('/');
   };

   const goToCoursePage = () => {
   	navigate('/courses');
   };


  return (
    <Row>
      <Col className="p-5 text-center">
        {showError ? (
          <>
            <h1>404 - Not Found</h1>
            <p>The page you are looking for cannot be found</p>
            <Button variant="primary" type="submit" onClick={backToHomePage}>Back Home</Button>
          </>
        ) : (
          <>
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere</p>
            <Button variant="primary" type="submit" onClick={goToCoursePage}>Enroll now!</Button>
          </>
        )}
      </Col>
    </Row>
  );
}
