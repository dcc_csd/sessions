import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext'; 
import {Navigate} from 'react-router-dom';
export default function Register(){
    
    //State hooks to store values of the input fields
    //Syntax: [statevariable, setterFunction] =  useState(assigned state/initial state)
    //Note: setterFunction is to update the state value
    const{user} = useContext(UserContext); 
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(confirmPassword);
    console.log(isActive);

    function registerUser(event){
        //prevents the default behaviour during the submission which is page redirection via form submission
    	event.preventDefault();
    	
    	fetch('http://localhost:4000/users/register',{
           method: "POST",
           headers: {
           	  "Content-Type" : "application/json"
           },
           body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo:mobileNo,
                password: password
           })
    	})
    	.then(res => res.json())
    	.then(data => {
    		/* data is the response from the web service*/
    		//checks the value of response
    		console.log(data);

         //if the received response is tre states will be empty and the iser will received a message'thankyou for registering'
    	 //else, if not receive response is true, the user will received a message "Please try again later"

    		if(data){
               setFirstName ('');
               setLastName ('');
               setEmail ('');
               setPassword ('');
               setConfirmPassword ('');

               alert('Thank you for registering!')
    		}
    		else{
    			alert("Please try again later.")
    		}
    	})

    }

    //The useEffect will run evrytime the state changes in any of the filed or values listed in [firstName, lastName, email, mobileNo, password, confirmPassword]

    useEffect(() => {

      if((firstName!=="" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" & confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
    		//sets the is active value to true to enable the button
    		setIsActive(true);
    	}
    	else{
    		setIsActive(false);
    	}
    
    }, [firstName, lastName, email, mobileNo, password, confirmPassword])
    

     /*
        fistname = "Glenn";
        setFirstName ("Glenn Wil");
        new value of fistName : Glenn Wil
     */

	return(
       
      (user.id !== null) ?
      <Navigate to= "/courses" /> 
      :
		<Form onSubmit ={(event) => registerUser (event)}>
		<h1 className="my-5 text-center">Register</h1>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				{/* Two-way binding*/}
				<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}/>
			</Form.Group>
			
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}/>
			</Form.Group>
			
            {/* Conditional Rendering:
                
                - if isActive is true - enabled button
                - else, disabled button
            */}
            {
                isActive? 
                <Button variant="primary" type="submit">Submit</Button>
                :
                <Button variant="primary" type="submit" disabled>Submit</Button>
            }

		</Form>
		)
}