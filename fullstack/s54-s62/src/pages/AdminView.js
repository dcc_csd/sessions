// import React, { useState, useEffect } from 'react';
// import { Table } from 'react-bootstrap';
// import EditCourse from '../components/EditCourse';
// import ArchiveCourse from '../components/ArchiveCourse';

// export default function AdminView({ coursesData }) {
//   const [courses, setCourses] = useState([]);

//   useEffect(() => {
//     // Fetch courses from your MongoDB backend
//     fetch('http://localhost:4000/courses') // Adjust the URL to your backend
//       .then((response) => response.json())
//       .then((data) => setCourses(data))
//       .catch((error) => console.error('Error fetching courses:', error));
//   }, []);

//   const handleArchiveCourse = (courseId) => {
//     // Send a request to your backend to archive the course with the given courseId
//     fetch(`http://localhost:4000/courses/${courseId}/archive`, {
//       method: 'PUT',
//       headers: {
//         Authorization: `Bearer ${localStorage.getItem('token')}`,
//         'Content-Type': 'application/json',
//       },
//     })
//       .then((res) => res.json())
//       .then((data) => {
//         if (data) {
//           // Course archived successfully
//           // You can update the coursesData or display a success message
//           // Reload the coursesData or manage it accordingly
//           // After archiving, you should update the courses list
//           setCourses((prevCourses) =>
//             prevCourses.map((course) =>
//               course._id === courseId
//                 ? { ...course, isActive: false }
//                 : course
//             )
//           );
//         } else {
//           // Course archiving failed
//           // Display an error message
//         }
//       });
//   };

//   return (
//     <>
//       <h1 className="text-center my-4">Admin Dashboard</h1>
//       <Table striped bordered hover responsive>
//         <thead>
//           <tr className="text-center">
//             <th>ID</th>
//             <th>Name</th>
//             <th>Description</th>
//             <th>Price</th>
//             <th>Availability</th>
//             <th colSpan="2">Actions</th>
//           </tr>
//         </thead>
//         <tbody>
//           {courses.map((course) => (
//             <tr key={course._id}>
//               <td>{course._id}</td>
//               <td>{course.name}</td>
//               <td>{course.description}</td>
//               <td>{course.price}</td>
//               <td className={course.isActive ? 'text-success' : 'text-danger'}>
//                 {course.isActive ? 'Available' : 'Unavailable'}
//               </td>
//               <td>
//                 <EditCourse course={course._id} />
//               </td>
//               <td>
//                 <ArchiveCourse
//                   courseId={course._id}
//                   handleArchiveCourse={handleArchiveCourse}
//                   isActive={course.isActive}
//                 />
//               </td>
//             </tr>
//           ))}
//         </tbody>
//       </Table>
//     </>
//   );
// }

import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import EditCourse from '../components/EditCourse';
import ArchiveCourse from '../components/ArchiveCourse';
import ReactivateCourse from '../components/ReactivateCourse';

export default function AdminView({ coursesData }) {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    // Fetch courses from your MongoDB backend
    fetch('http://localhost:4000/courses') 
      .then((response) => response.json())
      .then((data) => setCourses(data))
      .catch((error) => console.error('Error fetching courses:', error));
  }, []);

  const handleArchiveCourse = (courseId) => {
 
    fetch(`http://localhost:4000/courses/${courseId}/archive`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setCourses((prevCourses) =>
            prevCourses.map((course) =>
              course._id === courseId
                ? { ...course, isActive: !course.isActive }
                : course
            )
          );
        } else {
        
        }
      });
  };

  const handleReactivateCourse = (courseId) => {
    fetch(`http://localhost:4000/courses/${courseId}/activate`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setCourses((prevCourses) =>
            prevCourses.map((course) =>
              course._id === courseId
                ? { ...course, isActive: true }
                : course
            )
          );
        } else {
      
        }
      });
  };

  return (
    <>
      <h1 className="text-center my-4">Admin Dashboard</h1>
      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>
        <tbody>
          {courses.map((course) => (
            <tr key={course._id}>
              <td>{course._id}</td>
              <td>{course.name}</td>
              <td>{course.description}</td>
              <td>{course.price}</td>
              <td className={course.isActive ? 'text-success' : 'text-danger'}>
                {course.isActive ? 'Available' : 'Unavailable'}
              </td>
              <td>
                <EditCourse course={course._id} />
              </td>
              <td>
                {course.isActive ? (
                  <ArchiveCourse
                    courseId={course._id}
                    handleArchiveCourse={handleArchiveCourse}
                    isActive={course.isActive}
                  />
                ) : (
                  <ReactivateCourse
                    courseId={course._id}
                    handleReactivateCourse={handleReactivateCourse}
                    isActive={course.isActive}
                  />
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}
