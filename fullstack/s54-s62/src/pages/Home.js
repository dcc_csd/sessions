// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';

// // import Coursecard from '../components/CourseCard';

// export default function Home(){
//    return (
//      <> 
//           <Banner />
//           <Highlights />
          
          
//      </>
//    	)

// }

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import React, {useState, useEffect} from 'react';
import {useLocation} from 'react-router-dom';




export default function Home() {
  const location = useLocation();
  const [showError, setShowError] = useState(false);


 useEffect(() => {
    
    if (location.pathname !== '/') {
      setShowError(true);
    } else {
      setShowError(false);
    }
  }, [location.pathname]);


  return (
    <>
      <Banner showError={showError} />
      {!showError && <Highlights />}
    </>  
  );
}
