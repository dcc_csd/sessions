
const mongoose = require('mongoose');
const Order = require('../models/Order');
const OrderItem = require('../models/OrderItem');


// const { User, validate } = require("../models/User");

const { User } = require('../models/User');
const {Product, validate} = require('../models/Product');
const {verify, verifyAdmin} = require('../utils/auth');



  

// 1. create order
  // 1. create order
const isAdmin = (req) => {
  return req.user && req.user.isAdmin;
};

module.exports.createOrder = async (req, res) => {
  try {
    if (isAdmin(req)) {
      return res.status(403).json({ success: false, message: 'User is an admin. You cannot create your own order.' });
    }

    try {
      const orderItemsIds = await Promise.all(
        req.body.orderItems.map(async (orderItem) => {
          const newOrderItem = new OrderItem({
            quantity: orderItem.quantity,
            product: orderItem.product,
            price: orderItem.price,
            user: orderItem.userId,
          });

          const savedOrderItem = await newOrderItem.save();

          return savedOrderItem._id;
        })
      );

      // Calculate the total price
      const totalPrices = await Promise.all(
        orderItemsIds.map(async (orderItemId) => {
          const orderItem = await OrderItem.findById(orderItemId);
          const totalPrice = orderItem.price * orderItem.quantity;
          return totalPrice;
        })
      );

      const totalPrice = totalPrices.reduce((a, b) => a + b, 0);

      let order = new Order({
        orderItems: orderItemsIds,
        status: req.body.status,
        totalPrice: totalPrice,
        user: req.body.userId, // Set the user ID based on the request's user
      });

      order = await order.save();

      if (!order) {
        return res.status(400).json({ success: false, message: 'The order could not be created' });
      }

      // You can call the updateProductQuantities function here

      res.status(200).json({ success: true, message: 'Order created successfully', order });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
  } catch (outerError) {
    console.error(outerError);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};



//2. Get an order per ID
 module.exports.getOrderById = async (req, res) => {
  try {
    const orderId = req.params.id;

    const order = await Order.findById(orderId).populate('orderItems'); 

    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    const orderItemIds = order.orderItems.map(orderItem => orderItem._id);

    res.status(200).json({
      success: true,
      order,
      orderItemIds, 
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
   }
  };


//3. Get all orders with total orders|pending|processing status

  module.exports.getAllOrders = async (req, res) => {
  try {
    const allOrders = await Order.find();
    const totalOrders = allOrders.length;

    const pendingOrders = allOrders.filter(order => order.status === 'Pending').length;
    const processingOrders = allOrders.filter(order => order.status === 'Processing').length;
    const cancelledOrders = allOrders.filter(order => order.status === 'Cancelled').length;
    const deliveredOrders = allOrders.filter(order => order.status === 'Delivered').length;

    const orderList = await Order.find({}, { _id: 1, orderItems: 1, totalPrice: 1, dateOrdered: 1, status: 1, user: 1 }).sort({ dateOrdered: -1 });

    if (!orderList) {
      return res.status(500).json({ success: false, message: 'No orders found' });
    }

    res.status(200).json({
      success: true,
      totalOrders,
      pendingOrders,
      processingOrders,
      cancelledOrders,
      deliveredOrders,
      orders: orderList
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
 };

// 4. Add Cart - add product on the existing order
 module.exports.addOrderItems = async (req, res) => {
  try {
    const { orderId } = req.params;

  
    const order = await Order.findById(orderId);

    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    
    const orderItemsIds = await Promise.all(
      req.body.orderItems.map(async (orderItem) => {
        let newOrderItem = new OrderItem({
          quantity: orderItem.quantity,
          product: orderItem.product,
        });

        newOrderItem = await newOrderItem.save();
        order.orderItems.push(newOrderItem);

        return newOrderItem._id;
      })
    );

    // Calculation
    const totalPrices = await Promise.all(
      orderItemsIds.map(async (orderItemId) => {
        const orderItem = await OrderItem.findById(orderItemId).populate('product', 'price');
        const totalPrice = orderItem.product.price * orderItem.quantity;
        return totalPrice;
      })
    );

    const totalPrice = totalPrices.reduce((a, b) => a + b, 0);
    order.totalPrice += totalPrice;

    await order.save();

    // Update product qty
    await updateProductQuantities(req.body.orderItems);

    res.status(200).json({ success: true, message: 'Order items added in the cart', order });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};

// update product quantities
async function updateProductQuantities(orderItems) {
  for (const orderItem of orderItems) {
    const product = await Product.findById(orderItem.product);

    if (!product) {
       console.error(`Product not found for order item: ${orderItem.product}`);
      continue;
    }

    if (product.stockQty < orderItem.quantity) {
      console.error(`Insufficient stock for order item: ${orderItem.product}`);
      continue;
    }
    product.stockQty -= orderItem.quantity;

  await product.save();
  }
}

//5.Remove a product from an order
  module.exports.removeOrderItem = async (req, res) => {
  
  try {
    const { orderId, itemId } = req.params;
    const order = await Order.findById(orderId);

    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    // Check if the item to remove exists in the order's items
    const orderItemIndex = order.orderItems.findIndex((item) => item.equals(itemId));

    if (orderItemIndex === -1) {
      return res.status(404).json({ success: false, message: 'Item not found in order' });
    }

   
    order.orderItems.splice(orderItemIndex, 1);

    // Calculation
    const removedItem = await OrderItem.findById(itemId).populate('product', 'price');
    const removedItemPrice = removedItem.product.price * removedItem.quantity;
    order.totalPrice -= removedItemPrice;
    await order.save();

    await OrderItem.findByIdAndRemove(itemId);

    res.status(200).json({ success: true, message: 'Item removed from the cart', order });
   } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
   }
 
  }; 


//6. Change the product quantities inside the Cart

// Helper function to calculate total price
async function calculateTotalPrice(order) {
  let totalPrice = 0;
  for (const item of order.orderItems) {
    const orderItem = await OrderItem.findById(item).populate('product');
    totalPrice += orderItem.product.price * orderItem.quantity;
  }
  return totalPrice;
}

// update order item quantity and product stockQty
module.exports.updateOrderItemQuantity = async (req, res) => {
  try {
    const orderId = req.params.orderId;
    const orderItemId = req.params.orderItemId;
    const newQuantity = req.body.newQuantity;

    const order = await Order.findById(orderId).populate('orderItems');
    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    const orderItem = order.orderItems.find(item => item._id.toString() === orderItemId);
    if (!orderItem) {
      return res.status(404).json({ success: false, message: 'Order Item not found' });
    }

    const product = await Product.findById(orderItem.product);
    if (!product) {
      return res.status(404).json({ success: false, message: 'Product not found' });
    }

    const stockDifference = newQuantity - orderItem.quantity;

    if (stockDifference > product.stockQty) {
      return res.status(400).json({ success: false, message: 'Insufficient stock quantity' });
    }

    // Update the product's stockQty
    product.stockQty -= stockDifference;
    await product.save();

    orderItem.quantity = newQuantity;
    await orderItem.save();

    const totalPrice = await calculateTotalPrice(order);

    order.totalPrice = totalPrice;
    await order.save();

    res.status(200).json({ success: true, message: 'Order Item quantity updated successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};




//7. Change Order Status of the Order
  module.exports.updateOrderStatus = async (req, res) => {
  try {
    const orderId = req.params.orderId;
    const newStatus = req.body.status;

    const order = await Order.findById(orderId).populate('orderItems');

    if (!order) {
      return res.status(404).json({ success: false, message: 'Order not found' });
    }

    // Check if the new status is "Cancelled"
    if (newStatus === 'Cancelled' && order.status !== 'Cancelled') {
      // Iterate through order items and return quantity to stock
      for (const orderItem of order.orderItems) {
        const product = await Product.findById(orderItem.product);
        if (!product) {
          continue; // Handle the case where the product is not found
        }
        
        // Add the ordered quantity back to stockQty
        product.stockQty += orderItem.quantity;
        await product.save();
      }
    }

    // Check if the new status is "Processing" or "Delivered"
    if (newStatus === 'Processing' || newStatus === 'Delivered') {
      const productsWithStatus = [];

      for (const orderItem of order.orderItems) {
        const product = await Product.findById(orderItem.product);
        if (!product) {
          continue; // Handle the case where the product is not found
        }

        // Check if the product's status matches the new status
        if (product.status === newStatus) {
          productsWithStatus.push({ productId: product._id, status: newStatus });
        }
      }

      if (productsWithStatus.length > 0) {
        return res.status(400).json({
          success: false,
          message: `The following products already have a status of '${newStatus}': ${productsWithStatus.map(p => p.productId).join(', ')}`,
        });
      }
    }

    order.status = newStatus;
    await order.save();

    res.status(200).json({ success: true, message: 'Order status updated successfully', order });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};



//8. Get all orders of the specific user
  
module.exports.getOrdersByUserId = async (req, res) => {
  try {
    const userId = req.params.userId;

    // Find orders based on the 'user' field
    const orders = await Order.find({ user: userId });

    res.status(200).json(orders);
  } catch (err) {
    console.error('Error:', err);
    res.status(500).json(err);
  }
};


module.exports.getOrdersByUser = async (req, res) => {
  try {
    const orders = await Order.find({ userId: req.params.userId });
    res.status(200).json(orders);
  } catch (err) {
    res.status(500).json(err);
  }
};





//9. Get all orders per status
  module.exports.getOrdersByStatus = async (req, res) => {
  try {
    const { status } = req.params;
    const validStatuses = ['Pending', 'Processing', 'Cancelled', 'Delivered'];

    if (!validStatuses.includes(status)) {
      return res.status(400).json({ success: false, message: 'Invalid status' });
    }

    const orders = await Order.find({ status });

    if (!orders) {
      return res.status(500).json({ success: false, message: `No ${status} orders found` });
    }

    const totalOrders = orders.length;

    const orderList = await Order.find(
      { status },
      { _id: 1, orderItems: 1, totalPrice: 1, dateOrdered: 1, status: 1, user: 1 }
    ).sort({ dateOrdered: -1 });

    res.status(200).json({
      success: true,
      message: `This is the total number of ${status} orders: ${totalOrders}`,
      orders: orderList,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};













