
const Category= require("../models/category");


const {verify, verifyAdmin} = require('../utils/auth');

//1. Create a Category
     module.exports.addCategory = (req, res) =>{
	   let newCategory = new Category ({
		 
		 name: req.body.name,
		 brand: req.body.brand,
     color: req.body.color
			
	   });

	  return newCategory.save().then((category, error) => {
		if (error){
		return res.status(400).send('The category could not be created');
		
    }else{
		 return res.status(200).send('Category successfully created');
		}
	
    })
	  .catch(error => res.send (error));
   };


  
//2.  Get a list of categories
  module.exports.getCategories = async (req, res) => {
  try {
   
    const categoryList = await Category.find();
    if (!categoryList) {
      return res.status(500).json({ success: false, message: 'No categories found' });
    }
    res.status(200).json({ success: true, categories: categoryList });
    } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
   }
  };



//3. Get Category by ID
  module.exports.getCategoryById = async (req, res) => {
   try {
     const category = await Category.findById(req.params.id);
     if (!category) {
     return res.status(404).json({ message: 'The category with the given ID was not found.' });
     }
     res.status(200).json(category);
   
    } catch (error) {
     console.error(error);
     res.status(500).json({ message: 'Internal Server Error' });
    }
  };



//4. Update existing category


// Update a Category by ID
module.exports.updateCategory = async (req, res) => {
  const categoryId = req.params.id; // Get the category ID from the request parameters
  const updateFields = req.body; // Get the updated fields from the request body

  try {
    // Find the category by ID and update it
    const updatedCategory = await Category.findByIdAndUpdate(categoryId, updateFields, {
      new: true, // Return the updated category
    });

    if (!updatedCategory) {
      return res.status(404).json({ message: 'Category not found' });
    }

    return res.status(200).json({ message: 'Category updated successfully', category: updatedCategory });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};
  






//5. Delete a Category

  exports.deleteCategory = async (req, res) => {
    try {
     const category = await Category.findByIdAndRemove(req.params.id);
     if (category) {
       return res.status(200).json({ success: true, message: 'The category is deleted!' });
     } else {
       return res.status(404).json({ success: false, message: 'Category not found!' });
     }

   } catch (error) {
     console.error(error);
     return res.status(500).json({ success: false, error: error.message });
   }
 };
