//[SECTION] Dependencies and Modules
    const User = require("../models/User");
    const Course = require("../models/Course");
    const bcrypt = require('bcrypt');
    const auth = require("../auth");


//[SECTION] Check if the email already exists

    module.exports.checkEmailExists = (reqBody) => {

        // The result is sent back to the frontend via the "then" method found in the route file
        return User.find({email : reqBody.email}).then(result => {

            // The "find" method returns a record if a match is found
            if (result.length > 0) {
                return true;

            // No duplicate email found
            // The user is not yet registered in the database
            } else {
                return false;
            }
        })
        .catch(err => res.send(err))
    };


//[SECTION] User registration
    module.exports.registerUser = (reqBody) => {

        // Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
        // Uses the information from the request body to provide all the necessary information
        let newUser = new User({
            firstName : reqBody.firstName,
            lastName : reqBody.lastName,
            email : reqBody.email,
            mobileNo : reqBody.mobileNo,
            password : bcrypt.hashSync(reqBody.password, 10)
        })

        // Saves the created object to our database
        return newUser.save().then((user, error) => {

            // User registration failed
            if (error) {
                return false;

            // User registration successful
            } else {
                return true;
            }
        })
        .catch(err => err)
    };


//[SECTION] User authentication

    module.exports.loginUser = (req, res) => {
        User.findOne({ email : req.body.email} ).then(result => {

            console.log(result);

            // User does not exist
            if(result == null){

                return res.send(false);

            // User exists
            } else {
                
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
                // If the passwords match/result of the above code is true
                if (isPasswordCorrect) {
    
                    return res.send({ access : auth.createAccessToken(result) })

                // Passwords do not match
                } else {

                    return res.send(false);
                }
            }
        })
        .catch(err => res.send(err))
    };




//[SECTION] Retrieve user details

    module.exports.getProfile = (req, res) => {

        return User.findById(req.user.id)
        .then(result => {
            result.password = "";
            return res.send(result);
        })
        .catch(err => res.send(err))
    };


//[Section] Get all users

   module.exports.getAllUsers = (req, res) => {
    return User.find({}).then(result => {
        if(result.length === 0){
            return res.send("There is no users in the DB.");
        }else{
            return res.send(result);
        }
    })
}





    //[SECTION] Reset Password

    module.exports.resetPassword = async (req, res) => {
        try {

        //console.log(req.user)
        //console.log(req.body)

          const { newPassword } = req.body;
          const { id } = req.user; // Extracting user ID from the authorization header
      
          // Hashing the new password
          const hashedPassword = await bcrypt.hash(newPassword, 10);
      
          // Updating the user's password in the database
          await User.findByIdAndUpdate(id, { password: hashedPassword });
      
          // Sending a success response
          res.status(200).send({ message: 'Password reset successfully' });
        } catch (error) {
          console.error(error);
          res.status(500).send({ message: 'Internal server error' });
        }
    };

    //[SECTION] Reset Profile
    module.exports.updateProfile = async (req, res) => {
        try {

            console.log(req.user);
            console.log(req.body);
            
        // Get the user ID from the authenticated token
          const userId = req.user.id;
      
          // Retrieve the updated profile information from the request body
          const { firstName, lastName, mobileNo } = req.body;
      
          // Update the user's profile in the database
          const updatedUser = await User.findByIdAndUpdate(
            userId,
            { firstName, lastName, mobileNo },
            { new: true }
          );
      
          res.send(updatedUser);
        } catch (error) {
          console.error(error);
          res.status(500).send({ message: 'Failed to update profile' });
        }
      }




      //[SECTION] Update profile
    // module.exports.updateUser = async (req, res) => {
    //     try {

    //         console.log(req.user);
    //         console.log(req.body);
            
    //     // Get the user ID from the authenticated token
    //       const userId = req.params.userId;
      
    //       // Retrieve the updated profile information from the request body
    //       const { firstName, lastName, mobileNo, email } = req.body;
      
    //       // Update the user's profile in the database
    //       const updatedUser = await User.findByIdAndUpdate(
    //         userId,
    //         { firstName, lastName, mobileNo, email },
    //         { new: true }
    //       );
      
    //       res.send(updatedUser);
    //     } catch (error) {
    //       console.error(error);
    //       res.status(500).send({ message: 'Failed to update profile' });
    //     }
    //   }

  module.exports.updateUser = (req, res) => {
    let updatedUser = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        mobileNo: req.body.mobileNo,
        isAdmin: req.body.isAdmin
    }

    return User.findByIdAndUpdate(req.params.userId, updatedUser).then((course, error) => {
        if(error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
}


 
  //[SECTION] Get single user
 //  module.exports.getUser = async (req, res) => {
 //   try {
    
 //    const user = await User.findById(req.user.id);

 //    if (!user) {
 //      return res.status(404).send("User doesn't exist.");
 //    }

 //    return res.status(200).json(user);
 //  } catch (error) {
 //    console.error(error);
 //    return res.status(500).json({ message: 'Internal Server Error' });
 //  }
 // };

exports.findUserById = async (req, res) => {
    return User.findById(req.params.userId).then(result =>{
        if(result === 0){
            return res.send("Cannot find course with the provided ID.")
        }else{
            if(result.isActive === false){
                return res.send("The course you are trying to access is not available.");
            }else{
                return res.send(result);
            }
        }
    })
    .catch(error => res.send("You provided a wrong course ID"));
}






    //[ACTIVITY] Update Enrollment Status
    module.exports.updateEnrollmentStatus = async (req, res) => {
        try {
          const { userId, courseId, status } = req.body;
      
          // Find the user and update the enrollment status
          const user = await User.findById(userId);
      
          // Find the enrollment for the course in the user's enrollments array
          const enrollment = user.enrollments.find((enrollment) => enrollment.courseId === courseId);
      
          if (!enrollment) {
            return res.status(404).json({ error: 'Enrollment not found' });
          }
      
          enrollment.status = status;
      
          // Save the updated user document
          await user.save();
      
          res.status(200).json({ message: 'Enrollment status updated successfully' });
        } catch (error) {
          res.status(500).json({ error: 'An error occurred while updating the enrollment status' });
        }
      };    


//[ACTIVITY] Update user as admin controller
exports.updateUserAsAdmin = async (req, res) => {
    try {
      const { userId } = req.body;
  
      // Find the user and update isAdmin flag
      const user = await User.findById(userId);
  
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }
  
      user.isAdmin = true;
  
      // Save the updated user document
      await user.save();
  
      res.status(200).json({ message: 'User updated as admin successfully' });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while updating the user as admin' });
    }
  };
