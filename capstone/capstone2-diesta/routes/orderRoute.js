const express = require('express');
const router = express.Router();

const Order = require('../models/Order');
const User = require('../models/User');
const OrderItem = require('../models/OrderItem');

const orderController = require('../controllers/orderController');

const {verify, verifyAdmin} = require('../utils/auth');


//[ORDERS]

  //1.Creating orders
      router.post('/', verify, orderController.createOrder);


  
  //2. Get an order per ID
      router.get('/:id', verify, verifyAdmin, orderController.getOrderById);
 


  //3. Get all Orders 
      router.get('/', verify, orderController.getAllOrders);



  //4. Add to Cart - add product to an existing order
      router.post('/:orderId/add-items', verify, orderController.addOrderItems);

  
  //5. Add to Cart - delete product to an existing order
      router.delete('/:orderId/remove-item/:itemId', verify, orderController.removeOrderItem);



  //6. Change product quantities inside the Cart
      router.put('/:orderId/order-item/:orderItemId', verify, orderController.updateOrderItemQuantity);


  //7. Change Order Status of the Order
       router.put('/update-status/:orderId', verify, verifyAdmin, orderController.updateOrderStatus);


  //8. Get all orders of the specific user
       router.get('/user/:userId', verify, orderController.getOrdersByUserId);

       router.get('/find/:userId', verify, orderController.getOrdersByUserId);


  //. Get all orders base on the status
       router.get('/status/:status', verify, verifyAdmin, orderController.getOrdersByStatus);




module.exports = router;