const express = require('express');

const productController = require('../controllers/productController.js');


const Token = require("../models/token");
const sendEmail = require("../utils/email");
const cloudinary = require('../utils/cloudinary');
// const multer = require("../utils/multer");
const upload = require ("../utils/multer");


const auth = require("../utils/auth.js")
const sendMail = require("../utils/sendMail");
const router = express.Router();

const { verify, verifyAdmin } = require('../utils/auth');


const bcrypt = require('bcrypt');
const crypto = require('crypto');
const jtw = require('jsonwebtoken');

const nodemailer = require('nodemailer');


//[ADMIN Section]

//a.Register-Product
 router.post('/registration', upload.single('image'), productController.registerProduct);

  
router.post('/upload-image', productController.uploadImage);

router.post('/', productController.regProduct);

router.put('/:productId', upload.single('image'), productController.updateProductById);

router.get('/:productId', productController.getProductId);

router.get('/', productController.getAllProduct);


router.put('/:productId', productController.updateProductById);

// // router.post('/registration', uploadOptions, productController.registerProduct);


//b.Get all products (archive and active products)


 //c.Update specific Product (Admin Only)



// //d.Archive specific Product (Admin Only)
//    router.put("/archive/:productId", verify, verifyAdmin, productController.archiveProduct);


// //e.Re-activate specific Product (Admin Only)
//    router.put("/activate/:productId", verify, verifyAdmin, productController.reactivateProduct);


// //f.Get all archive product
//     router.get("/archive/product", verify, verifyAdmin, productController.findArchivedProducts);
   

// //g.add and upload multiple images to a specific product
//     // router.post('/upload-images/:productId', verify, verifyAdmin, multer.array('images', 5), productController.addImagesToProduct);


// // //[USER/CLIENT SECTION]

// //a.Retrieve all active product
//     router.get("/get-active-products", verify, productController.getAllActiveProducts);


// //b.Retrieve specific active 
//     router.get("/view/:productId", verify, productController.getProductId);



module.exports = router;