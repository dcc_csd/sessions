const mongoose = require('mongoose');

const CartSchema = new mongoose.Schema(
  {
    userId: { type: String, required: true },

	productId: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Product', 
    },
        
    status: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'active',
    },
 
    { timestamps: true }

  },
  
);

module.exports = mongoose.model('Wishlist', CartSchema);