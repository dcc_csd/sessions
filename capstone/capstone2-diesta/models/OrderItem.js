const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

const orderItemSchema = new mongoose.Schema({

   quantity: {
        type: Number,
        required: true
    },
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    price: {
        type: Number,
       
    },
})




module.exports = mongoose.model("OrderItem", orderItemSchema);