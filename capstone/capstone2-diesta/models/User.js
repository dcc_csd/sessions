//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },

     verified: {
    type: Boolean,
    default: false
         },

    emailToken: {
    type: String
      },
    
    country: {
        type: String,
      },
    
    city:{
        type: String,
      },
    
    address:{
        type: String,
      },
    
    zipCode:{
        type: Number,
      },


    cart:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Cart',
      },


    avatar:{
    public_id: {
      type: String
               },
    
    url: {
      type: String
         },
    },
 
    
    createdAt:{
    type: Date,
    default: Date.now(),
    },
    
    resetPasswordToken: String,
 
    resetPasswordTime: Date,
   

  


    // The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
    enrollments : [
        {
            courseId : {
                type : String,
                required : [true, "Course ID is required"]
            },
            enrolledOn : {
                type : Date,
                default : new Date()
            },
            status : {
                type : String,
                default : "Enrolled"
            }
        }
    ]
})

//[SECTION] Model
module.exports = mongoose.model("User", userSchema);