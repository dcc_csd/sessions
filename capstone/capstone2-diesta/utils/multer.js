// const multer = require("multer");
// const path = require("path");



// // Multer config
// module.exports = multer({
//   storage: multer.diskStorage({
//   	destination: (req, file, cb) => {
//   		cb(null, path.join(__dirname, "../uploads")) 
//   	},
  
//   	fileFilter: (req, file, cb) => {
// 	    let ext = path.extname(file.originalname)  
// 	    if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
//       		cb(new Error("File type is not supported"), false)
//       		return;
//     	}
//     	cb(null, true)
//   	}
// 	})
// })
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 20, // Changed 'fieldsize' to 'fileSize'
  },
});

module.exports = upload;
