// require("dotenv").config();
// const cloudinary = require("cloudinary").v2;

// cloudinary.config({
//   cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
//   api_key: process.env.CLOUDINARY_API_KEY,
//   api_secret: process.env.CLOUDINARY_API_SECRET,
// });

// const opts = {
//   overwrite: true,
//   invalidate: true,
//   resource_type: "auto",
// };

// const uploadToCloudinary = (image) => {
//   return new Promise((resolve, reject) => {
//     cloudinary.uploader.upload(image, opts, (error, result) => {
//       if (result && result.secure_url) {
//         console.log(result.secure_url);
//         resolve(result.secure_url);
//       } else {
//         console.log(error.message);
//         reject({ message: error.message });
//       }
//     });
//   });
// };

// module.exports = { cloudinary, uploadToCloudinary };

const cloudinary = require("cloudinary").v2;
require("dotenv").config();

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

module.exports = cloudinary;




// const cloudinary = require('cloudinary').v2; // Import the Cloudinary library

// cloudinary.config({
//   cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
//   api_key: process.env.CLOUDINARY_API_KEY,
//   api_secret: process.env.CLOUDINARY_API_SECRET,
// });

// async function cloudinaryUpload(base64EncodedImage) {
//   try {
//     const result = await cloudinary.uploader.upload(base64EncodedImage, {
//       upload_preset: '../uploads',
//     });

//     return result.url; 
//   } catch (error) {
//     console.error('Error uploading image to Cloudinary:', error);
//     throw error;
//   }
// }

// module.exports = {
//   cloudinaryUpload,
// };
