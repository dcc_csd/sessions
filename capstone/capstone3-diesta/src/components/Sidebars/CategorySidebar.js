import React from 'react';
import {
  PiDressLight,
  PiShirtFoldedLight,
  PiBabyLight,
  PiAlignLeftDuotone,
  PiAppleLogoLight,
  PiBaseballCapLight,
  PiBicycleLight,
  PiGameControllerLight,
  PiGift,
} from 'react-icons/pi';
import { TbCategory } from "react-icons/tb";
import '../../App.css';

export default function CategorySidebar() {
  return (
    <div className="category-sidebar">
      <div className="category-sidebar-content">
        <ul className="category-list">
          {/* CATEGORIES item with dark background */}
          <li className="bg-dark text-white p-3">
            <div>
              <TbCategory className="category-icon" />
              <span className="category-label text-white">CATEGORIES</span>
            </div>
          </li>
          
          <li className="border-bottom border-secondary py-3 px-1">
            <a href="#/category-1" className="category-item">
              <PiShirtFoldedLight className="category-icon" /> Men
            </a>
          </li>
          
          <li className="border-bottom border-secondary py-3 px-1">
            <a href="#/category-2" className="category-item">
              <PiDressLight className="category-icon" /> Women
            </a>
          </li>
          
          <li className="border-bottom border-secondary py-3 px-1">
            <a href="#/category-3" className="category-item">
              <PiBabyLight className="category-icon" /> Kid's Wear
            </a>
          </li>
          
          <li className="border-bottom border-secondary py-3 px-1">
            <a href="#/category-1" className="category-item">
              <PiBaseballCapLight className="category-icon" /> Accessories
            </a>
          </li>
          
          <li className="border-bottom border-secondary py-3 px-1">
            <a href="#/category-3" className="category-item">
              <PiGameControllerLight className="category-icon" /> Toys
            </a>
          </li>
          
          <li className="border-bottom border-secondary py-3 px-1">
            <a href="#/category-1" className="category-item">
              <PiBicycleLight className="category-icon" /> Sports
            </a>
          </li>
          
          <li className="border-bottom border-secondary py-3 px-1">
            <a href="#/category-2" className="category-item">
              <PiAppleLogoLight className="category-icon" /> Gadgets
            </a>
          </li>
          
          <li className="py-3 px-1">
            <a href="#/category-3" className="category-item">
              <PiGift className="category-icon" /> Gift Corner
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}