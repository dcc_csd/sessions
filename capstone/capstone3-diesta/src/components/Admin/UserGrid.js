import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import UserCard from './UserCard';
import Pagination from './Pagination'; // Import the Pagination component

const UserGrid = ({ users }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 9;

  // Get current users based on the current page
  const indexOfLastUser = currentPage * itemsPerPage;
  const indexOfFirstUser = indexOfLastUser - itemsPerPage;
  const currentUsers = users.slice(indexOfFirstUser, indexOfLastUser);

  // Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <Container className="mt-5">
      <Row>
        {currentUsers.map((user) => (
          <Col key={user._id} lg={{ span: 4 }}>
            <UserCard user={user} />
          </Col>
        ))}
      </Row>
      <Pagination
        itemsPerPage={itemsPerPage}
        totalItems={users.length}
        paginate={paginate}
      />
    </Container>
  );
};

export default UserGrid;
