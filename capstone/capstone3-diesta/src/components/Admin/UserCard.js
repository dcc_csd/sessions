
import React from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { AiOutlineEye } from 'react-icons/ai';
import { BiEnvelope, BiPhone } from 'react-icons/bi';
import { Link } from 'react-router-dom';

const UserCard = ({ user }) => {
  const { _id, firstName, lastName, email, mobileNo, image } = user;

  return (
    <Card
      className="mb-3"
      style={{
        maxWidth: '540px',
        boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
        padding: '5px',
      }}
    >
      <div className="d-flex justify-content-end">
        <Link to={`/view/${_id}`}>
          <Button
            variant="text-dark"
            size="lg"
            bsPrefix
            className="btn-icon-only"
            style={{ border: 'none', background: 'none' }}
          >
            <AiOutlineEye size={24} />
          </Button>
        </Link>
      </div>
      

      <div className="d-flex align-items-center">
        <div
          style={{
            width: '80px',
            height: '80px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            border: '1px solid #ccc',
          }}
        >
          <img
            src={image}
            className="img-fluid"
            alt={firstName}
            style={{
              width: '80%',
              height: '80%',
              verticalAlign: 'right',
            }}
          />
        </div>
        <div className="col-md-8">
          <div className="card-body">
            <span
              className="card-title user-name"
              style={{
                fontWeight: 'bold',
                fontSize: '14px',
              }}
            >
              {firstName.toUpperCase()} {lastName.toUpperCase()}
            </span>
            <div className="d-flex align-items-center">
              <BiEnvelope className="me-2" />
              <span className="card-text user-email" style={{ fontSize: '13px' }}>{email}</span>
            </div>
            <div className="d-flex align-items-center">
              <BiPhone className="me-2" />
              <span className="card-text user-mobile" style={{ fontSize: '13px' }}>{mobileNo}</span>
            </div>
            <p className="card-text">
              <small className="text-muted" style={{ fontSize: '11px' }}>
                ID: {_id}
              </small>
            </p>
          </div>
        </div>
      </div>
    </Card>
  );
};

UserCard.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    mobileNo: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
  }),
};

export default UserCard;







