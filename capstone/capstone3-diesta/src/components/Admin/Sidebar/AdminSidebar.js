
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import StoreIcon from "@mui/icons-material/Store";
import BrandingWatermarkIcon from '@mui/icons-material/BrandingWatermark';
import SettingsApplicationsIcon from "@mui/icons-material/SettingsApplications";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import SettingsSystemDaydreamOutlinedIcon from "@mui/icons-material/SettingsSystemDaydreamOutlined";
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import PsychologyOutlinedIcon from "@mui/icons-material/PsychologyOutlined";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import { Dropdown } from 'react-bootstrap';

import { DarkModeContext } from "./darkModeContext";

import shoplogo1Image from "./shoplogo1.png";
import shoplogo2Image from "./shoplogo2.png";


import "./sidebar.css";

const Sidebar = () => {
  const { dispatch } = useContext(DarkModeContext);

  return (
    <div className="sidebar">
    

      <div className="center">
        <ul>
          <p className="title">MAIN</p>
          <Link to="/admin-dashboard" style={{ textDecoration: "none" }}>
          <li>
            <DashboardIcon className="icon" />
            <span><p className="text text-dark">Dashboard</p></span>
          </li>
          </Link>
          <p className="title">LISTS</p>
          
          <Link to="/admin-list-page" style={{ textDecoration: "none" }}>
            <li>
              <AdminPanelSettingsIcon className="icon" />
              <span>Admin</span>
            </li>
          </Link>

          <Link to="/user-list" style={{ textDecoration: "none" }}>
            <li>
              <PersonOutlineIcon className="icon" />
              <span>Users</span>
            </li>
          </Link>

          <Link to="/all-product" style={{ textDecoration: "none" }}>
            <li>
              <StoreIcon className="icon" />
              <span>Products</span>
            </li>
          </Link>
          
          <Link to="/all-order" style={{ textDecoration: "none" }}>
          <li>
            <CreditCardIcon className="icon" />
            <span>Orders</span>
          </li>
          </Link>

          <li>
            <LocalShippingIcon className="icon" />
            <span>Delivery</span>
          </li>


          <p className="title">USEFUL</p>
          <li>
            <BrandingWatermarkIcon className="icon" />
            <span>Brand</span>
          </li>
          <li>
            <NotificationsNoneIcon className="icon" />
            <span>Authentication</span>
          </li>
          <p className="title">SERVICE</p>
          <li>
            <SettingsSystemDaydreamOutlinedIcon className="icon" />
            <span>Notification</span>
          </li>
          
          <li>
            <SettingsApplicationsIcon className="icon" />
            <span>Settings</span>
          </li>
          <p className="title">USER</p>
          <li>
            <AccountCircleOutlinedIcon className="icon" />
            <span>Profile</span>
          </li>

           <Link to="/logout" style={{ textDecoration: "none" }}>"
          <li>
            <ExitToAppIcon className="icon" />
            <span>Logout</span>
          </li>  
          </Link>

        </ul>
      </div>
      <div className="bottom">
        <div
          className="colorOption"
          onClick={() => dispatch({ type: "LIGHT" })}
        ></div>
        <div
          className="colorOption"
          onClick={() => dispatch({ type: "DARK" })}
        ></div>
      </div>
    </div>
  );
};


export default Sidebar;
