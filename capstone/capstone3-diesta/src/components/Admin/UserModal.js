import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const UserModal = ({ user, show, onClose }) => {
  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>User Details</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {user && (
          <div>
            <h5>Name: {user.firstName} {user.lastName}</h5>
            <p>Email: {user.email}</p>
            <p>Mobile No: {user.mobileNo}</p>
            {/* Add more user details here */}
          </div>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UserModal;

