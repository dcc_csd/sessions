import React, { useState } from 'react';
import { Table, FormControl, Dropdown } from 'react-bootstrap';
import { FiArrowUp, FiArrowDown } from 'react-icons/fi';
import { BsThreeDotsVertical } from 'react-icons/bs';
import EditUser from './EditUser';



const UserTable = ({ users }) => {
  const [showCount, setShowCount] = useState(10);
  const [searchTerm, setSearchTerm] = useState('');
  const [sortField, setSortField] = useState(null);
  const [sortDirection, setSortDirection] = useState('asc');
  const [currentPage, setCurrentPage] = useState(1);

  const handleShowCountChange = (count) => {
    setShowCount(count);
    setCurrentPage(1); // Reset to the first page when changing the number of entries
  };

  const handleSort = (field) => {
    if (field === sortField) {
      setSortDirection(sortDirection === 'asc' ? 'desc' : 'asc');
    } else {
      setSortField(field);
      setSortDirection('asc');
    }
  };

  const sortedUsers = [...users];

  if (sortField) {
    sortedUsers.sort((a, b) => {
      const fieldA = (a[sortField] || '').toLowerCase();
      const fieldB = (b[sortField] || '').toLowerCase();
      return sortDirection === 'asc' ? fieldA.localeCompare(fieldB) : fieldB.localeCompare(fieldA);
    });
  }

  const filteredUsers = sortedUsers.filter((user) => {
    return (
      user.firstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      user.lastName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      user._id.toLowerCase().includes(searchTerm.toLowerCase())
    );
  });

  const getSortIcon = (field) => {
    if (field === sortField) {
      return sortDirection === 'asc' ? <FiArrowUp /> : <FiArrowDown />;
    }
    return null;
  };

  const entriesPerPage = parseInt(showCount, 10);
  const totalEntries = filteredUsers.length;
  const totalPages = Math.ceil(totalEntries / entriesPerPage);
  const startIndex = (currentPage - 1) * entriesPerPage;
  const endIndex = startIndex + entriesPerPage;
  const displayedUsers = filteredUsers.slice(startIndex, endIndex);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderPagination = () => {
    const pages = [];
    for (let i = 1; i <= totalPages; i++) {
      pages.push(
        <li key={i} className={`page-item ${currentPage === i ? 'active' : ''}`}>
          <a className="page-link" onClick={() => handlePageChange(i)}>
            {i}
          </a>
        </li>
      );
    }


    return (
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-end">
          <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
            <a className="page-link" onClick={() => handlePageChange(currentPage - 1)}>
              Previous
            </a>
          </li>
          {pages}
          <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
            <a className="page-link" onClick={() => handlePageChange(currentPage + 1)}>
              Next
            </a>
          </li>
        </ul>
      </nav>
    );
  };

  return (
    <div>
      <div className="d-flex justify-content-between align-items-center mb-3">
        <FormControl
          type="text"
          placeholder="Search by Name or ID"
          style={{ width: '350px', marginBottom: '-10px' }}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <div className="d-flex align-items-center">
          <div>
            Show
            <select
              className="ms-2"
              onChange={(e) => handleShowCountChange(e.target.value)}
              value={showCount}
            >
              <option value={7}>7</option>
              <option value={10}>10</option>
              <option value={15}>15</option>
            </select>
            entries
          </div>
        </div>
      </div>
      <Table bordered hover>
        <thead>
          <tr>
            <th onClick={() => handleSort('image')}>
              Profile Picture {getSortIcon('image')}
            </th>
            <th onClick={() => handleSort('firstName')}>
              Name {getSortIcon('firstName')}
            </th>
            <th onClick={() => handleSort('email')}>
              Email {getSortIcon('email')}
            </th>
            <th onClick={() => handleSort('mobileNo')}>
              Mobile No {getSortIcon('mobileNo')}
            </th>
            
            <th> User ID</th>
            <th> Action</th>
          </tr>
        </thead>
        <tbody>
          {displayedUsers.map((user) => (
            <tr key={user._id}>
              <td>
                <img
                  src={user.image}
                  alt={user.firstName}
                  style={{
                    width: '50px',
                    height: '50px',
                    borderRadius: '50%',
                  }}
                />
              </td>
              <td>{user.firstName} {user.lastName}</td>
              <td>{user.email}</td>
              <td>{user.mobileNo}</td>
              <td>{user._id}</td>
              <td>
                <Dropdown>
                  <Dropdown.Toggle
                    variant="text-dark"
                    id="dropdown-basic4-tb2"
                    bsPrefix
                    className="btn-icon-only"
                    size="sm"
                  >
                    <BsThreeDotsVertical />
                  </Dropdown.Toggle>
                  <Dropdown.Menu align="right">
                    <Dropdown.Item href="#/loading">View</Dropdown.Item>
                    <EditUser user={user} />
                  </Dropdown.Menu>
                </Dropdown>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {renderPagination()}
    </div>
  );
};

export default UserTable;


