import React, { useState, useEffect } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditUser({ user }) {
  // State for edit user modal
  const [showEdit, setShowEdit] = useState(false);

  // State for user data
  const [userData, setUserData] = useState({
    userId: '',
    firstName: '',
    lastName: '',
    email: '',
    mobileNo: '',
  });

  useEffect(() => {
    if (user) {
      setUserData({
        userId: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        mobileNo: user.mobileNo,
      });
    }
  }, [user]);

  const openEdit = () => {
    setShowEdit(true);
  };

  const closeEdit = () => {
    setShowEdit(false);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUserData({
      ...userData,
      [name]: value,
    });
  };

  const editUser = (e) => {
    e.preventDefault();

    fetch(`https://capstone2-diesta.onrender.com/users/${userData.userId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        firstName: userData.firstName,
        lastName: userData.lastName,
        email: userData.email,
        mobileNo: userData.mobileNo,
      }),
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        console.log('Update response:', data);

        if (data) {
          Swal.fire({
            title: 'Update Success!',
            icon: 'success',
            text: 'User Details Successfully Updated!',
          });

          closeEdit();
        } else {
          Swal.fire({
            title: 'Update Error!',
            icon: 'error',
            text: 'Please try again!',
          });
        }
      })
      .catch((error) => {
        console.error('Error updating user:', error);
        Swal.fire({
          title: 'Update Error!',
          icon: 'error',
          text: 'An error occurred while updating the user.',
        });
      });
  };

  return (
    <>
      <Button variant="primary" size="sm" onClick={openEdit}>
        Edit
      </Button>

      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={editUser}>
          <Modal.Header closeButton>
            <Modal.Title>{userData.firstName} {userData.lastName} </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="userFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                name="firstName"
                value={userData.firstName}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="userLastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                name="lastName"
                value={userData.lastName}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="userEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                name="email"
                value={userData.email}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="userMobileNo">
              <Form.Label>Mobile No</Form.Label>
              <Form.Control
                type="text"
                name="mobileNo"
                value={userData.mobileNo}
                onChange={handleInputChange}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
