import React from 'react';
import { Card, Container, Row, Col } from 'react-bootstrap';
import Image from 'react-bootstrap/Image';
import userImage from '../../data/user.png';
import orderImage from '../../data/order.png';
import cancelImage from '../../data/cancel.png';


const AdminDashboard = () => {
  return (
    <Container fluid>
     <h1 className="mb-5 mt-3 text-center">ADMIN DASHBOARD</h1> 
      <Row className="justify-content-between">

      {/*First*/}
        <Col md={3}>
          <Card>
            <Card.Body>
              <Container fluid>
                <Row>
                  <Col>
                    <Card.Title><span id="card-admin-values" className="card-value">20</span></Card.Title>        
                  </Col>
                  
                  <Col>
                    
                      <Image fluid src={userImage} size={25} /> 
                    
                  </Col>
                </Row>

                <Row>
                  <Col><Card.Text><span className="card-name">User</span></Card.Text></Col>
                </Row>
              </Container>
              
              
            </Card.Body>
          </Card>
        </Col>



       {/*Second*/}
        <Col md={3}>
          <Card>
            <Card.Body>
              <Container fluid>
                <Row>
                  <Col>
                    <Card.Title><span id="card-admin-values" className="card-value">20</span></Card.Title>        
                  </Col>
                  
                  <Col>
                    
                      <Image fluid src={orderImage} size={25} /> 
                    
                  </Col>
                </Row>

                <Row>
                  <Col><Card.Text><span className="card-name">Order</span></Card.Text></Col>
                </Row>
              </Container>
              
              
            </Card.Body>
          </Card>
        </Col>



        {/*Third*/}
        <Col md={3}>
          <Card>
            <Card.Body>
              <Container fluid>
                <Row>
                  <Col>
                    <Card.Title><span id="card-admin-values" className="card-value">20</span></Card.Title>        
                  </Col>
                  
                  <Col>
                    
                     <Image fluid src={cancelImage} size={25} /> 
                  </Col>
                </Row>

                <Row>
                  <Col><Card.Text><span className="card-name">Cancelled</span></Card.Text></Col>
                </Row>
              </Container>
              
              
            </Card.Body>
          </Card>
        </Col>




        {/*Fourth*/}
        <Col md={3}>
          <Card>
            <Card.Body>
              <Container fluid>
                <Row>
                  <Col>
                    <Card.Title><span id="daily-revenue-values" className="card-value">$5000</span></Card.Title>        
                  </Col>
                
                
                {/*<Col>    
                // <Image fluid src={cancelImage} size={25} /> 
                // </Col>*/}  
                
                </Row>

                <Row className="mt-3 mb-1">
                  <Col><Card.Text><span className="card-name">Daily Revenue</span></Card.Text></Col>
                </Row>
              </Container>
              
              
            </Card.Body>
          </Card>
        </Col>

     
      </Row>
    </Container>
  );
};

export default AdminDashboard;
