import React, { useState } from 'react';
import { Table, Dropdown } from 'react-bootstrap';
import { FaCog } from 'react-icons/fa';


const RecentOrdersTable = () => {
  const [orders, setOrders] = useState([
    {
      id: 1,
      productName: 'Product 1',
      qty: 5,
      orderDate: '2023-10-07',
      totalAmount: 100,
      status: 'Pending',
    },
 
  ]);

  const handleRemoveOrder = (orderId) => {
  
  };

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Order ID</th>
          <th>Product Name</th>
          <th>Qty</th>
          <th>Order Date</th>
          <th>Total Amount</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {orders.slice(0, 10).map((order) => (
          // First Row
          <tr key={order.id}>
            <td>{order.id}</td>
            <td>{order.productName}</td>
            <td>{order.qty}</td>
            <td>{order.orderDate}</td>
            <td>${order.totalAmount}</td>
            <td>{order.status}</td>
            <td>
              <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  <FaCog /> {/* Use the settings icon here */}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item href="#">View</Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => handleRemoveOrder(order.id)}
                  >
                    Remove
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default RecentOrdersTable;