import React, { useState } from 'react';
import { Heart } from 'react-bootstrap-icons';
import { Badge, Offcanvas } from 'react-bootstrap';

export default function WishlistComponent() {
  const [show, setShow] = useState(false);
  const [wishlistItems, setWishlistItems] = useState([]); // State to store wishlist items

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const removeFromWishlist = (productId) => {
    // Remove the product from the wishlist (e.g., based on productId)
    // Update the wishlistItems state and trigger a re-render
    setWishlistItems(wishlistItems.filter((item) => item !== productId));
  };

  const addToWishlist = (productId) => {
    // Add the product to the wishlist (e.g., based on productId)
    // Update the wishlistItems state and trigger a re-render
     
    // Check if the product is already in the wishlist to avoid duplicates
    if (!wishlistItems.includes(productId)) {
      // Add the product to the wishlist by updating the state
      setWishlistItems([...wishlistItems, productId]);
    } else {
      removeFromWishlist(productId)
    };
  };
  
  return (
    <div>
      <li
        onClick={handleShow}
        style={{
          position: "absolute", top: "7px",
          textDecoration: "none",
          backgroundColor: "transparent",
          position: "relative",
        }}
      >
        <Heart size={20} />
        <Badge pill style={{ position: "absolute", top: "18px", left: "7px", fontSize: "10px" }}
          variant="secondary" className="mr-2"
        >
          {wishlistItems.length} {/* Display the wishlist item count */}
        </Badge>
      </li>

      <Offcanvas show={show} onHide={handleClose} key={'end'} placement={'end'} name={'end'}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Wishlist</Offcanvas.Title>
        </Offcanvas.Header>

        <Offcanvas.Body>
          {wishlistItems.length === 0 ? (
            "No products here yet..."
          ) : (
            <ul>
              {wishlistItems.map((productId) => (
                <li key={productId}>
                  {/* Display wishlist item details */}
                  <img src='#'/>
                  <span>Product Name: </span>
                  <span>Product ID: {productId}</span>
                  <button onClick={() => removeFromWishlist(productId)}>Remove</button>
                </li>
              ))}
            </ul>
          )}
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  );
}
