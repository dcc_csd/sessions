import React from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import '../../App.css';

export default function Dashboard() {
  const email = "sample@email.com";

  return (
    <div>
      <p>The following addresses will be used on the checkout page by default.</p>

      <Container fluid className="my-4">
        <Row>
          <Col className="mt-2">
            <div className="d-flex mb-3">
              
                <h5>Billing address</h5>
                <Button className="bg-white btn-outline-dark text-dark ms-auto">Add</Button>
              
            </div>
            <p>You have not set up this type of address yet.</p>
          </Col>

          <Col className="mt-2">
            <div className="d-flex mb-3">
              
                <h5>Shipping address</h5>
                <Button className="bg-white btn-outline-dark text-dark ms-auto">Add</Button>
              
            </div>
            <p>You have not set up this type of address yet.</p>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
