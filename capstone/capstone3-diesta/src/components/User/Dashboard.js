import React from 'react';
import '../../App.css';

export default function Dashboard() {
  const email = "sample@email.com";


  return (
    <div>
      <p>Hello <strong>{email},</strong> (Not <strong>{email}</strong>? Click <a href="/logout">here</a> to proceed in logging out.)</p>
      <p>From your account dashboard you can view your recent orders, manage your shipping and billing address, and edit your password and account details.</p>
    </div>
  );
}
