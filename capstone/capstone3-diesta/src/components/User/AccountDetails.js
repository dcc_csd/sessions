import React from 'react';
import {Form, Button} from 'react-bootstrap';

function FormFloatingCustom() {
  return (
    <>
      <Form>
      <Form.Label htmlFor="inputPassword" className="mt-3">Account Details</Form.Label>
        <Form.Group>
        {/*<Form.Label htmlFor="inputFirstName" >First Name</Form.Label>*/}
        <Form.Floating className="mb-3">
          <Form.Control  size="md" 
            id="floatingFirstName"
            type="text"
            placeholder="John"
          />
          <label htmlFor="floatingInputCustom">First Name</label>
        </Form.Floating>
        </Form.Group>
        
        <Form.Group>
        {/*<Form.Label htmlFor="inputLastName" >Last Name</Form.Label>*/}
        <Form.Floating>
          <Form.Control
            id="floatingLastName"
            type="text"
            placeholder="Doe"
          />
          <label htmlFor="floatingInputCustom">Last Name</label>
        </Form.Floating>
        </Form.Group>

        <Form.Group>
        {/*<Form.Label htmlFor="inputDisplayName" >Display Name</Form.Label>*/}
        <Form.Floating  className="mt-3">
          <Form.Control
            id="floatingDisplayName"
            type="text"
            placeholder="JohnDoeReal"
          />
          <label htmlFor="floatingDisplayName">Display Name</label>
        </Form.Floating>
        <Form.Text id="passwordHelpBlock" muted>
          This will be how your name will be displayed in the account section and in product reviews.
        </Form.Text>

        </Form.Group>

        <Form.Group>
        {/*<Form.Label htmlFor="inputDisplayName" >Display Name</Form.Label>*/}
        <Form.Floating  className="mt-3">
          <Form.Control
            id="floatingEmailAddress"
            type="email"
            placeholder="john.D@example.com"
          />
          <label htmlFor="floatingDisplayName">Email Address</label>
        </Form.Floating>
        </Form.Group>

        <Form.Group>
        <Form.Label htmlFor="inputPassword" className="mt-5">Password Change</Form.Label>
        <Form.Control
          type="password"
          id="inputPassword5"
          aria-describedby="passwordHelpBlock"
        />
        <Form.Text id="passwordHelpBlock" muted>
          Your password must be 8-20 characters long, contain letters and numbers,
          and must not contain spaces, special characters, or emoji.
        </Form.Text>
        </Form.Group>

        <Button variant="secondary" type="submit" className="mt-4">
        Submit
      </Button>
      </Form>
    </>
  );
}

export default FormFloatingCustom;