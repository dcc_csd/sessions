import React, { useContext, useState, useEffect } from 'react';
import { Table, Dropdown } from 'react-bootstrap';
import { BsThreeDotsVertical } from 'react-icons/bs';
import UserContext from './../../UserContext';
import { useNavigate } from 'react-router-dom';

export default function OrderTable() {
  const [orders, setOrders] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetch('https://capstone2-diesta.onrender.com/order', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log('Fetched data:', data);
        setOrders(data.orders); // Extract the 'orders' array from the fetched data
      })
      .catch((error) => console.error('Error fetching orders:', error));
  }, []);

  console.log('Orders state:', orders);

  if (!orders || !orders.length) {
    return <p>No orders available.</p>;
  }

  return (
    <>
      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>Order ID</th>
            <th>Date Ordered</th>
            <th>Total Amount</th>
            <th>Status</th>
            {/* Add more table headers as needed for your order schema */}
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order) => (
            <tr key={order._id}>
              <td>{order._id}</td>
              <td>{order.dateOrdered}</td>
              <td>{order.totalPrice}</td>
              <td className={`text-${getStatusColor(order.status)}`}>
                {order.status}
              </td>
              
              <td>
                <Dropdown>
                  <Dropdown.Toggle variant="success" id="dropdown-basic">
                    <BsThreeDotsVertical />
                  </Dropdown.Toggle>
                  <Dropdown.Menu align="right">
                    <Dropdown.Item onClick={() => navigate(`/order-details/${order._id}`)}>
                      View Details
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

function getStatusColor(status) {
  switch (status) {
    case 'Pending':
      return 'success'; // Green
    case 'On-Delivery':
      return 'primary'; // Blue
    case 'Cancelled':
      return 'danger'; // Red
    case 'Completed':
      return 'dark'; // Black
    default:
      return 'dark';
  }
}
