import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function ArchiveCourse({ courseId, handleArchiveCourse, isActive }) {
  const [showModal, setShowModal] = useState(false);

  const handleArchiveClick = () => {
    setShowModal(true);
  };

  const handleConfirmAction = () => {
    const newIsActive = !isActive;
    handleArchiveCourse(courseId, newIsActive);

    setShowModal(false);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <Button variant={isActive ? "danger" : "success"} onClick={handleArchiveClick}>
        {isActive ? "Archive" : "Restore"}
      </Button>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm {isActive ? "Archive" : "Restore"}</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to {isActive ? "archive" : "restore"} this course?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Cancel
          </Button>
          <Button variant={isActive ? "danger" : "success"} onClick={handleConfirmAction}>
            Yes, {isActive ? "Archive" : "Restore"}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
