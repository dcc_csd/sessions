import React from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import {
  PiTruckDuotone,
  PiHeadsetDuotone,
  PiMoneyDuotone,
} from 'react-icons/pi';

import './highlight.css'; 

export default function Highlights() {
  return (
  <Container fluid>
    <Row className="mt-3 mb-3 justify-content-center">
      <Col xs={12} md={6} lg={4}>
        <Card className="cardHighlight p-2">
          <Card.Body>
            <Container fluid>
              <Row>
                {/* Image */}
                <Col xs={3} sm={2} md={4}>
                  <PiTruckDuotone className="icon-dashboard text-dark" />
                </Col>

                
                <Col xs={9} sm={10} md={8} className="border-start">
                  <Card.Title>
                    <h6>FREE SHIPPING</h6>
                    <p className = "font mt-3">On All Orders</p>
                  </Card.Title>
               
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </Card>
      </Col>

      {/*small and extra small screen size*/}
      <Col xs={12} md={0}  className="my-3 d-block d-md-none">
        <Card className="cardHighlight p-2">
          <Card.Body>
            <Container fluid>
              <Row>
                {/* Image */}
                <Col xs={3} sm={2} md={4}>
                  <PiHeadsetDuotone className="icon-dashboard text-dark" />
                </Col>

                {/* Text */}
                <Col xs={9} sm={10} md={8} className="border-start">
                  <Card.Title>
                    <h6>24/7 SUPPORT</h6>
                    <p className = "font mt-3">Customer Service</p>
                  </Card.Title>
                  
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </Card>
      </Col>

      {/*Middle on medium to xxl screen*/}
      <Col xs={12} md={6} lg={4}  className="d-none d-md-block">
        <Card className="cardHighlight p-2">
          <Card.Body>
            <Container fluid>
              <Row>
                {/* Image */}
                <Col xs={3} sm={2} md={4}>
                  <PiHeadsetDuotone className="icon-dashboard text-dark" />
                </Col>

                {/* Text */}
                <Col xs={9} sm={10} md={8} className="border-start">
                  <Card.Title>
                    <h6>24/7 SUPPORT</h6>
                    <p className = "font mt-3">Customer Service</p>
                  </Card.Title>
                  
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </Card>
      </Col>

      {/* medium */}
      <Col xs={12} md={12} className="d-none d-md-block d-lg-none mt-md-3 mt-lg-0">
        <Card className="cardHighlight p-2">
          <Card.Body>
            <Container fluid>
              <Row>
                {/* Image */}
                <Col xs={3} sm={2} md={4}>
                  <PiMoneyDuotone className="icon-cash-medium text-dark" />
                </Col>

                {/* Text */}
                <Col xs={9} sm={10} md={8} className="border-start">
                  <Card.Title>
                    <h6>100% RETURN</h6>
                    <p className = "font mt-3">Within 30 Days</p>
                  </Card.Title>
                  
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </Card>
      </Col>


      <Col lg={4} className="d-md-none d-lg-block mt-md-3 mt-lg-0">
        <Card className="cardHighlight p-2">
          <Card.Body>
            <Container fluid>
              <Row>
                {/* Image */}
                <Col xs={3} sm={2} md={4}>
                  <PiMoneyDuotone className="icon-dashboard  text-dark" />
                </Col>

                {/* Text */}
                <Col xs={9} sm={10} md={8} className="border-start">
                  <Card.Title>
                    <h6>100% RETURN</h6>
                    <p className = "font mt-3">Within 30 Days</p>
                  </Card.Title>
                  
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </Card>
      </Col>


    </Row>
  </Container>
  );
}