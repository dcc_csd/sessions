import React, { useState } from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { PiMagnifyingGlassLight} from 'react-icons/pi';

import Carousel from 'react-bootstrap/Carousel';
import Image from 'react-bootstrap/Image';

import '../../App.css';


const carouselItems = [
  {
    id: 1,
    imageSrc: 'https://as2.ftcdn.net/v2/jpg/05/92/78/05/500_F_592780502_d5uMTecbLIIC8wJhJ010XqWeE5xvaYBZ.jpg', 
    altText: 'Image 1',
  },
  {
    id: 2,
    imageSrc: 'https://img.freepik.com/premium-photo/portrait-two-young-fashion-model-woman-black-evening-gown-hair-style-looking-idea-pointing-left-side-copy-space-center-area-text-ads-studio-lighting-beige-yellow-background_121764-1766.jpg?size=626&ext=jpg&ga=GA1.1.679654178.1684464640&semt=ais',
  },
  {
    id: 3,
    imageSrc: 'https://www.dropcents.com/blog/wp-content/uploads/2012/10/89_Clothing-Banner-500x215.jpeg', 
    altText: 'Image 3',
  },
];

export default function FrontAd({ onSearch }) {
  const [searchQuery, setSearchQuery] = useState('');

  const handleSearch = () => {
    onSearch(searchQuery);
  };
 
 const handleFormSubmit = (e) => {
    e.preventDefault(); 
    onSearch(searchQuery);
  };


  return (
    <>
        <Container fluid>
            <Row>
                <Col>
                  <InputGroup className="mb-3">
                  <Form.Control
                    type="text"
                    placeholder="Search Products Here"
                    value={searchQuery}
                    onChange={(e) => setSearchQuery(e.target.value)}
                    onKeyPress={(e) => {
                      if (e.key === 'Enter') {
                        handleSearch();
                      }
                    }}
                  />
                  <Button variant="dark" onClick={handleSearch}>
                    <PiMagnifyingGlassLight />
                  </Button>
                </InputGroup>

                </Col>
            </Row>
            <Row>
                <Col>
                    <Carousel fade pause indicators={false}>
                    {carouselItems.map((item) => (
                        <Carousel.Item interval={10000} key={item.id}>
                            <img className="d-block w-100"
                                src={item.imageSrc}
                                alt={item.altText}/>

                            <Carousel.Caption>
                                <div><h5>New Summer 2023</h5></div>
                                <div><h1>Women Events</h1></div>
                                <div><p className="">Nulla vitae elit libero, a pharetra augue mollis interdum.</p></div>
                            </Carousel.Caption>
                        </Carousel.Item>
                            ))}
                    </Carousel>
                </Col>      
            </Row>
        </Container>

    </>
  );
}



