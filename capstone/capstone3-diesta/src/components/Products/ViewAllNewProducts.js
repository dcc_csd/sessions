// ViewAllNewProducts.js
import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

export default function ViewAllNewProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    // Fetch and set all new products here
  }, []);

  return (
    <div>
      <h5>All New Products</h5>
      {products.map((product) => (
        <div key={product._id}>
          <Link to={`single-product/${product._id}`}>
            <Card>
              {/* Render product information here */}
            </Card>
          </Link>
        </div>
      ))}
    </div>
  );
}
