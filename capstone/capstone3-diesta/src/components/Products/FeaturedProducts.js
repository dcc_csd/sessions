import React from 'react';
import Card from 'react-bootstrap/Card';
import { TbChevronRight } from "react-icons/tb";


import '../../App.css';

export default function FeaturedProducts() {

    const newProducts = [
    {
    	id: 1,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 500.00,
    },
    {
        id: 2,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 350.00,
    },
    {
        id: 3,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 999.00,
    },
    {
        id: 4,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 59.99,
    },
    {
        id: 5,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 1.00,
    },

    ]

    const popularProducts = [
    {
        id: 1,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 500.00,
    },
    {
        id: 2,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 350.00,
    },
    {
        id: 3,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 999.00,
    },
    {
        id: 4,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 59.99,
    },
    {
        id: 5,
        imgSrc: "https://cdn.shopify.com/s/files/1/0070/7032/files/image5_4578a9e6-2eff-4a5a-8d8c-9292252ec848.jpg?v=1620247043",
        title: "Hair Growth Serum",    
        price: 1.00,
    },

    ]

    return (
        <>
            {/*New Products*/}
            <div>
                <div className="d-flex mb-2">
                    <h5 className="border-bottom border-dark border-3">New Products</h5>
                    <p className="ms-auto"><a href="#" className="text-secondary">View All <TbChevronRight/></a></p>
                </div>
                <div className="d-flex gap-1">
                    {newProducts.map((products) => (
                        <Card className="py-2 m-0">
                            <Card.Img key ={products.id} variant="top" src={products.imgSrc} />
                                <Card.Body>
                                    <Card.Subtitle className="text-secondary">{products.title}</Card.Subtitle>
                                    <Card.Text className="mt-2"><h6>P{products.price}</h6></Card.Text>
                                </Card.Body>
                        </Card>
                    ))}
                </div>
            </div>

            {/*Popular Products*/}
            <div className="mt-5">
                <div className="d-flex mb-2">
                    <h5 className="border-bottom border-dark border-3">Popular Products</h5>
                    <p className="ms-auto"><a href="#" className="text-secondary">View All <TbChevronRight/></a></p>
                </div>
                <div className="d-flex gap-1">
                    {popularProducts.map((products) => (
                        <Card className="py-2 m-0">
                            <Card.Img key ={products.id} variant="top" src={products.imgSrc} />
                                <Card.Body>
                                    <Card.Subtitle className="text-secondary">{products.title}</Card.Subtitle>
                                    <Card.Text className="mt-2"><h6>P{products.price}</h6></Card.Text>
                                </Card.Body>
                        </Card>
                    ))}
                </div>
            </div>
        

        </>
    )
}