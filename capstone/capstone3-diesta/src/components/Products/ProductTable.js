import React, { useState, useEffect } from 'react';
import { Table, Dropdown, FormControl } from 'react-bootstrap';
import { BsThreeDotsVertical } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

function ProductTable({ productData }) {
  const [products, setProducts] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [showCount, setShowCount] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);

  const navigate = useNavigate();

  useEffect(() => {
    // Fetch products from your MongoDB backend
    fetch('https://capstone2-diesta.onrender.com/product')
      .then((response) => response.json())
      .then((data) => setProducts(data))
      .catch((error) => console.error('Error fetching products:', error));
  }, []);

  const handleEditClick = (productId) => {
    // Use the navigate function to programmatically navigate.
    navigate(`/edit-product/${productId}`);
  };

  const handleShowCountChange = (count) => {
    setShowCount(count);
    setCurrentPage(1); // Reset to the first page when changing the number of entries
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderPagination = () => {
    const totalEntries = products.length;
    const entriesPerPage = parseInt(showCount, 10);
    const totalPages = Math.ceil(totalEntries / entriesPerPage);
    const pages = [];

    for (let i = 1; i <= totalPages; i++) {
      pages.push(
        <li key={i} className={`page-item ${currentPage === i ? 'active' : ''}`}>
          <a className="page-link" onClick={() => handlePageChange(i)}>
            {i}
          </a>
        </li>
      );
    }

    return (
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-end">
          <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
            <a className="page-link" onClick={() => handlePageChange(currentPage - 1)}>
              Previous
            </a>
          </li>
          {pages}
          <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
            <a className="page-link" onClick={() => handlePageChange(currentPage + 1)}>
              Next
            </a>
          </li>
        </ul>
      </nav>
    );
  };

  const filteredProducts = products.filter((product) => {
    return (
      product.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
      product.description.toLowerCase().includes(searchTerm.toLowerCase())
    );
  });

  const startIndex = (currentPage - 1) * showCount;
  const endIndex = startIndex + showCount;
  const displayedProducts = filteredProducts.slice(startIndex, endIndex);

  return (
    <>
      <div>
        <div className="d-flex justify-content-between align-items-center mb-3">
          <FormControl
            type="text"
            placeholder="Search by Name or Description"
            style={{ width: '350px', marginBottom: '-10px' }}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <div className="d-flex align-items-center">
            <div>
              Show
              <select
                className="ms-2"
                onChange={(e) => handleShowCountChange(e.target.value)}
                value={showCount}
              >
                <option value={5}>5</option>
                <option value={10}>10</option>
                <option value={15}>15</option>
              </select>
              entries
            </div>
          </div>
        </div>
      </div>
      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>Image</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {displayedProducts.map((product) => (
            <tr key={product._id}>
              <td>
                {product.images && product.images.length > 0 ? (
                  <img
                    src={product.images[0].url}
                    alt={product.name}
                    style={{
                      width: '120px',
                      height: '120px',
                    }}
                  />
                ) : (
                  <div>Image</div>
                )}
              </td>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td style={{ width: '150px' }}> ${product.price}</td>
              <td className={product.isActive ? 'text-success' : 'text-danger'}>
                {product.isActive ? 'Active' : 'Unavailable'}
              </td>
              <td>
                <Dropdown>
                  <Dropdown.Toggle variant="success" id="dropdown-basic">
                    <BsThreeDotsVertical />
                  </Dropdown.Toggle>
                  <Dropdown.Menu align="right">
                    <Dropdown.Item href="add-product">Add</Dropdown.Item>
                    <Dropdown.Item onClick={() => handleEditClick(product._id)}>
                      Edit
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {renderPagination()}
    </>
  );
}

export default ProductTable;
