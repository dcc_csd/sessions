import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function EditProduct() {
  const { productId } = useParams();

  const [imagePreview, setImagePreview] = useState('');
  const [productData, setProductData] = useState({
    name: '',
    codeNo: '',
    description: '',
    slug: '',
    category: '',
    price: '',
    stockQty: '',
    discountPrice: '',
    brand: '',
    isActive: '',
    isSale: '', 
    isFeatured: '', 
    image: '',
  });

  useEffect(() => {
    const fetchProductData = async () => {
      try {
        const response = await fetch(`https://capstone2-diesta.onrender.com/product/${productId}`);
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const product = await response.json();
        setProductData(product);
      } catch (error) {
        console.error('Error fetching product:', error);
      }
    };

    fetchProductData();
  }, [productId]);

  const handleInputChange = (e) => {
    const { name, value, type, checked } = e.target;
    const newValue = type === 'checkbox' ? checked : value;

    setProductData({
      ...productData,
      [name]: newValue,
    });
  };

const handleImageChange = (e) => {
  const file = e.target.files[0];
  setProductData({
    ...productData,
    image: file,
  });

  // Read and preview the selected image
  if (file) {
    const reader = new FileReader();
    reader.onload = (e) => {
      setImagePreview(e.target.result);
    };
    reader.readAsDataURL(file);
  }
};

  const saveProduct = (e) => {
    e.preventDefault();

    const formData = new FormData();
  for (const key in productData) {
    if (key === 'image') {
      // If the key is 'image', append it differently
      formData.append('image', productData.image);
    } else {
      formData.append(key, productData[key]);
    }
  }

    fetch(`https://capstone2-diesta.onrender.com/product/${productId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: formData,
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        console.log('Update response:', data);
        if (data) {
          Swal.fire({
            title: 'Update Success!',
            icon: 'success',
            text: 'Product Details Successfully Updated!',
          }).then(() => {
            // No need for navigation here; rely on a Link component for navigation.
          });
        } else {
          Swal.fire({
            title: 'Update Error!',
            icon: 'error',
            text: 'Please try again!',
          });
        }
      })
      .catch((error) => {
        console.error('Error updating product:', error);
        Swal.fire({
          title: 'Update Error!',
          icon: 'error',
          text: 'An error occurred while updating the product.',
        });
      });
  };

  return (
    <Container className="mx-auto my-3 border">
      <Row className="bg-dark text-white border-bottom">
        <h1 className="ms-5 my-4">Product Details</h1>
      </Row>
      <Row className="d-flex flex-column px-5 py-2 my-3">
        <form onSubmit={saveProduct} className="">
          <Col className="d-flex flex-column gap-3">
          
            {/* Name */}
              <Row class="d-flex mb-4">
                <Col xs={5} sm={4} md={3}>
                  <label>Name:</label>
                </Col>

                <Col xs={7} sm={8} md={9}>
                  <input class="w-100" type="text" name="name" value={productData.name} onChange={handleInputChange}/>
                </Col>
              </Row>

            {/* Code No. */}
              <Row class="d-flex mb-4">
                <Col xs={5} sm={4} md={3}>
                  <label>Code No:</label>
                </Col>

                <Col xs={7} sm={8} md={9}>
                  <input class="w-100" type="text" name="codeNo" value={productData.codeNo} onChange={handleInputChange}/>
                </Col>
              </Row>
                  
            {/* Description */}
              <Row class="d-flex mb-4">
                      <Col xs={5} sm={4} md={3}>
                        <label>Description:</label>
                      </Col>

                      <Col xs={7} sm={8} md={9}>
                        <input class="w-100" type="text" name="description" value={productData.description} onChange={handleInputChange}/>
                      </Col>
              </Row>

            {/* Slug */}
              <Row class="d-flex mb-4">
                      <Col xs={5} sm={4} md={3}>
                        <label>Slug:</label>
                      </Col>

                      <Col xs={7} sm={8} md={9}>
                        <input class="w-100" type="text" name="slug" value={productData.slug} onChange={handleInputChange}/>
                      </Col>
              </Row>

            {/* Category */}
              <Row class="d-flex mb-4">
                      <Col xs={5} sm={4} md={3}>
                        <label>Category:</label>
                      </Col>

                      <Col xs={7} sm={8} md={9}>
                        <input class="w-100" type="text" name="category" value={productData.category} onChange={handleInputChange}/>
                      </Col>
              </Row>

            {/* Checkboxes */}
              <Row class="d-flex mb-4">
                      <Col xs={0} sm={4} md={3} lg={5}>

                      </Col>

                      <Col xs={12} sm={8} md={9} lg={7}>
                        <div className="d-flex">
                          <div className="d-flex gap-2 ms-5 ms-sm-2 ms-md-5 ms-lg-3 ms-xl-5">
                            <label className="ms-md-2 ms-md-0">Active:</label>
                            <input type="checkbox" name="isActive" checked={productData.isActive} onChange={handleInputChange}/>
                          </div>

                          <div className="d-flex gap-2 mx-3 mx-md-5">
                            <label>Featured:</label>
                            <input type="checkbox" name="isFeatured" checked={productData.isFeatured} onChange={handleInputChange}/>
                          </div>

                          <div className="d-flex gap-2">
                            <label>Sale:</label>
                            <input type="checkbox" name="isSale" checked={productData.isSale} onChange={handleInputChange}/>
                          </div>
                        </div>
                      </Col>
              </Row>

            {/* Price */}
              <Row class="d-flex mb-4">
                      <Col xs={5} sm={4} md={3}>
                        <label>Price:</label>
                      </Col>

                      <Col xs={7} sm={8} md={9}>
                        <input class="w-100" type="number" name="price" value={productData.price} onChange={handleInputChange}/>
                      </Col>
              </Row>

            {/* Discount Price */}
              <Row class="d-flex mb-4">
                      <Col xs={5} sm={4} md={3}>
                        <label>Discount Price:</label>
                      </Col>

                      <Col xs={7} sm={8} md={9}>
                        <input class="w-100" type="number" name="discountPrice" value={productData.discountPrice} onChange={handleInputChange}/>
                      </Col>
              </Row>

            {/* Stock Qty */}
              <Row class="d-flex mb-4">
                      <Col xs={5} sm={4} md={3}>
                        <label>Stock Qty:</label>
                      </Col>

                      <Col xs={7} sm={8} md={9}>
                        <input class="w-100" type="number" name="stockQty" value={productData.stockQty} onChange={handleInputChange}/>
                      </Col>
              </Row>

            {/* Brand */}
              <Row class="d-flex mb-4">
                      <Col xs={5} sm={4} md={3}>
                        <label>Brand:</label>
                      </Col>

                      <Col xs={7} sm={8} md={9}>
                        <input class="w-100" type="text" name="brand" value={productData.brand} onChange={handleInputChange}/>
                      </Col>
              </Row>

              <div className="d-flex flex-column">
                   
                      <label>Upload Image:</label>
                      <input type="file" accept="image/*" name="image" onChange={handleImageChange}/>
                      {/* Display the image preview */}
                      {imagePreview && (
                        <img className="mx-auto" src={imagePreview} alt="Product Thumbnail" style={{ maxWidth: '200px' }} />
                      )}
                      
              </div>
                
              <div className="d-flex">
            
                {/* Use Link component to navigate back to "All Products" page */}
                
                <Link to="/all-product" className="me-auto">
                  <Button variant="secondary">Back to All Products</Button>
                </Link>

                <Button variant="success" type="submit">
                Submit
                </Button>
              </div>

          
          </Col>
        </form>



      </Row>
    </Container>
  );
}