import React, { useContext, useState } from 'react';
import { Person, Cart4, Heart } from "react-bootstrap-icons";
import {Container, Navbar, Nav, NavDropdown, Button, Badge, Offcanvas } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import WishlistComponent from "../components/User/WishlistComponent.js";


import shoplogo2Image from "../components/Admin/Sidebar/shoplogo2.png";
import shoplogo1Image from "../components/Admin/Sidebar/shoplogo1.png";



export default function AppNavbar({size}) {
  const { user } = useContext(UserContext);
  const [show, setShow] = useState(true);
  const [cart, setCart] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <Navbar bg="light" expand="sm">
      <Container fluid className="justify-content-between">
        <Navbar.Brand as={Link} to="/" className="ms-3">
          <img src={shoplogo1Image} alt="Logo" height="30" />
          <img src={shoplogo2Image} alt="Logo" height="50" />

        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-center">
          <Nav>
            
            {user.id !== null ? (
              <>
                {user.isAdmin ? (
                  

                  //admin   
                  <>
                    <Nav.Link as={NavLink} to="/admin-dashboard" exact>DASHBOARD</Nav.Link>
                    <Nav.Link as={NavLink} to="/" exact>HOME</Nav.Link>
                    <Nav.Link as={NavLink} to="/product" exact>PRODUCTS</Nav.Link>
                    <Nav.Link as={NavLink} to="/order-list" exact>ORDER</Nav.Link>
                    
                  </>


                ) : (

                //user

                  <>
                    
                    <Nav.Link as={NavLink} to="/" exact>HOME</Nav.Link>
                    <Nav.Link as={NavLink} to="/product" exact>PRODUCTS</Nav.Link>
                    <Nav.Link as={NavLink} to="/wishlist" exact>WISHLIST</Nav.Link>
                    <Nav.Link as={NavLink} to="/user-cart-view/:userID" exact>CART</Nav.Link>
                    <Nav.Link as={NavLink} to="/faqs" exact>FAQ</Nav.Link>          
                 
                  </>
                )}
                           
              </>
          
            //visitors
            ) : (
           
             < >

              <Nav.Link as={NavLink} to="/" exact>HOME</Nav.Link>          
              <Nav.Link as={NavLink} to="/product" exact>PRODUCTS</Nav.Link>
              <Nav.Link as={NavLink} to="/faqs" exact>FAQ</Nav.Link>
              <Nav.Link as={NavLink} to="/register" exact>REGISTER</Nav.Link>
              <Nav.Link as={NavLink} to="/login" exact>LOGIN</Nav.Link>
              
            </>

            )}
        

            {user.id !== null ? (

            <>
                {!user.isAdmin ? (
                  <>
                  <div>
                     <NavDropdown title={<Person size={28} style={{ position: "absolute", top: "5px", right: "-10px", fontSize: "10px" }} />} id="person-dropdown">
                     <NavDropdown.Item as={NavLink} to="/profile" exact>
                      Profile
                     </NavDropdown.Item>
                     
                     <NavDropdown.Item as={NavLink} to="/user-order2/:userID" exact>
                      Orders
                    </NavDropdown.Item>
                    
                    <NavDropdown.Item as={NavLink} to="/logout" exact>
                      Logout
                    </NavDropdown.Item>
                    
                    </NavDropdown>
                 </div>

                </>
                ) : (
                  <>
                    <Nav.Link as={NavLink} to="/profile" exact>PROFILE</Nav.Link>
                    
                  </>
                )}
                
              </>
            ) : (
              <>
              </>
            
            )}
          
          </Nav>
        
        </Navbar.Collapse>

     <div className="d-flex align-items-center"> {/* Create a flex container */}
      <Nav className="d-flex flex-row ms-3">
       <span className= "wish" size={29} style={{ position: "absolute", top: "20px", right: "60px", fontSize: "10px" }}>
       <WishlistComponent  /> 
       </span>
       <Cart4 className= "cart" size={29} style={{ position: "absolute", top: "25px", right: "100px", fontSize: "10px" }}/>
        
       <span className= "size" size={29} style={{ position: "absolute", top: "30px", right: "90px", fontSize: "15px" }}>
       {size}
       </span>


    </Nav>
    </div>
  </Container>
</Navbar>
  );
}
