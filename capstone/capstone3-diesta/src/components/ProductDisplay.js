import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';
import '../App.css';

const productItems = [
  {
    id: 1,
    imageSrc: 'https://mountstudio.com.sg/wp-content/uploads/campaign-fashion-photography-zalora-3.jpg',
    tag: 'Collection 2023',
    category: "MEN'S",
    link: 'https://example.com/mens'
  },
  {
    id: 2,
    imageSrc: 'https://img.freepik.com/free-photo/three-young-beautiful-smiling-girls-trendy-summer-casual-jeans-clothes-sexy-carefree-women-posing-positive-models-sunglasses_158538-4730.jpg?size=626&ext=jpg&ga=GA1.1.679654178.1684464640&semt=ais',
    tag: 'Collection 2023',
    category: "WOMEN'S",
    link: 'https://example.com/mens'
  },
  {
    id: 3,
    imageSrc: 'https://img.freepik.com/free-photo/group-beautiful-girls-boys-pastel-wall_155003-10578.jpg?size=626&ext=jpg&ga=GA1.1.679654178.1684464640&semt=ais',
    tag: 'Collection 2023',
    category: "KID'S WEAR",
    link: 'https://example.com/mens'
  },
  {
    id: 4,
    imageSrc: 'https://img.freepik.com/free-vector/classic-women-men-shoes-realistic-light-wood-floor-background-with-red-high-heels-pink-pumps-illustration_1284-65187.jpg?size=626&ext=jpg&ga=GA1.1.679654178.1684464640&semt=ais',
    tag: 'Collection 2023',
    category: "SHOES",
    link: 'https://example.com/mens'
  },
  {
    id: 5,
    imageSrc: 'https://mountstudio.com.sg/wp-content/uploads/campaign-fashion-photography-zalora-3.jpg',
    tag: 'Collection 2023',
    category: "SPORTS",
    link: 'https://example.com/mens'
  },
  {
    id: 6,
    imageSrc: 'https://img.freepik.com/free-photo/model-career-kit-still-life_23-2150229753.jpg?size=626&ext=jpg&ga=GA1.1.679654178.1684464640&semt=ais',
    tag: 'Collection 2023',
    category: "GADGETS",
    link: 'https://example.com/mens'
  },
  {
    id: 7,
    imageSrc: 'https://mountstudio.com.sg/wp-content/uploads/campaign-fashion-photography-zalora-3.jpg',
    tag: 'Collection 2023',
    category: "ACCESSORIES",
    link: 'https://example.com/mens'
  },
  {
    id: 8,
    imageSrc: 'https://img.freepik.com/free-photo/cute-plush-toys-arrangement_23-2150312316.jpg?size=626&ext=jpg&ga=GA1.1.679654178.1684464640&semt=ais',
    tag: 'Collection 2023',
    category: "TOYS",
    link: 'https://example.com/mens'
  },
  {
    id: 9,
    imageSrc: 'https://img.freepik.com/free-photo/pretty-excited-girl-cozy-sweater-holding-gift-boxes-joyfully-looking-camera-white-background_574295-4268.jpg?size=626&ext=jpg&ga=GA1.1.679654178.1684464640&semt=ais',
    tag: 'Collection 2023',
    category: "GIFT CORNER",
    link: 'https://example.com/mens'
  },
  
];

export default function ProductDisplay() {
  const [activeIndexes, setActiveIndexes] = useState([0, 0, 0]);

  const handleSelect = (selectedIndex, columnIndex) => {
    const updatedIndexes = [...activeIndexes];
    updatedIndexes[columnIndex] = selectedIndex;

    setActiveIndexes(updatedIndexes);
  };

 return (
    <Container fluid>
      <Row>
        {/* First Column */}
        <Col md="6">
          <a href={productItems[0].link} target="_blank" rel="noopener noreferrer">
            <Carousel fade pause indicators={false} controls={false} activeIndex={activeIndexes[0]} onSelect={(selectedIndex) => handleSelect(selectedIndex, 0)}
            >
              {productItems.slice(0, 1, 2).map((item, index) => (
                <Carousel.Item interval={10000} key={item.id}>
                  <img className="d-block w-100 m-2" src={item.imageSrc} alt={item.category} />
                  <Carousel.Caption>
                    <div>
                      <h5>{item.category}</h5>
                      <p>{item.tag}</p>
                    </div>
                  </Carousel.Caption>
                </Carousel.Item>
              ))}
            </Carousel>
          </a>
        </Col>

        {/* Second Column */}
        <Col md="3">
          <a href={productItems[2].link} target="_blank" rel="noopener noreferrer">
            <Carousel fade pause indicators={false} controls={false} activeIndex={activeIndexes[1]} onSelect={(selectedIndex) => handleSelect(selectedIndex, 1)}>
              {productItems.slice(2, 3, 4).map((item, index) => (
                <Carousel.Item interval={10000} key={item.id} className="emboss-on-hover">
                  
                  <img className="d-block w-100 m-2" src={item.imageSrc} alt={item.category} />
                  <Carousel.Caption>
                    <div>
                      <h5>{item.category}</h5>
                      <p>{item.tag}</p>
                    </div>
                  </Carousel.Caption>
                </Carousel.Item>
              ))}
            </Carousel>
          </a>

          <a href={productItems[4].link} target="_blank" rel="noopener noreferrer">
            <Carousel className="" fade pause indicators={false} controls={false} activeIndex={activeIndexes[2]} onSelect={(selectedIndex) => handleSelect(selectedIndex, 2)}>
              {productItems.slice(4, 6).map((item, index) => (
                <Carousel.Item interval={10000} key={item.id} className="emboss-on-hover">
                  
                  <img className="d-block w-100 m-2" src={item.imageSrc} alt={item.category} />
                  <Carousel.Caption>
                    <div>
                      <h5>{item.category}</h5>
                      <p>{item.tag}</p>
                    </div>
                  </Carousel.Caption>
                </Carousel.Item>
              ))}
            </Carousel>
          </a>
        </Col>

        {/* Third Column */}
        <Col md="3">
          <a href={productItems[6].link} target="_blank" rel="noopener noreferrer">
            <Carousel fade pause indicators={false} controls={false} activeIndex={activeIndexes[3]} onSelect={(selectedIndex) => handleSelect(selectedIndex, 3)}>
              {productItems.slice(6, 8).map((item, index) => (
                <Carousel.Item interval={10000} key={item.id} className="emboss-on-hover">
                  
                  <img className="d-block w-100 m-2" src={item.imageSrc} alt={item.category} />
                  <Carousel.Caption>
                    <div>
                      <h5>{item.category}</h5>
                      <p>{item.tag}</p>
                    </div>
                  </Carousel.Caption>
                </Carousel.Item>
              ))}
            </Carousel>
          </a>

          <a href={productItems[8].link} target="_blank" rel="noopener noreferrer">
            <Carousel className="" fade pause indicators={false} controls={false} activeIndex={activeIndexes[4]} onSelect={(selectedIndex) => handleSelect(selectedIndex, 4)}>
              {productItems.slice(8, 9, 0).map((item, index) => (
                <Carousel.Item interval={10000} key={item.id} className="emboss-on-hover">
                  
                  <img className="d-block w-100 m-2" src={item.imageSrc} alt={item.category} />
                  <Carousel.Caption>
                    <div>
                      <h5>{item.category}</h5>
                      <p>{item.tag}</p>
                    </div>
                  </Carousel.Caption>
                </Carousel.Item>
              ))}
            </Carousel>
          </a>
        </Col>
      </Row>
    </Container>
  );
}