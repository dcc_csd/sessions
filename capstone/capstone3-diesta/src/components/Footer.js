import React from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { TbChevronRight } from "react-icons/tb";

import '../App.css';

export default function Footer() {

    return (
        
            <Container fluid className="bg-dark py-4 text-white">
                <Row className="my-4 d-flex justify-content-center align-items-center">
                    {/*1st*/}
                    <Col sm={5} className="d-flex flex-column w-25">
                        <img src="#" className="my-3"/>
                        <div className="d-flex gap-2 my-3">
                            <TbChevronRight/> <span className="text-white">Address Here 216531</span>
                        </div>
                        <div className="d-flex gap-2 my-3">
                            <TbChevronRight/> <span className="text-white">Phone Number Here 216531</span>
                        </div>
                        <div className="d-flex gap-2 my-3">
                            <TbChevronRight/> <span className="text-white">Email Here @gmail.com</span>
                        </div>
                    </Col>


                    {/*2nd*/}
                    <Col sm={2}>
                        <h6>Information</h6>
                        <div className="my-3 border-bottom border-3 w-25">
                        </div>
                        <div>
                            <span><a href="#" className="link">About Us</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Latest Post</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Selling Tips</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Advertising</a></span>
                        </div>
                        <div>
                            <span><a href="#" className="link">Contact Us</a></span>
                        </div>
                        
                    </Col>

                    {/*3rd*/}
                    <Col sm={2}>
                        <h6>My Account</h6>
                        <div className="my-3 border-bottom border-3 w-25">
                        </div>
                        <div>
                            <span><a href="#" className="link">My Account</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Login/Register</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Cart</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Wishlist</a></span>
                        </div>
                        <div>
                            <span><a href="#" className="link">Order History</a></span>
                        </div>
                    </Col>

                    {/*4th*/}
                    <Col sm={3}>
                        <h6>Help & Support</h6>
                        <div className="my-3 border-bottom border-3 w-25">
                        </div>
                        <div>
                            <span><a href="#" className="link">How To Shop</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Payment</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Returns</a></span>
                        </div>
                        <div className="my-2">
                            <span><a href="#" className="link">Delivery</a></span>
                        </div>
                        <div>
                            <span><a href="#" className="link">Privacy & Cookie Policy</a></span>
                        </div>
                    </Col>

                </Row>
                <Row className="border-top border-secondary ">
                    <div className="d-flex mt-4 gap-5 justify-content-center">
                        <span className="text-white">@Copyright HERE</span>
                        {/*<span className="mx-5">put logo and socials here</span>
                        <span>put payment here</span>*/}
                    </div>
                </Row>
            </Container>

        
    )
}