
import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function ReactivateCourse({ courseId, handleReactivateCourse, isActive }) {
  const [showModal, setShowModal] = useState(false);

  const handleReactivateClick = () => {
    setShowModal(true);
  };

  const handleConfirmReactivate = () => {
    handleReactivateCourse(courseId);

    setShowModal(false);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <Button variant="success" onClick={handleReactivateClick}>
        Reactivate
      </Button>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm Reactivation</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to reactivate this course?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Cancel
          </Button>
          <Button variant="success" onClick={handleConfirmReactivate}>
            Yes, Reactivate
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
