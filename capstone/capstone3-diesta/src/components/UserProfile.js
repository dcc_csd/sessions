// import React, {useContext} from 'react';


import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import React, { useEffect, useState } from 'react';

export default function UserProfile() {
  const [userData, setUserData] = useState({});
  const [isUpdate, setIsUpdating] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [address, setAddress] = useState('');
  const [shippingAddress, setShippingAddress] = useState('');

  useEffect(() => {
    // kunin ang token
    const token = localStorage.getItem('token');

    if (token) {
      fetch('https://capstone2-diesta.onrender.com/users/details', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUserData(data);
          setFirstName(data.firstName);
          setLastName(data.lastName);
          setEmail(data.email);
          setMobileNo(data.mobileNo);
          setAddress(userData.address);
          setShippingAddress(userData.shippingAddress);
        });
    }
  }, []);

  const handleEditClick = () => {
    setIsUpdating(true);
  };

  const handleCancelClick = () => {
    setIsUpdating(false);
    setFirstName(userData.firstName);
    setLastName(userData.lastName);
    setEmail(userData.email);
    setMobileNo(userData.mobileNo);
    setAddress(userData.address);
    setShippingAddress(userData.shippingAddress);
  };

  const handleSaveClick = () => {
    
  };

   const profileBanner = {
    width: '100%',
    borderRadius: '45px',
    border: '3px solid white',
  };

    const pos = {
    position: 'relative',
  };

  const profilePicture = {
    padding: '0',
    width: '20vw',
    height: 'auto',
    borderRadius: '100%',
    border: '10px solid white',
    position: 'absolute',
    top: '75%',
    left: '38%',
  };

  return (
    <div className="d-flex  align-self-center justify-content-center">
    <Container className="p-5 my-5 border">
      <Row>
        <div className="d-flex flex-column justify-content-center mb-4 mb-lg-5" style={pos}>
          <img fluid className="" style={profileBanner} src="https://dfstudio-d420.kxcdn.com/wordpress/wp-content/uploads/2019/06/digital_camera_photo-1080x675.jpg"/>

          <img fluid className="mx-auto" style={profilePicture} src="https://cdn-icons-png.flaticon.com/512/219/219969.png"/>
        </div>
            
      {!isUpdate ? (
        <>

          <div className="mt-sm-5 d-flex justify-content-center">
            <h3 className="mt-3 mx-auto text-secondary">
              <strong>{userData.firstName} {userData.lastName}</strong>
            </h3>  
          </div>

        
          <div className="pt-2 mt-2">
            <p className="text-muted">
              Hello <strong>{userData.firstName} {userData.lastName}!</strong>
            </p>

            <p className="text-muted">
              From your account you can easily view and track orders. You can manage and change your account information like address, contact information and history of orders.
            </p>
          </div>  

          <hr className="my-3"/>
          
          <div className="my-2 text-muted ">
            <h4><strong>ACCOUNT INFORMATION</strong></h4>
            <Container className="pt-2">
                <p><strong>Email:</strong> {userData.email}</p>
                <p><strong>Contact Number:</strong> {userData.mobileNo}</p>
                <p><strong>Address:</strong></p>
                <span>{userData.address}</span>
                <p><strong>Shipping Address:</strong></p>
                <span>{userData.shippingAddress}</span>
            </Container>
          </div>

          <button onClick={handleEditClick} className=" btn-outline-dark btn">Edit Profile</button>
        </>
      ) : (
        <Form className="mt-xs-3 mt-sm-5">
          <Form.Group className="mb-3">
            <Form.Label className="me-2 mt-3">First Name:</Form.Label>
            <Form.Control type="text" id="firstName" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label className="me-2 mt-3">Last Name:</Form.Label>
            <Form.Control type="text" id="lastName" value={lastName} onChange={(e) => setLastName(e.target.value)} />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label className="me-2 mt-3">Email:</Form.Label>
            <Form.Control type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label className="me-2 mt-3">Contact Number:</Form.Label>
            <Form.Control type="text" id="mobileNo" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label className="me-2 mt-3">Address:</Form.Label>
            <Form.Control type="text" id="address" value={address} onChange={(e) => setAddress(e.target.value)} />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label className="me-2 mt-3">Shipping Address:</Form.Label>
            <Form.Control type="text" id="shippingAddress" value={shippingAddress} onChange={(e) => setShippingAddress(e.target.value)} />
          </Form.Group>

          <div className="mt-5 d-flex justify-content-between">
            <Button type="button" onClick={handleCancelClick} variant="outline-primary" className="mx-3">
              Cancel
            </Button>

            <Button type="button" onClick={handleSaveClick} variant="outline-danger" className="mx-3">
              Save
            </Button>
          </div>
        </Form>

      )}
      </Row>

      <Row>
          <Col></Col>
      </Row>

    </Container>
    </div>
  );
}