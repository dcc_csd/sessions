import {Container, Row, Col, Button,Card} from 'react-bootstrap';
import coursesData from '../data/coursesData';
import {useState} from 'react';
 

import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'


export default function CourseCard({courseProp}){
	
    //Checks to see if the data was successfullt passed
	// console.log(props);
	// Every components receives information in a form of an object
	// console.log(typeof props);
	
    const { _id, name, description, price } = courseProp;

    //Use the state hook in this component to be able to store its state
    /*
			Syntac:
			const [getter, setter] =useState(initialGetterValue);

    */

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);

    
    // console.log(useState(0));

    //Function that keeps track of the enrollees for a course
    // function enroll(){

    // 	setCount(count + 1);
    // 	console.log('Enrollees: ' + count);
    // }
    
    // function enroll() {
    // if (seats > 0) {
    //   setSeats(seats - 1); // Deduct one seat
    //   console.log('Enrollees: ' + (30 - seats));
    //    } else {
    //     alert('No more seats available!');
    //    }
    // }
    

	return (
        <Container fluid>
        <Row className ="mt-3 mb-3"> 
        	
        	<Col xs={12} md={4}  xl={12}>
        	
        	  <Card>
			      
                  <Card.Body>
			      
			      		 <Card.Title>{name}</Card.Title>
			       		 <Card.Subtitle>Description</Card.Subtitle>
			       		 <Card.Text>{description}</Card.Text>
                 <Card.Subtitle>Price:</Card.Subtitle>
                 <Card.Text>PhP {price}</Card.Text>
			           	
			       {/*// <Button variant="primary" onClick={enroll}>Enroll</Button>*/}

			       <Link  className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			      
			      </Card.Body>
			  
			   </Card>
        	
         	</Col>
	   </Row> 
	   </Container>
		)
}

//Check if the CourseCard component is getting the correct prop types

CourseCard.propTypes = {
	course: PropTypes.shape({
		name:  PropTypes.string.isRequired,
		description : PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
    })
}