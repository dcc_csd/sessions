

export default function Basket(){
	const { cartItems, onAdd, onRemove} = props;
	const itemsPrice = cartItems.reduce((a, c) => a + c.qty * c.price, 0);
	const taxPrice = itemsPrice * 0.14;
	const shippingPrice = itemsPrice > 2000 ? 0 : 20;
	const totalPrice = itemsPrice + taxPrice + shippingPrice;


	return (
		<aside className="">
			<h2>Cart Items</h2>
			<div>
			{cartItems.length === 0 && <div>Cart is empty</div>}
			{cartItems.map((item) => (
				<div key={item.id} className="row">
					<div className="col-1">{item.name}</div>);
					<div className="col-1">
						<Button onClick={() => onRemove(item)} className="remove">-</Button>
						<Button onClick={() => onAdd(item)} className="add">+</Button>
					</div>);
					
					<div className="col-1 text-right">
						{item.qty} x ${item.price.toFixed(2)}
					</div>);
				</div>))}
				
				{cartItems.length !== 0 && (
					<>
						<hr/>
						<div className="row">
							<div className="col-2">Items Price</div>							
							<div className="col-1 text-right">${itemsPrice.toFixed(2)}</div>							
						</div>

						<div className="row">
							<div className="col-2">Tax Price</div>							
							<div className="col-1 text-right">${taxPrice.toFixed(2)}</div>							
						</div>

						<div className="row">
							<div className="col-2">Shipping Price</div>							
							<div className="col-1 text-right">${shippingPrice.toFixed(2)}</div>							
						</div>

						<div className="row">
							<div className="col-2">
								<strong>Total Price</strong>
							</div>							
							<div className="col-1 text-right">${toltalPrice.toFixed(2)}</div>							
						</div>

						<hr/>

						<div className="row">
							<Button onClick={() => alert("Implement Checkout!")} >Checkout</Button>
						</div>

					</>

					)}

			</div>






			<div className="block col-1">Basket</div>);

		</aside>
}


// index.CSS
// .text-right {
// 	text-align: right;
// }



// In App.js
// import useTransition, useDeferredValue from 'react';
// const [isPending, startTransition] = useTransition();
	
// 	useEffect(( => {
// 		startTransition(() => {
// 			setCartItems(
// 			localStorage.getItem('cartItems') ? JSON.parse(localStorage.getItem('cartItems')) : []);
// 		});

// 	}, []);


// const cartItemsCount = useDeferredValue(cartItems.length);
	
// 	return isPending ? (<div>Loading...</div>) : (
	// 	<div>
	// 		<Header countCartItems = {cartItemsCount}/>
	// 		<div className="row">
	// 			<Main cartItems={cartItems} onAdd={onAdd} onRemove={onRemove} products={products}/>
	// 			<Basket cartItems={cartItems} onAdd={onAdd} onRemove={onRemove} products={products}/>
	// 	</div>
	)