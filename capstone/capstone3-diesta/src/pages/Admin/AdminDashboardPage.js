import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import AdminSidebar from '../../components/Admin/Sidebar/AdminSidebar'; // Adjust the path
import AdminDashboard from '../../components/Admin/AdminDashboard'; // Adjust the path
import RecentOrdersTable from '../../components/Admin/RecentOrdersTable'; // Adjust the path

import "./home.css";


const AdminDashboardPage = () => {
  return (
    <Container fluid>
      
      <Row>
    
        <Col sm={3} className="mt-3">
          <AdminSidebar />
        </Col>
        
     
        <Col sm={9} >
          <div className="admin-content">
            <AdminDashboard className="mb-0"/>            
          </div>

           
          <div className="admin-content mt-3">
            <RecentOrdersTable />
          </div>
        </Col>
      </Row>
    </Container>  
    );
};

export default AdminDashboardPage;

