import React, { useState, useEffect } from 'react';
import UserTable from '../../components/Admin/UserTable';
import AdminSidebar from '../../components/Admin/Sidebar/AdminSidebar';
import { Link } from 'react-router-dom';

const UserList = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // Fetch users from your MongoDB backend
    fetch('https://capstone2-diesta.onrender.com/users/users')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => setUsers(data))
      .catch((error) => console.error('Error fetching users:', error));
  }, []);

  const linkStyle = {
    textDecoration: 'none',
    color: 'blue',
  };

  const containerStyle = {
    display: 'flex',
  };

  const sidebarStyle = {
    width: '250px', // Adjust the width of the sidebar as needed
  };

  const contentStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    padding: '20px', // Add padding to the content area as needed
  };

  const headerStyle = {
    marginBottom: '25px',
  };

  const headingStyle = {
    fontSize: '24px',
    marginTop: '10px', // Add margin to the top of the heading
    marginLeft: '10px', // Add margin to the left of the heading
  };

  return (
    <div className="user-view">
      <div className="user-view-header" style={headerStyle}>
        {/* Removed the top three icons and links */}
      </div>

      <div style={containerStyle}>
        <div style={sidebarStyle}>
          {/* Include the AdminSidebar component on the left */}
          <AdminSidebar />
        </div>

        <div style={contentStyle}>
          
          <h1 className="text-left" style={headingStyle}>
            USER LIST
          </h1>
          {/* Place the UserTable component inside the second container */}
          <UserTable users={users} />
          {/* Add the search button or component here */}
        </div>
      </div>
    </div>
  );
};

export default UserList;
