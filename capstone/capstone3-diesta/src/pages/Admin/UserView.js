import React, { useState, useEffect } from 'react';
import UserGrid from '../../components/Admin/UserGrid';
import AdminSidebar from '../../components/Admin/Sidebar/AdminSidebar'; // Import your AdminSidebar component
import { Link } from 'react-router-dom';

const UserView = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // Fetch users from your MongoDB backend
    fetch('https://capstone2-diesta.onrender.com/users/users')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => setUsers(data))
      .catch((error) => console.error('Error fetching users:', error));
  }, []);

  const linkStyle = {
    textDecoration: 'none',
    color: 'blue',
  };

  const containerStyle = {
    display: 'flex',
  };

  const sidebarStyle = {
    width: '250px', // Adjust the width of the sidebar as needed
  };

  const contentStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    padding: '20px', // Add padding to the content area as needed
  };

  const headingStyle = {
    alignSelf: 'flex-start',
    marginLeft: '10px', // Add margin to the left of the heading
    fontSize: '24px',
    marginTop: '30px',
    marginBottom: '-30px', // Customize the font size
  };

    const cardListStyle = {
    margin: '10px 0', // Customize the margin of the User Card List
    padding: '10px', // Customize the padding of the User Card List
    border: '1px solid #ccc', // Add a border for styling
    borderRadius: '5px', // Customize the border radius
    backgroundColor: '#f9f9f9', // Customize the background color
  };

    const cardDetailsStyle = {
    fontSize: '12px', // Customize the font size for card details
    whiteSpace: 'nowrap', // Prevent text from wrapping
    overflow: 'hidden', // Hide overflowing text
    textOverflow: 'ellipsis', // Add ellipsis for truncated text
  };


  return (
    <div className="user-view">
      <div className="user-view-header">
        {/* Removed the heading from here */}
      </div>
      <div style={containerStyle}>
        <div style={sidebarStyle}>
          <AdminSidebar />
        </div>
        <div style={contentStyle}>
         
          <h1 className="text-left" style={headingStyle}>
            Admin User List
          </h1>
          <UserGrid users={users} />
        </div>
      </div>
    </div>
  );
};

export default UserView;



