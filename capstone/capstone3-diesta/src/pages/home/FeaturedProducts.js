import React, { useEffect, useState, useRef } from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import { FaHeart } from 'react-icons/fa';
import "./features.css";

export default function FeaturedProducts({ searchQuery }) {
  const [newProducts, setNewProducts] = useState([]);
  const [products, setProducts] = useState([]);
  const [imgs, setImgs] = useState([]);
  const [newPageIndex, setNewPageIndex] = useState(0);
  const [filteredProducts, setFilteredProducts] = useState([]);

  const itemsPerPage = 30;

  useEffect(() => {
    // Fetch product data and set new products and images
    fetch('https://capstone2-diesta.onrender.com/product/')
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
        setNewProducts(data.slice(newPageIndex * itemsPerPage, (newPageIndex + 1) * itemsPerPage));

        let ids = data.map((item) => item._id);
        let imgs = data.map((item) => item.images[0]);
        let productimage = imgs.map((item) => item.url);

        setImgs(productimage);
      })
      .catch((error) => {
        console.error('Error fetching product data:', error);
      });
  }, [newPageIndex]);

  useEffect(() => {
    // Filter products based on the search query
    const newFilteredProducts = products.filter((product) => {
      if (product.slug) {
        return product.slug.toLowerCase().includes(searchQuery.toLowerCase());
      }
      return false;
    });
    setFilteredProducts(newFilteredProducts);
  }, [searchQuery, products]);

  const handleNewNextPage = () => {
    setNewPageIndex(newPageIndex + 1);
  };

  const isNewNextPageDisabled = (newPageIndex + 1) * itemsPerPage >= products.length;

  const carouselRef = useRef(null);
  const itemWidth = 260;
  const scrollInterval = 3000;

  useEffect(() => {
    // Handle scrolling in the carousel
    const container = carouselRef.current;

    if (container) {
      const totalItems = container.childElementCount;
      let currentPosition = -30;

      const scrollCarousel = () => {
        currentPosition += itemWidth;
        if (currentPosition >= totalItems * itemWidth) {
          currentPosition = -30;
        }
        container.scrollTo({ left: currentPosition, behavior: 'smooth' });
      };

      const interval = setInterval(scrollCarousel, scrollInterval);

      return () => {
        clearInterval(interval);
      };
    }
  }, [scrollInterval]);

  return (
    <div>
      {/* New Products Carousel */}
      <div className="d-flex mb-2">
        <h5 className="border-bottom border-dark border-3">New Products</h5>
      </div>
      <div className="carousel-container" ref={carouselRef}>
        {filteredProducts.map((product, index) => (
          <div key={product._id} className="product-card">
            <Link to={`single-product/${product._id}`}>
              <Card className="me-5">
                <Card.Img
                  src={imgs[newPageIndex * itemsPerPage + index]}
                  alt={`Product Image`}
                  className="clickable-image light-gray-effect"
                />
                <div className="pill-box">
                  <span className="pill-text">{product.slug}</span>
                </div>
                <Card.Body>
                  <Card.Title>
                    <div className="product-info">
                      {product.name}
                    </div>
                    <Card.Text>
                      <span className="price-text">Price: ${product.price}</span>
                    </Card.Text>
                    <div className="add-to-cart-container mt-2">
                      <button className="add-to-cart-button bg-dark">Add to Cart</button>
                      <button className="wishlist-button btn btn-outline-dark">
                        <FaHeart />
                      </button>
                    </div>
                  </Card.Title>
                </Card.Body>
              </Card>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}
