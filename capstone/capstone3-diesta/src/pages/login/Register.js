import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import {
  MDBBtn,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBCheckbox,
} from 'mdb-react-ui-kit';
import { AiFillCloseCircle } from 'react-icons/ai';
import UserContext from '../../UserContext';
import './login.css';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function registerUser(event) {
    event.preventDefault();

    // Check if passwords match and mobile number is 11 digits
    if (password !== confirmPassword) {
      Swal.fire({
        title: 'Password Mismatch',
        icon: 'error',
        text: 'Passwords do not match. Please try again.',
      });
      return;
    }

    if (mobileNo.length !== 11) {
      Swal.fire({
        title: 'Invalid Mobile Number',
        icon: 'error',
        text: 'Mobile number must be 11 digits long. Please check.',
      });
      return;
    }

    fetch('https://capstone2-diesta.onrender.com/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password,
      }),
    })
      .then((res) => {
        if (res.status === 200) {
          // Registration was successful
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNo('');
          setPassword('');
          setConfirmPassword('');

          Swal.fire({
            title: 'Registration Successful!',
            icon: 'success',
            text: 'You have successfully registered!',
          });

          navigate('/login'); 
        } else {
          Swal.fire({
            title: 'Registration Failed!',
            icon: 'error',
            text: 'Registration was unsuccessful. Please try again.',
          });
        }
      });
  }

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      mobileNo.length === 11 &&
      password !== '' &&
      confirmPassword !== '' &&
      password === confirmPassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

  return (
    <>
      {user.id !== null ? (
        navigate('/product') 
      ) : (
        <div className="modal-bg d-flex justify-content-center align-items-center">
          <MDBCard className="m-5 embossed" style={{ maxWidth: '600px', position: 'relative' }}>
            {/* Close button with AiFillCloseCircle icon */}
            <AiFillCloseCircle
              size={32}
              className="position-absolute end-0 p-3"
              style={{
                cursor: 'pointer',
                color: 'darkblue',
                zIndex: '2', // Set a higher zIndex
                padding: '0.5rem', // Add padding
                backgroundColor: 'white', // Add a background color
                borderRadius: '50%', // Make it a circle
              }}
              onClick={() => navigate('/')} // You can change this to close the modal
            />

            <MDBCardBody className="px-5">
              <h2 className="text-uppercase text-center mt-2">Register an account</h2>
              <MDBRow className="mt-4 g-3">
                <MDBCol>
                  <MDBInput
                    placeholder="First Name"
                    wrapperClass="mb-4"
                    size="lg"
                    id="form1"
                    type="text"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                  <MDBInput
                    placeholder="Email"
                    wrapperClass="mb-4"
                    size="lg"
                    id="form3"
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <MDBInput
                    placeholder="Password"
                    wrapperClass="mb-4"
                    size="lg"
                    id="form5"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </MDBCol>
                <MDBCol>
                  <MDBInput
                    placeholder="Last Name"
                    wrapperClass="mb-4"
                    size="lg"
                    id="form2"
                    type="text"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                  <MDBInput
                    placeholder="Mobile No."
                    wrapperClass="mb-4"
                    size="lg"
                    id="form4"
                    type="number"
                    value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}
                  />
                  <MDBInput
                    placeholder="Confirm Password"
                    wrapperClass="mb-4"
                    size="lg"
                    id="form6"
                    type="password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                  />
                </MDBCol>
                <div className="d-flex flex-row justify-content-center mb-4">
                  <MDBCheckbox name="flexCheck" id="flexCheckDefault" label="I agree to all statements in Terms of Service" />
                </div>
                <MDBBtn className="mb-4 w-100 gradient-custom-4" size="lg" disabled={!isActive} onClick={registerUser}>
                  Register
                </MDBBtn>
              </MDBRow>
            </MDBCardBody>
          </MDBCard>
        </div>
      )}
    </>
  );
}
