
import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import UserContext from '../../UserContext'; 
import Swal from 'sweetalert2';

import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBInput,
} from 'mdb-react-ui-kit';
import './login.css';

export default function Login() {
  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);
  

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function authenticate(e) {
    e.preventDefault();
    fetch('https://capstone2-diesta.onrender.com/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);
          Swal.fire({
            title: 'Login Successful!',
            icon: 'success',
            text: 'Welcome to D-Store!',
          });
        } else {
          Swal.fire({
            title: 'Authentication Failed!',
            icon: 'error',
            text: 'Check your login details and try again!',
          });
        }
      });
  }

  const retrieveUserDetails = (token) => {
    fetch('https://capstone2-diesta.onrender.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });

        // Redirect based on isAdmin
        if (data.isAdmin) {
          navigate('/admin-dashboard'); 
        } else {
          navigate('/'); 
        }
      });
  };


  return (
    <MDBContainer className="my-4">
      <MDBCard className="border-0 bg-transparent">
        <MDBRow className="d-flex justify-content-center">
          
          <MDBCol xs='12' md='6' className="d-flex justify-content-center">
            <MDBCardImage
              src='https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/img1.webp'
              alt="login form"
              className='rounded-start custom-card-body'
            />
          </MDBCol>
          
          <MDBCol xs='12' md='5' className="ms-3">
            <MDBCardBody className='d-flex flex-column'>
              <div className='d-flex flex-row mt-2'>
                <img
                  src='/shoplogo1.png'
                  alt="Logo"
                  width="50"
                  height="50"
                  className="me-2"
                />
                <span className="h1 fw-bold mb-0">D-Shop</span>
              </div>
              <h5 className="fw-normal my-4 mx-auto pb-1" style={{ letterSpacing: '1px' }}>Sign into your account</h5>
              <form onSubmit={authenticate}>
                
                <Form>
                        <Form.Label>Email address</Form.Label>
                </Form>

                <MDBInput
                  wrapperClass='mb-4'
                  id='formControlLg'
                  type='email'
                  size="lg"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />

                <Form>
                      <Form.Label>Password</Form.Label>
                </Form>
                <MDBInput
                  wrapperClass='mb-4'
                  id='formControlLg'
                  type='password'
                  size="lg"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />

                <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
                <MDBBtn
                  className="mb-4 px-5"
                  color='dark'
                  size='lg'
                  disabled={!isActive}
                  type="submit"

                >
                  Login
                </MDBBtn>
                </div>
              </form>
              <a className="small text-muted mx-auto" href="#!">Forgot password?</a>
              <p className="mb-5 pb-lg-2 mx-auto mt-2" style={{ color: '#393f81' }}>
                Don't have an account? <a href="/register" style={{ color: '#393f81' }}>Register here</a>
              </p>
            </MDBCardBody>
          </MDBCol>
        </MDBRow>
      </MDBCard>
    </MDBContainer>
  );
}

