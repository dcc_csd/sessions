// UserOrderList.jsx
import React, { useState, useEffect } from 'react';
import UserOrderSummary from './UserOrderSummary';

const UserOrderList = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await fetch('https://capstone2-diesta.onrender.com/users/users');
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const userData = await response.json();
        setUsers(userData);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchUsers();
    console.log('UserOrderList rendered');
  }, []);

  return (
    <div>
      <h1>User Order List</h1>
      {users.map((user) => (
        <div key={user.id}>
          <h2>User: {user.name}</h2>
          <UserOrderSummary userId={user.id} />
        </div>
      ))}
    </div>
  );
};

export default UserOrderList;
