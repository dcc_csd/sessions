
import React, { useState, useEffect } from 'react';
import { Card, Container, Col, Row, Button, Form } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

import UserContext from './../../UserContext';

function OrderCheckout() {
  const location = useLocation();
  let Cartview=[];
  const CVParam = new URLSearchParams(location.search).get('Cartview');
  console.log('CVParam:', Cartview.length);

  const [orderDetails, setOrderDetails] = useState({
    products: [],
    totalQuantity: 0,
    totalPrice: 0,
  });

  // Process the order
  const processOrder = () => {
        console.log('Cartview:', Cartview);
    const products = Cartview.map((cartItem) => {
      const product = cartItem.products[0];

      const subtotal = product.quantity * product.price;
      return {
        name: product.name,
        quantity: product.quantity,
        price: product.price,
        subtotal: subtotal,
      };
    });


    const totalQuantity = products.reduce((total, product) => total + product.quantity, 0);
    const totalPrice = products.reduce((total, product) => total + product.subtotal, 0);


    setOrderDetails({ products, totalQuantity, totalPrice });
  };


  return (

    <div>
      <h1>Place an Order</h1>
     
      <button onClick={processOrder}>Place Order</button>

      {orderDetails.products.length > 0 && (
        <div>
          <h2>Order Summary</h2>
          <ul>
            {orderDetails.products.map((product, index) => (
              <li key={index}>
                Product: {product.name},
                Quantity: {product.quantity},
                Price: ${product.price},
                Subtotal: ${product.subtotal}
              </li>
            ))}
          </ul>
          <p>Total Quantity: {orderDetails.totalQuantity}</p>
          <p>Total Price: ${orderDetails.totalPrice}</p>
        </div>
      )}
    </div>
  );
}

export default OrderCheckout;