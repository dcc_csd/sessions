import React, { useContext, useEffect, useState } from 'react';
import { Card, Container, Col, Row, Button, Form, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from './../../UserContext';
import Swal from 'sweetalert2';

const UserOrderSummary = () => {
  const { user } = useContext(UserContext);
  const userId = user.id;
  const [orders, setOrders] = useState([]);
  const [fullName, setFullName] = useState('');

  const getOrderData = async (userId) => {
    try {
      const response = await fetch(`https://capstone2-diesta.onrender.com/order/find/${userId}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });

      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const ordersData = await response.json();
      return ordersData;
    } catch (error) {
      console.error('Error fetching user orders:', error);
    }
  };

  const getName = async (userId) => {
    try {
      const response = await fetch(`https://capstone2-diesta.onrender.com/users/find/${userId}`);
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      const userData = await response.json();
      const fullName = `${userData.firstName} ${userData.lastName}`;
      setFullName(fullName);
    } catch (error) {
      console.error('Error fetching user name:', error);
    }
  };

  const getProductNames = async (orderItems) => {
    return Promise.all(orderItems.map(async (orderItem) => {
      try {
        const response = await fetch(`https://capstone2-diesta.onrender.com/products/${orderItem.product}`);
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        const productData = await response.json();
        return productData.name;
      } catch (error) {
        console.error('Error fetching product name:', error);
        return 'Product Name Not Available';
      }
    }));
  };

  useEffect(() => {
    if (userId) {
      getName(userId)
        .then(() => {
          getOrderData(userId)
            .then(async (data) => {
              const ordersWithProductNames = await Promise.all(data.map(async (order) => {
                const productNames = await getProductNames(order.orderItems);
                return { ...order, productNames };
              }));
              setOrders(ordersWithProductNames);
            })
            .catch((error) => {
              console.error('Error fetching data:', error);
            });
        })
        .catch((error) => {
          console.error('Error fetching user name:', error);
        });
    }
  }, [userId]);

  return (
    <div>
      <h2>Order Summary for {fullName}  </h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Order Date</th>
            <th>Product IDs</th>
            <th>Total Price</th>
            <th>Status</th>

          </tr>
        </thead>
        <tbody>
          {orders.map((order) => (
            <tr key={order._id}>
              <td>{order.dateOrdered}</td>
              <td>
                {order.orderItems && order.orderItems.length > 0
                  ? order.orderItems.join(', ')
                  : 'N/A'}
              </td>
              <td>${order.totalPrice.toFixed(2)}</td>
              <td>{order.status}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};
export default UserOrderSummary;
