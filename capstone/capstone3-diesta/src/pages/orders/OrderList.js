import React, { useState, useEffect } from 'react';
import AdminSidebar from '../../components/Admin/Sidebar/AdminSidebar';
import OrderTable from '../../components/Order/OrderTable'; 
import { Link } from 'react-router-dom';




const OrderList = () => {
  const [order, setOrder] = useState([]);
 

  const linkStyle = {
    textDecoration: 'none',
    color: 'blue',
  };

  const containerStyle = {
    display: 'flex',
  };

  const sidebarStyle = {
    width: '250px', 
  };

  const contentStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    padding: '20px', 
  };

  const headerStyle = {
    marginBottom: '25px',
  };

  const headingStyle = {
    fontSize: '24px',
    marginTop: '10px', 
    marginLeft: '10px', 
  };

  return (
    <div className="product-view">
      <div className="user-product-header" style={headerStyle}>
      
      </div>

      <div style={containerStyle}>
        <div style={sidebarStyle}>
          {/* Include the AdminSidebar component on the left */}
          <AdminSidebar />
        </div>

        <div style={contentStyle}>
          
          <h1 className="text-left" style={headingStyle}>
            ORDER LIST
          </h1>
         
           <OrderTable order={order} />
          
        </div>
      </div>
    </div>
  );
};

export default OrderList;

