import FrontAd from '../components/home/FrontAd';
import React, { useState } from 'react';
import Highlights from '../components/home/Highlights';
import CategorySidebar from '../components/Sidebars/CategorySidebar';
import ProductDisplay from '../components/ProductDisplay';
import FeaturedProducts from '../pages/home/FeaturedProducts';
import TopProducts from '../pages/home/TopProducts';
import Footer from '../components/Footer';
import Brands from '../components/home/Brands';
import Card from 'react-bootstrap/Card';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';


import '../App.css';

export default function Home() {

const [searchQuery, setSearchQuery] = useState('');

  const handleSearch = (query) => {
    setSearchQuery(query);
  };


    return (
        <>
            <Container fluid className="p-0 m-0">
                <Row className="my-4 justify-content-between height">
                    <Col xs="12" sm="12" md="4" lg="3" className="order-2 d-md-none">
                        <CategorySidebar/>
                    </Col>
                    

                    <Col xs="12" sm="12" md="8" lg="9" className="order-1 d-md-none mt-sm-3 mt-md-0">
                        <FrontAd/>
                        <Highlights className="mt-4 justify-content-center"/>
                    </Col>

                    
                    <Col xs="12" sm="12" md="4" lg="3" className="order-1 d-none d-md-block">
                        <CategorySidebar/>
                    </Col>
                    

                    <Col xs="12" sm="12" md="8" lg="9" className="order-2 d-none d-md-block mt-sm-3 mt-md-0">
                        <FrontAd onSearch={handleSearch} />
                        <Highlights className="mt-4 justify-content-center"/>
                    </Col>
                
                </Row>
                
                        
                <Row className="mt-3 justify-content-center">
                    <ProductDisplay />
                </Row>

                <Row className="mt-3 justify-content-center">
                    <FeaturedProducts searchQuery={searchQuery} />
                </Row>

                <Row className="mt-3 justify-content-center">
                    <TopProducts />
                </Row>

                <Row className="my-5 justify-content-center">
                    <Brands />
                </Row>
                
            </Container>
        </>
    )
}
