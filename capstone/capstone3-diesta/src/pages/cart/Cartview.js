import React, { useContext, useEffect, useState } from 'react';
import { Card, Container, Col, Row, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from './../../UserContext';
import Swal from 'sweetalert2';


function IncrementButton({ cartItemId, increaseQuantity }) {
  return (
    <button className="btn" onClick={() => increaseQuantity(cartItemId, 1)}>
      +
    </button>
  );
}

function DecrementButton({ cartItemId, decreaseQuantity }) {
  return (
    <button className="btn" onClick={() => decreaseQuantity(cartItemId, -1)}>
      -
    </button>
  );
}


function CartSummary() {
  const [cart, setCart] = useState([]);
  const [productDataArray, setProductDataArray] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const[quantity, setQuantity] = useState({});
  const [itemQuantities, setItemQuantities] = useState({});

  const [updatedCart, setUpdatedCart] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const { user } = useContext(UserContext);
  const userId = user.id;

const getCartData = async (userId) => {
    try {
      const response = await fetch(`https://capstone2-diesta.onrender.com/carts/find/${userId}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });

      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const cartData = await response.json();
      return cartData;
    } catch (error) {
      console.error('Error fetching cart data:', error);
    }
  };

const getProductData = async (productId) => {
  try {
    const response = await fetch(`https://capstone2-diesta.onrender.com/product/${productId}`);

    if (!response.ok) { 
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

const productData = await response.json();
    return productData;
  } catch (error) {
    console.error('Error fetching product data:', error);
  }
  };



const calculateTotalPriceForProduct = (product, qty) => {
    return product.price * qty;
  };

const removeProductFromCart = (cartItemId) => {
fetch(`https://capstone2-diesta.onrender.com/carts/${cartItemId}`, {
  method: 'DELETE',
  headers: {
  'Authorization': `Bearer ${localStorage.getItem('token')}`,
    },
  })
  .then((response) => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
      }
    
      const updatedCart = cart.filter((cartItem) => cartItem._id !== cartItemId);
      setCart(updatedCart);

 
      Swal.fire({
        icon: 'success',
        title: 'Item Removed',
        text: 'The product has been successfully removed from your cart.',
      });
    })
    .catch((error) => {
      console.error('Error deleting product from the cart:', error);
      
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'An error occurred while removing the product from your cart.',
      });
    });

  };

 
const setInitialQuantities = (cartItems) => {
    const initialQuantities = {};
    cartItems.forEach((cartItem) => {
      initialQuantities[cartItem._id] = cartItem.products[0].quantity;
    });
    setItemQuantities(initialQuantities);
  };



const increaseQuantity = (itemId) => {
    setCart((prevCart) => {
      return prevCart.map((cartItem) => {
        if (cartItem._id === itemId) {
          return {
            ...cartItem,
            products: [
              {
                ...cartItem.products[0],
                quantity: cartItem.products[0].quantity + 1,
              },
            ],
          };
        }
        return cartItem;
      });
    });
  };



const decreaseQuantity = (itemId) => {
    setCart((prevCart) => {
      return prevCart.map((cartItem) => {
        if (cartItem._id === itemId) {
          return {
            ...cartItem,
            products: [
              {
                ...cartItem.products[0],
                quantity:
                  cartItem.products[0].quantity > 1
                    ? cartItem.products[0].quantity - 1
                    : 1, // Ensure the quantity is at least 1
              },
            ],
          };
        }
        return cartItem;
      });
    });
  };


const updateItemQuantity = (itemId, newQuantity) => {
    setItemQuantities((prevQuantities) => ({
      ...prevQuantities,
      [itemId]: newQuantity,
      }));
     };

const calculateUpdatedTotalPrice = () => {
  let updatedTotalPrice = 0;
  cart.forEach((cartItem, index) => {
    const product = productDataArray[index];
    if (product) {
      updatedTotalPrice += product.price * cartItem.products[0].quantity;
    }
  });
  return updatedTotalPrice;
};


const updatedTotalPrice = calculateUpdatedTotalPrice();



const createOrder = async (cartItemId) => {
  try {
    
    if (cart.length === 0) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Your cart is empty. Add items to your cart before creating an order.',
      });
      return; 
    }

    const orderData = {
      userId: userId,
      orderItems: cart.map((cartItem, index) => ({
        productId: cartItem.products[0].productId,
        quantity: cartItem.products[0].quantity,
        price: productDataArray[index].price,
      })),
      shippingAddress1: "123 Main St",
      city: "Your City",
      zip: "12345",
      country: "Your Country",
      phone: "12345678901",
      totalPrice: calculateUpdatedTotalPrice(),
    };
    console.log(orderData);

    const response = await fetch('https://capstone2-diesta.onrender.com/order', {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(orderData),
    });

      if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    // Order creation successful
    Swal.fire({
      icon: 'success',
      title: 'Order Created',
      text: 'Your order has been created successfully.',
    });

  // Delay the page reload by 2 seconds
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  } catch (error) {
    console.error('Error creating an order:', error);
   }
  };



    // Function to perform checkout (create order and delete cart items)
  const performCheckout = async () => {
      
      // First, create the order
      await createOrder();

      //delete the cart items
      const cartItemIds = cart.map((cartItem) => cartItem._id);
      const deletionPromises = cartItemIds.map(deleteCartItem);

      // Wait for all cart items to be deleted
      await Promise.all(deletionPromises);

      // Optionally, you can perform additional actions after checkout
      console.log('Checkout completed');
    };

    // Function to be called when the user clicks the checkout button

    const handleCheckout = async () => {

      // Call the performCheckout function
      await performCheckout();
    };





const deleteCartItem = async (cartItemId) => {
  try {
    const response = await fetch(`https://capstone2-diesta.onrender.com/carts/${cartItemId}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    return true; 
  } catch (error) {
    console.error('Error deleting product from the cart:', error);
    return false; 
  }
};



useEffect(() => {
    if (userId) {
      getCartData(userId)
        .then((data) => {
          setCart(data);

        
          Promise.all(data.map((cartItem) => getProductData(cartItem.products[0].productId)))
            .then((productDataArray) => {
              setProductDataArray(productDataArray);

           
              const totalPrice = cart.reduce((total, item, index) => {
                const product = productDataArray[index];
                return total + product.price * item.products[0].quantity;
              }, 0);

              setTotalPrice(totalPrice);
            })
            .catch((error) => {
              console.error('Error fetching product data:', error);
            });
        })
        .catch((error) => {
          console.error('Error fetching data:', error);
        });
    }
  }, [userId]);



 return (
    <div className="m-5">
      <Card>
        <Card.Header className="bg-dark text-white">
          <h1 className="m-1">Cart Summary</h1>
        </Card.Header>
        {cart && cart.length > 0 ? (
          <div fluid className="p-4">
            {cart
            .filter((cartItem) => cartItem.status === "active")
            .map((cartItem, index) => (
              

              <div key={cartItem._id}>
                <Card.Body className="">
                  <Container fluid>
                    <Row className="gap-2">
                      <Col md={3} className="border-end border-4 p-2">
                        <img
                          src={productDataArray[index]?.images?.[0]?.url}
                          alt="Product"
                          style={{ maxWidth: '100%', maxHeight: '100%' }}
                        />
                      </Col>
                      <Col md={7} className="d-flex flex-column justify-content-center">
                        {productDataArray[index] && (
                          
                          <div>
                            <h5 className="mt-2">Name: {productDataArray[index].name}</h5>
                            <p className="mt-2">Price: $ {productDataArray[index].price}</p>

                            <Form.Group>
                              <Form.Label>Quantity</Form.Label>
                                <div className="d-flex mb-2">
                                  <div className="classname3 d-flex" >
                                                                                    
                                     <DecrementButton cartItemId={cartItem._id} decreaseQuantity={decreaseQuantity}/>
                                     <Form.Control className="text-center" style={{ width: '75px'}} type="number" min="1" value={cartItem.products[0].quantity} readOnly/>
                                        
                                     <IncrementButton cartItemId={cartItem._id} increaseQuantity={increaseQuantity} />
                                  </div>

                                  <Button className="ms-2" variant="danger" onClick={() => removeProductFromCart(cartItem._id)}>Remove</Button>

                                  <div className="ms-auto">
                                    <p>
                                      Subtotal: ${calculateTotalPriceForProduct(
                                        productDataArray[index],
                                        cartItem.products[0].quantity
                                      )}
                                    </p>
                                
                                  </div>


                                </div>
                      
                            </Form.Group>

                          </div>

                        )}
                        
                      </Col>
                    </Row>
                  </Container>
                  <hr className="mb-0" />
                </Card.Body>
              </div>
            ))}
            <div className="d-flex">
              <h4 className="pt-2 px-3">Total Price: ${calculateUpdatedTotalPrice()}</h4>
              
              <Button 
                className="ms-auto btn btn-outlineDark bg-dark text-white"
                onClick={handleCheckout}  style={{ width: '200px'}} 
                 >
                Checkout
              </Button>
               
            </div>
          </div>
        ) : (
          <div>
            <Card.Body className="mx-auto">
              <h4>Your cart is empty.</h4>
            </Card.Body>
          </div>
        )}
      </Card>
    </div>
  );
}

export default CartSummary;