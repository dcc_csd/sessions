import React, { useContext, useEffect, useState } from 'react';
import { Card, Button, Container, Col, Row, InputGroup } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { useParams, Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

import UserContext from './../../UserContext'; 
import './SingleProductDetails.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faLinkedin } from 'react-icons/fa';
import { FaRegHeart } from 'react-icons/fa';
import { BsFacebook, BsTwitter, BsWhatsapp } from "react-icons/bs";
import Swal from 'sweetalert2';

export default function SingleProductDetails({handleAddToCart}) {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  
  const { productId } = useParams();
  
  const [quantity, setQuantity] = useState(1);
  
 
  const [cart, setCart] = useState([]);
  
  const [product, setProduct] = useState('');
  const [price, setPrice] = useState(0);


  const [stockQty, setStockQty] = useState(null);
  const [wishlist, setWishlist] = useState([]);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [cartItems, setCartItems] = useState(false);
  const [isButtonDisabled, setButtonDisabled] = useState(false);


 // To increment the quantity
const incrementQuantity = () => {
  setQuantity(quantity + 1);
};

// To decrement the quantity
const decrementQuantity = () => {
  if (quantity > 1) {
    setQuantity(quantity - 1);
  }
};

const handleButtonClick = () => {
    setButtonDisabled(true);
  };


useEffect(() => {
   



const fetchProductData = async () => {
      try {
        const response = await fetch(`https://capstone2-diesta.onrender.com/product/${productId}`);
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const productData = await response.json();

        const { product, stockQty } = productData;

        setProduct(productData);

        setStockQty(stockQty);

      } catch (error) {
        console.error('Error fetching product:', error);
      }
    };

    fetchProductData();
  }, [productId]);
 
 


  

 
 const addToCart = () => {
  // You should have the userId, productId, and quantity.
  
  const productDetails = {
      id: product._id, // Use the correct ID of the product.
      name: product.name,
      price: product.price,
      quantity: quantity,
      // You can include other product details as needed.
    };
   
   handleAddToCart(productDetails);
   
   const userId = user.id; 
   
   const addToCart = () => {
    if (cartItems.length === 0) {
       alert('Please log in or register to add items to your cart.');
   } else {
   const newItem = { id: 1, name: 'Product Name', price: 10 };
      setCartItems([...cartItems, newItem]);
    }
  };


   // navigate('/orders')
  // Send a POST request to the cart endpoint to add the product.
  fetch('https://capstone2-diesta.onrender.com/carts/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify({
      userId: userId,
      products: [
        {
          productId: product._id, // Use the correct ID of the product.
          quantity: quantity,
        },
      ],
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.message === 'success') {
        Swal.fire({
          title: 'Added to cart',
          icon: 'success',
          text: 'You have successfully added this product to the cart.',
        });

        navigate('/orders');
      } else {
        Swal.fire({
          title: 'Added to cart',
          icon: 'success',
          text: 'You have successfully added this product to the cart.',
        });
      }
    })
    .catch((error) => {
      console.error(error);
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'An error occurred while adding to the cart. Please try again later.',
      });
    });
};
  return (
    <Card className="my-5 mx-3 bg-dark text-white">
      <Container fluid>
        <Row className="p-3">
          <h1>{product.name}</h1>
        </Row>

        <Row className="bg-white text-dark p-4">
          <Col md={4}>
            <Card.Img src={product.images?.[0]?.url} alt={`Image 0`} />
          </Col>
          
          <Col md={8}>
            <Card.Body>
              <div className="product-details">
                <div className="product-name">
                  <h3> {product.name} </h3>
                </div>

                <div className="product-price">
                  <h5> ${product.price} </h5>
                </div>

                <div className="product-code">
                  SKU No: {product.codeNo} | Slug: {product.slug}
                </div>

                <div className="product-category mt-1">
                  <p>Category: {product.category}</p>
                </div>
              
                <div className="product-social-icons d-flex gap-3 mt-2">
                  <p className="product-category">Share To:</p>
                  <BsFacebook/>
                  <BsTwitter/>
                  <BsWhatsapp/>
                </div>
                
                <hr></hr>

                <div className="product-description">
                  {/*{product.description}*/}
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam arcu est, gravida et congue a, hendrerit vel est. Praesent nec magna at dolor egestas blandit. Aliquam volutpat justo urna, facilisis viverra elit hendrerit non. </p>
                  
                </div>
                <p className= "stock" style={{ marginRight: '20px'}}> STOCK QTY : {product.stockQty} </p>
                
                <div className="d-flex justify-content-start">
                
     
                     <div className="classname3 d-flex" >
                                                                         
                          <button  className="btn " onClick={decrementQuantity}>-</button>
                          <input  className="text-center" readOnly type="number" min="1" max={product.stockQty} value={quantity} onChange={(e) => setQuantity(e.target.value)}/> 
                             
                          <button  className="btn w-15 me-auto" onClick={incrementQuantity}>+</button>
                      
                         
                            <button className="btn btn-dark" style={{ marginLeft: '50px' }} onClick={addToCart}>
                              Add to Cart
                            </button>
                          
                          <div className="null" style={{ marginRight: '10px' }}></div>

                          <button className="btn btn-outline-dark" style={{ marginRight: '4px' }}><FaRegHeart /></button>
                        

                        
                      </div>
                                
                </div>

              </div>
            </Card.Body>
          </Col>
        </Row>



      </Container>
    </Card>

 );

}