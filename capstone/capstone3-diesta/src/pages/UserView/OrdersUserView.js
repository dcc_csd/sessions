import Orders from '../../components/User/Orders';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


import '../../App.css';

export default function UserView() {


    return (
        <>
            {/*Top Part*/}
            <Container fluid className="p-0 m-0 gap-1">
                <Row className="my-5 d-flex justify-content-between height">
                    <Col xs="12"className="mb-5 d-flex align-items-center">
                        <Orders/>
                    </Col>
                </Row>

            </Container>

        </>
    )
}