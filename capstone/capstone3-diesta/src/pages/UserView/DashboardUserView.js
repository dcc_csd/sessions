import Dashboard from '../../components/User/Dashboard';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


import '../../App.css';

export default function UserView() {


    return (
        <>
            {/*Top Part*/}
            <Container fluid className="p-0 m-0">
                <Row className="my-5 d-flex justify-content-between height gap-1">
                    <Col xs="12" className="mb-5 d-flex align-items-center">
                        <Dashboard/>
                    </Col>
                </Row>

            </Container>

        </>
    )
}