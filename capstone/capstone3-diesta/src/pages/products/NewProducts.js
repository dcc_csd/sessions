import React, { useState, useContext } from 'react';
import {Container, Row, Col, Form, Button } from 'react-bootstrap';
import AdminSidebar from '../../components/Admin/Sidebar/AdminSidebar'; // Adjust the path

import { Link } from 'react-router-dom'; // Import Link from 'react-router-dom'
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import ImageUpload from './ImageUpload';

export default function AddProduct() {
    const { user } = useContext(UserContext);

    // Input states
    const [data, setData] = useState({
        name: '',
        codeNo: '',
        description: '',
        slug: '',
        category: '',
        price: '',
        stockQty: '',
        image: null, // Initialize image as null
    });

    const handleChange = (name) => (e) => {
        const value = name === 'image' ? e.target.files[0] : e.target.value;
        setData({ ...data, [name]: value });
    };

      const handleImageUpload = (file) => {
        // Handle image upload here
        setData({ ...data, image: file });
    };

  
    const handleSubmit = async () => {
        try {
            const formData = new FormData();
            formData.append('image', data.image);
            formData.append('name', data.name);
            formData.append('codeNo', data.codeNo);
            formData.append('description', data.description);
            formData.append('slug', data.slug);
            formData.append('category', data.category);
            formData.append('price', data.price);
            formData.append('stockQty', data.stockQty);

            const token = localStorage.getItem('token');

            const response = await fetch('https://capstone2-diesta.onrender.com/product/registration', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
            },
            body: formData,
        });

                     

                if (response.ok) {
                // Show a success alert
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Product Successfully Submitted!',
                });

                setData({
                    name: '',
                    codeNo: '',
                    description: '',
                    slug: '',
                    category: '',
                    price: '',
                    stockQty: '',
                    image: '',
                
                   });
            } else {
                // Show an error alert
                Swal.fire({
                    title: 'Error!',
                    icon: 'error',
                    text: 'Please try again!',
                });
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        user.isAdmin === true && (
            <Container fluid className="">
                <Row>
                    <Col sm={5} lg={3} xl={2} className="mt-3">
                        <AdminSidebar />
                    </Col>

                    <Col sm={7} lg={9} xl={10}>
                        <div className="admin-content d-flex flex-column align-items-center justify-content-center my-4 border">
                            <div className="mb-3">
                                <h1>ADD PRODUCT</h1>
                            </div>
                            <Container style={{  margin: 'auto' }}>
                                <Row className="d-flex align-items-center">
                                    <Col sm={12} lg={6} className="me-5">
                                        <div>
                                            <Form>
                                                    <Form.Label>Product Name</Form.Label>
                                            </Form>
                                            <input
                                                className="form-control"
                                                placeholder="Enter Product Name"
                                                type="text"
                                                name="name"
                                                value={data.name}
                                                onChange={handleChange('name')}
                                            />
                                        </div>
                                        <div className="my-3">
                                            <Form>
                                                    <Form.Label>Code Number</Form.Label>
                                            </Form>
                                            <input
                                                className="form-control"
                                                placeholder="Enter Code No"
                                                type="text"
                                                name="codeNo"
                                                value={data.codeNo}
                                                onChange={handleChange('codeNo')}
                                            />
                                        </div>
                                        <div className="">
                                            <Form>
                                                    <Form.Label>Description</Form.Label>
                                            </Form>
                                            <input
                                                className="form-control"
                                                placeholder="Enter Description"
                                                type="text"
                                                name="description"
                                                value={data.description}
                                                onChange={handleChange('description')}
                                            />
                                        </div>
                                        <div className="my-3">
                                            <Form>
                                                    <Form.Label>Category Number</Form.Label>
                                            </Form>
                                            <input
                                                className="form-control"
                                                placeholder="Enter Category"
                                                type="text"
                                                name="category"
                                                value={data.category}
                                                onChange={handleChange('category')}
                                            />
                                        </div>
                                        <div className="">
                                            <Form>
                                                    <Form.Label>Amount</Form.Label>
                                            </Form>
                                            <input
                                                className="form-control"
                                                placeholder="Price"
                                                type="number"
                                                name="price"
                                                value={data.price}
                                                onChange={handleChange('price')}
                                            />
                                        </div>
                                        <div className="mt-3 w-75">
                                            <label htmlFor="image">Select Image</label>
                                            <input
                                                id="image" 
                                                className="form-control"
                                                type="file"
                                                accept="image/*"
                                                onChange={handleChange('image')}
                                            />
                                        </div>
                                    </Col>

                                    <Col className="d-flex align-items-center justify-content-center mx-auto mt-2 mt-md-0">
                                        <div className="mb-3">
                                            {data.image && (
                                                <img
                                                    fluid
                                                    src={URL.createObjectURL(data.image)}
                                                    alt="Selected"
                                                    style={{ maxWidth: '25vw' }}
                                                />
                                            )}
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <div className="text-center">
                                        <button className="btn btn-primary" onClick={handleSubmit}>
                                            Submit
                                        </button>
                                    </div>
                                </Row>

                                <Row className="mt-3">
                                <Link to="/all-product"  >View all products</Link>
                                </Row>
                            </Container>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    );
}
