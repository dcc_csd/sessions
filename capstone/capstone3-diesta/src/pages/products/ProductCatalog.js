import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import { TbChevronRight } from 'react-icons/tb';
import { Link } from 'react-router-dom';

export default function ProductCatalog() {

  const [newProducts, setNewProducts] = useState([]);
  const [popularProducts, setPopularProducts] = useState([]);
  const [products, setProducts] = useState([]);
  const [imgs, setImgs] = useState([]);
  const [newPageIndex, setNewPageIndex] = useState(0);
  const [popularPageIndex, setPopularPageIndex] = useState(0);

  const itemsPerPage = 5;

  useEffect(() => {
    fetch('https://capstone2-diesta.onrender.com/product/')
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
        setNewProducts(data.slice(newPageIndex * itemsPerPage, (newPageIndex + 1) * itemsPerPage));
        setPopularProducts(data.slice(popularPageIndex * itemsPerPage, (popularPageIndex + 1) * itemsPerPage));

        let ids = data.map((item) => item._id);
        let imgs = data.map((item) => item.images[0]);
        let productimage = imgs.map((item) => item.url);

        setImgs(productimage);
      })
      .catch((error) => {
        console.error('Error fetching product data:', error);
      });
  }, [newPageIndex, popularPageIndex]);

  const handleNewNextPage = () => {
    setNewPageIndex(newPageIndex + 1);
  };

  const handlePopularNextPage = () => {
    setPopularPageIndex(popularPageIndex + 1);
  };

  const isNewNextPageDisabled = (newPageIndex + 1) * itemsPerPage >= products.length;
  const isPopularNextPageDisabled = (popularPageIndex + 1) * itemsPerPage >= products.length;

  return (
    <div className="mt-3 mb-3">
      {/* New Products */}
      <div className="d-flex mb-2">
        <h5 className="border-bottom border-dark border-3">New Products</h5>
        <p className="ms-auto">
          <button
            onClick={handleNewNextPage}
            className="text-secondary btn"
            style={{ fontSize: '18px', padding: '10px' }}
            disabled={isNewNextPageDisabled}
          >
            View More <TbChevronRight />
          </button>


        </p>
      </div>
      <div className="d-flex gap-1">
        {newProducts.map((product, index) => (
          <div key={product._id}>
            <Link to={`product/${product._id}`}>
              <Card.Img
                src={imgs[newPageIndex * itemsPerPage + index]}
                alt={`Image 0`}
              />
            </Link>
          </div>
        ))}
      </div>

      {/* Popular Products */}
      <div className="mt-5">
        <div className="d-flex mb-2">
          <h5 className="border-bottom border-dark border-3">Popular Products</h5>
          <p className="ms-auto">
            <button
              onClick={handlePopularNextPage}
              className="text-secondary btn"
              style={{ fontSize: '18px', padding: '10px' }}
              disabled={isPopularNextPageDisabled}
            >
              View More <TbChevronRight />
            </button>
          </p>
        </div>
        <div className="d-flex gap-1">
          {popularProducts.map((product, index) => (
            <div key={product._id}>
              <Link to={`product/${product._id}`}>
                <Card.Img
                  src={imgs[popularPageIndex * itemsPerPage + index]}
                  alt={`Image 0`}
                />
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
