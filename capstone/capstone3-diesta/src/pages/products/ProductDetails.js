import {useState,useEffect, useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function AddCourse(){

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    //input states
    const [name,setName] = useState("");
    const [codeNo,setCodeNo] = useState("");
    const [description,setDescription] = useState("");
    const [slug,setSlug] = useState("");
    const [category,setCategory] = useState("");
    const [price,setPrice] = useState("");
    const [stockQty,setStockQty] = useState("");

    function createProduct(e){

        //prevent submit event's default behavior
        e.preventDefault();

        let token = localStorage.getItem('token');
        console.log(token);

        fetch('https://capstone2-diesta.onrender.com/product/',{

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({

                name: name,
                codeNo: codeNo,
                description: description,
                slug: slug,
                category: category,
                price: price,
                stockQty: stockQty


            })
        })
        .then(res => res.json())
        .then(data => {

            //data is the response of the api/server after it's been process as JS object through our res.json() method.
            console.log(data);

            if(data){
                Swal.fire({

                    icon:"success",
                    title: "Product Added"

                })

                navigate("/admin-dashboard");
            } else {
                Swal.fire({

                    icon: "error",
                    title: "Unsuccessful Product Creation",
                    text: data.message

                })
            }

        })

   
    }

    return (

            (user.isAdmin === true)
            ?
            <>
                <h1 className="my-5 text-center"></h1>
                <Form onSubmit={e => createProduct(e)}>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" 
                        required value={name} 
                        onChange={e => {setName(e.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Code #</Form.Label>
                        <Form.Control type="text"
                         required value={codeNo} 
                         onChange={e => {setCodeNo(e.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" 
                        required value={description}
                        onChange={e => {setDescription(e.target.value)}}/>
                    </Form.Group>

                     <Form.Group>
                        <Form.Label>Slug</Form.Label>
                        <Form.Control type="text" 
                         required value={slug} 
                         onChange={e => {setSlug(e.target.value)}}/>
                    </Form.Group>

                     <Form.Group>
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" 
                         required value={price} 
                         onChange={e => {setPrice(e.target.value)}}/>
                    </Form.Group>

                     <Form.Group>
                        <Form.Label>Category</Form.Label>
                        <Form.Control type="category" 
                        required value={category} 
                        onChange={e => {setCategory(e.target.value)}}/>
                    </Form.Group>

                     <Form.Group>
                        <Form.Label>Stock #</Form.Label>
                        <Form.Control type="number" 
                        required value={stockQty} 
                        onChange={e => {setStockQty(e.target.value)}}/>
                    </Form.Group>

                    <Button variant="primary" type="submit" className="my-5">Submit</Button>

                </Form>
            </>
            :
            <Navigate to="/products" />

    )


}