import React, { useState, useEffect } from 'react';

import AdminSidebar from '../../components/Admin/Sidebar/AdminSidebar';
import ProductTable from '../../components/Products/ProductTable'; 
import { Link } from 'react-router-dom';




const ProductList = () => {
  const [product, setProduct] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    // Fetch users from your MongoDB backend
    fetch('https://capstone2-diesta.onrender.com/product')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => setProduct(data))
      .catch((error) => console.error('Error fetching products:', error));
  }, []);







  const handleEdit = (productId) => {
    const productToEdit = product.find((p) => p._id === productId);
    if (!productToEdit) {
      console.error(`Product with ID ${productId} not found.`);
    } else {
      setSelectedProduct(productToEdit);
    }
  };

  const linkStyle = {
    textDecoration: 'none',
    color: 'blue',
  };

  const containerStyle = {
    display: 'flex',
  };

  const sidebarStyle = {
    width: '250px', // Adjust the width of the sidebar as needed
  };

  const contentStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    padding: '20px', // Add padding to the content area as needed
  };

  const headerStyle = {
    marginBottom: '25px',
  };

  const headingStyle = {
    fontSize: '24px',
    marginTop: '10px', // Add margin to the top of the heading
    marginLeft: '10px', // Add margin to the left of the heading
  };

  return (
    <div className="product-view">
      <div className="user-product-header" style={headerStyle}>
      
      </div>

      <div style={containerStyle}>
        <div style={sidebarStyle}>
          {/* Include the AdminSidebar component on the left */}
          <AdminSidebar />
        </div>

        <div style={contentStyle}>
          
          <h1 className="text-left" style={headingStyle}>
            PRODUCT LIST
          </h1>
          {/* Place the UserTable component inside the second container */}
           <ProductTable product={product} />
          {/* Add the search button or component here */}
        </div>
      </div>
    </div>
  );
};

export default ProductList;

