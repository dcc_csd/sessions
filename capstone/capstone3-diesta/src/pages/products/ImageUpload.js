import React, { useState } from 'react';

export default function ImageUpload() {
  const [selectedImage, setSelectedImage] = useState(null);

  // Function to handle image selection
  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        setSelectedImage(reader.result);
      };
      reader.readAsDataURL(file);
    }
  };

  return (
    <div>
      <h3 className="display-3">Upload Image</h3>
      <div className="form-group">
        <input
          type="file"
          accept="image/*"
          onChange={handleImageChange}
          className="form-control-file"
        />
      </div>
      {selectedImage && (
        <div>
          <h4 className="mt-3">Selected Image:</h4>
          <img
            src={selectedImage}
            alt="Preview"
            style={{ maxWidth: '400px' }}
          />
        </div>
      )}
    </div>
  );
}
