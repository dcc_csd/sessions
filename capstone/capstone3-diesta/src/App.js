import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState, createContext, useEffect } from 'react'
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';

import './App.css';


import AppNavbar from './components/AppNavbar';
import Navbar from './components/Navbar/navbar';
import UserProfile from './components/UserProfile';
import UserGrid from './components/Admin/UserGrid';
import Error from './components/Error';


// ADDITIONAL
import Footer from './components/Footer';
import FrontAd from './components/home/FrontAd';
import Highlights from './components/home/Highlights';
import ProductDisplay from './components/ProductDisplay';

import DashboardUserView from './pages/UserView/DashboardUserView';
import OrdersUserView from './pages/UserView/OrdersUserView';
import WishlistUserView from './pages/UserView/WishlistUserView';
import AddressesUserView from './pages/UserView/AddressesUserView';
import AccountUserView from './pages/UserView/AccountUserView';
// import Cart from './pages/Cart';

// 

import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/login/Register';
import CourseView from './pages/CourseView';
import Login from './pages/login/Login';
import Logout from './pages/Logout';


import ProductCatalog from './pages/products/ProductCatalog';
import AdminDashboardPage from './pages/Admin/AdminDashboardPage';
import UserView from './pages/Admin/UserView';
import UserList from './pages/Admin/UserList';
import FAQ from './pages/Admin/FAQ';
import NewProducts from './pages/products/NewProducts';
import ProductList from './pages/products/ProductList';
import EditProduct from './components/Products/EditProduct';
import FeaturedProducts from './pages/home/FeaturedProducts';

//Products
import PDetails from './pages/UserView/PDetails';
import SingleProductDetails from './pages/UserView/SingleProductDetails';
import ViewAllNewProducts from './components/Products/ViewAllNewProducts';
import ViewAllPopularProducts from './components/Products/ViewAllPopularProducts';


//Order
import Cartview from './pages/cart/Cartview';
import OrderCheckout from './pages/orders/OrderCheckout';
import OrderList from './pages/orders/OrderList';
import UserOrderList from './pages/orders/UserOrderList';
import UserOrderSummary from './pages/orders/UserOrderSummary';

export const DataContainer = createContext();
function App() {
 

 const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  
  const unsetUser = () => {
    localStorage.clear();
  };

 const [show, setShow] = useState(true);

 const [cart, setCart] = useState([]);

 const handleAddToCart = (productDetails) => {
    setCart([...cart, { productDetails }]);
  };

 
  useEffect(() => {
  
    fetch(`https://capstone2-diesta.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (typeof data._id !== 'undefined') {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        } else {
        
          setUser({
            id: null,
            isAdmin: null
          });
        }
      });
  }, []);

  const [CartItem, setCartItem] = useState([])
  
  const [selectedProduct,setSelectedProduct]=useState(null);

  const addToCart = (product,num=1) => {
    const productExit = CartItem.find((item) => item.id === product.id)
    if (productExit) {
      setCartItem(CartItem.map((item) => (item.id === product.id ? { ...productExit, qty: productExit.qty + num } : item)))
    } else {
      setCartItem([...CartItem, { ...product, qty: num }])
    }
  }

  const decreaseQty = (product) => {
    const productExit = CartItem.find((item) => item.id === product.id)
    // If product quantity == 1 then we have to remove it
    if (productExit.qty === 1) {
      setCartItem(CartItem.filter((item) => item.id !== product.id))
    }
    //else we just decrease the quantity 
    else {
      setCartItem(CartItem.map((item) => (item.id === product.id ? { ...productExit, qty: productExit.qty - 1 } : item)))
    }
  }

  const deleteProduct = (product)=> {
      setCartItem(CartItem.filter((item) => item.id !== product.id))

  }
  useEffect(()=> {
      localStorage.setItem("cartItem",JSON.stringify(CartItem));
  },[CartItem])


  return (
    <DataContainer.Provider value={{ cart, setCart, handleAddToCart, decreaseQty, deleteProduct, selectedProduct, setSelectedProduct }}>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar size={cart.length} />
          <Container fluid>
            <Routes>

              // USER
              <Route path="/dashboard" element={<DashboardUserView />} />
             
              <Route path="/wishlist" element={<WishlistUserView />} />
              <Route path="/addresses" element={<AddressesUserView />} />
              <Route path="/accountDetails" element={<AccountUserView />} />


              <Route path="/" element={<Home />} />
              <Route path="/product/" element={<ProductCatalog />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              
              <Route path="/logout" element={<Logout />} />
              <Route path="/*" element={<Error />} /> 
              
              <Route path="/profile" element={<UserProfile />} />
              <Route path="/faqs" element={<FAQ />} />
              <Route path="/user-order2/:userId" element={<UserOrderSummary />} />


              // ADMIN
              <Route path="/admin-dashboard" element={<AdminDashboardPage />} />

              {user.isAdmin === true && (
                <>
                  <Route path="/admin-dashboard" element={<AdminDashboardPage />} />
                  <Route path="/admin-list-page" element={<UserView />} />
                  <Route path="/user-list" element={<UserList />} />
                  <Route path="/add-product" element={<NewProducts />} />
                  <Route path="/all-product" element={<ProductList />} />
                  <Route path="/all-order" element={<OrderList />} />
                  <Route path="/edit-product/:productId" element={<EditProduct />} />
                  <Route path="/featured-products/" element={<FeaturedProducts />} />
                </>
            )}

               //Visitors
                  <Route  path="/" elements={<FeaturedProducts />} />

                  <Route path="/single-product/:productId" element={<SingleProductDetails handleAddToCart={handleAddToCart} />} />
                  <Route path="/product/product/:productId" element={<PDetails  />} />
                  <Route path="/user-cart-view/:userID" element={<Cartview />} />
                  <Route path="/cart" element={Cartview} />
                  <Route path="/checkout" element={OrderCheckout} />
                  <Route path="/faq" element={<FAQ />} />

          </Routes>
        </Container>
        <Footer />
      </Router>
    </UserProvider>
  </DataContainer.Provider>
  );
}

export default App;


