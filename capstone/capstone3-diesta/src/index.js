import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { UserProvider } from './UserContext';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import { CartProvider } from './pages/single/acart';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
 
  <UserProvider>
  <App />
  </UserProvider>
 
  </React.StrictMode>
);


