const express = require("express");

const app = express();
const port = 4000;

//Middlewares
app.use(express.json());
//Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));
//accepts all data type


//[SECTION] Routes
//get method
app.get("/", (req, res) => {
	res.send("Hello World!");
})

app.get("/hello", (req, res) => {
	res.send("Hello from /hello end point.");
})



//post method
app.post("/hello", (req, res) => {
	res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`);
})

//post ROUTE to register user
let users = [];

app.post("/signup", (req, res) =>{
	console.log(req.body);

	if(req.body.username !=="" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	}else{
		res.send("Please input BOTH username and password.");
	}

})

app.put("/signup", (req, res) => {
	let message;
	for (let i = 0; i < users.length; i++) {
		if (req.body.username !== "" && req.body.username === users[i].username){
			message = "Account already registered.";
			break;
		}else{
			message = "Please register.";
		}

		
	}
	res.send(message);
	
})


//Change password using put method
//put method
app.put("/change-password", (req, res) => {
	let message; 

	for (let i = 0; i < users.length; i++){
		if(req.body.username === users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`;
				break;
		}else{
			message = "User does not exist.";
		}
	}
	res.send(message);
})




if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
} 


