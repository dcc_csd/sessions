const http = require('http');

const port = 4000;

const server = http.createServer((req, res) => {
// console.log(req)

    if(req.url ==='/'){
         
        res.end('Welcome to our Home Page.')
            
    }

    else if (req.url ==='/about'){
        
        res.end('Here is our short history')
    }

    else {
        res.end(`
      <h1> Ooops! </h1>
      <p> We can't seem to find the page you are looking for </p>
      <a href="/">back home</a>
      `)
    }





})

// server.listen(4000)
server.listen(port, () => console.log('Server is running at localhost:4000'));