//[section] Dependencies and Modules
const express = require('express');
const courseController = require('../controllers/course.js');

const auth = require("../auth.js")
//destructure from auth
const {verify, verifyAdmin} = auth;

//[section] Routing Component
const router = express.Router();




// create a course POST Method routes
    router.post('/', verify, verifyAdmin, courseController.addCourse);


// Get all courses
    router.get('/all', courseController.getAllCourses);


//Get all "active" course

   router.get("/", courseController.getAllActive);

//Get 1 specific course using its ID
   router.get("/view/:courseId", courseController.getCourse);


//Updating a Course (Admin Only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);


//Archive a Course (Admin Only)
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);


//Re-activate Course (Admin Only)
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

router.get("/archive", courseController.getAllArchive);




    module.exports = router;