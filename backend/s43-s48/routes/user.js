//[section] Dependencies and Modules
// [ SECTION ] Dependencies and Modules
const express = require('express');
const auth = require("../auth.js")
const {verify, verifyAdmin} = auth;
const router = express.Router();
const userController = require('../controllers/user');




// check email routes
router.post('/checkEmail', (req, res) =>{
	userController.checkEmailExist(req.body).then(
		resultFromController => res.send (resultFromController));
});

//user registration routes
router.post('/register',(req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});



//user authentication

router.post('/login', userController.loginUser);


//user details

router.get("/details", verify, userController.getProfile);



//Enroll user to a course
router.post("/enroll", verify, userController.enroll);


//Get all enrollments
router.get("/getEnrollments", verify, userController.getEnrollments);

// router.post('/login',(req,res) =>{
// 	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
// });

//[section] export route system


//Reset password
router.put("/reset-password", verify, userController.resetPassword);



module.exports = router;


