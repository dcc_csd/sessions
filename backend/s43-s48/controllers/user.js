//[section] Dependencies and modules
const User = require('../models/User');
const Course = require("../models/Course");

const bcrypt = require('bcrypt');

const auth = require('../auth');


//check email exists
/*

Steps:
1. use mongoose 'find' method to find duplicate emails 
2. Use the 'then ' method to send a response back to the frontend application based on the result of the "find" method.

*/
module.exports.checkEmailExist = (reqBody) =>{
	console.log(reqBody);
	return User.find({email: reqBody.email}).then(result =>{
		console.log(result);
		if(result.length > 0){
			return true
		}else {
			return false
		};

	});
}; 


// user registration
/*
Steps
1.Creates a new User object using the mongoose model and the informaion from the request body

*/

module.exports.registerUser = (reqBody) =>{
	console.log(reqBody);
	let newUser = new User ({
		 
		firstName: reqBody.firstName,
		lastName:reqBody.lastName,
		email:reqBody.email,
		mobileNo:reqBody.mobileNo,
		password:bcrypt.hashSync(reqBody.password,10)
	});

	return newUser.save().then((user, error) => {
		if (error){
			return false
		}else{
			return true
		}
	})
	.catch(err => err);
};

//user authentication
module.exports.loginUser = (req, res) =>{
      return User.findOne({email: req.body.email}).then(result => {
      	console.log(result);
      	if(result === null){
      		return res.send (false) // the email doesnt' exist in our DB
      	} else{
      		const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

      		console.log(isPasswordCorrect);

      		if(isPasswordCorrect){
      			return res.send({ access: auth.createAccessToken(result)})
      		}else{
      			return res.send(false); //Passwords do not match
      		}
      	}
      })
      .catch(err => res.send(err))

}


//my solution
// module.exports.userDetails = (reqBody) =>{
// 	console.log(reqBody);
// 	return User.findOne({_id: reqBody.id},).then(result =>{
// 		console.log(result);
// 		if(result ===null){
			
// 			return res.send ("Cannot find data with the provided ID")
// 		}else {
// 			result.password = ""
// 			return result
			
// 			}

				
// 	});
// }; 

//rominick solution retieve user details
// module.exports.getProfile = (req, res) =>{
// 	return User.findById(req.user.id).then(result =>{
// 		result.password ="******"
// 		return result
// 	})
//     .catch(error => error)
// }

	
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = ""
		return res.send(result);
	})
	.catch(error => error)
}	


//Enroll student
/*module.exports.enroll = async(req, res) => {
	console.log(req.user.id);
	console.log(req.body.courseId);

	if(req.user.isAdmin){
		return res.send("You cannot do this action.")
	}
	let isUserUpdated = await User.findById(req.user.id).then(user => {
            let newEnrollment = {
        	courseId: req.body.courseId,
        	}

        let listofEnrollments = user.enrollments; 
        let filteredCourseId =[];
        listofEnrollments.foreach((result) =>{
	  console.log(result)
	  filteredCourseID.push(result.courseId);
        })


       if(user.enrollments.inlcudes({courseId: req.body.couseId})){
	  return res.send("You are already enrolled for this course.")
        	};
         
          user.enrollments.push(newEnrollment);
          return user.save().then(user => true).catch(error => res.send(error));
	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course =>{
		let enrollee = {
			userId: req.user.id
		}
		course.enrollees.push(enrollee);
		return course.save().then(course => true).catch(error => res.send(error))
	})

	if(isCourseUpdated !== true){
		return res.send({message : isCourseUpdated});
	}
	if(isUserUpdated && isCourseUpdated){
		return res.send("You are now enrolled to a course. Thank you!");
	}
}
*/
// Enroll student
module.exports.enroll = async (req, res) => {
  try {
    console.log(req.user.id);
    console.log(req.body.courseId);

    if (req.user.isAdmin) {
      return res.status(403).send("You cannot perform this action.");
    }

    // Find the user (student) by their ID
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).send("User not found.");
    }

    // Find the course by its ID
    const course = await Course.findById(req.body.courseId);

    if (!course) {
      return res.status(404).send("Course not found.");
    }

    // Check if the user is already enrolled in this course
    const isAlreadyEnrolled = user.enrollments.some((enrollment) => enrollment.courseId.toString() === req.body.courseId);
    if (isAlreadyEnrolled) {
      return res.status(400).send("You are already enrolled in this course.");
    }

    // Create a new enrollment object with course details
    let newEnrollment = {
      courseId: course._id,
      name: course.name,
      description: course.description,
      price: course.price,
    };

    // Add the enrollment to the user's enrollments array
    user.enrollments.push(newEnrollment);

    // Save the updated user
    await user.save();

    // Create a new enrollee object with user details
    let newEnrollee = {
      userId: req.user.id,
      firstName:user.firstName,
      lastName:user.lastName 
    };

    // Add the enrollee to the course's enrollees array
    course.enrollees.push(newEnrollee);

    // Save the updated course
    await course.save();

    return res.send("You are now enrolled in the course. Thank you!");
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).send("Internal Server Error");
  }
};


//Get Enrolled Student
module.exports.getEnrollments = (req, res) => {
	// if(req.user.isAdmin){
	// 	return res.send("You cannot do this action.")
	// }
	User.findById(req.user.id).then(result => {
		if(result.enrollments ==[]|| result.enrollments ==null){
			return res.send("You are not enrolles to any courses")
		}else{
			res.send(result.enrollments);
		}
	}).catch(err => res.send(err))
}
		
	     	// if(result === null){
	     	// 	console.log(result)
      	// 	return res.send ("Not Yet Enroll") // no registered student
      	// } else{
      	// 	console.log(result)
      	// 	 return res.send(result.enrollments)
      
//
module.exports.resetPassword = async (req, res) => {

try {
    const newPassword = req.body.newPassword;
    const userId = req.user.id;

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Update the user's password in the database
    await User.findByIdAndUpdate(userId, {
      password: hashedPassword
    });

    res.status(200).json({ message: "Password reset successfully" });
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};