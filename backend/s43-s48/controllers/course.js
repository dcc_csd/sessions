//[section] Dependencies and modules
const Course = require('../models/Course.js');
const userController = require('../controllers/User');
const auth = require('../auth');


//register course
module.exports.addCourse = (req, res) =>{
	let newCourse = new Course ({
		 
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
			
	});

	return newCourse.save().then((course, error) => {
		if (error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send (error));
};


//get all courses
module.exports.getAllCourses = (req, res) =>{
	return Course.find({}).then(result =>{
		if(result.lengt === 0){
			return res.send("There is no courses in the DB.");
		}else{
			return res.send(result);
		}
	})
}


//get all active
module.exports.getAllActive = (req, res)=>{
   return Course.find({isActive: true}).then(result =>{
   	if (result.lengt ===0){
   		return res.send("There is currently no active course.")
   	}else{
   		return res.send(result);
   	}
   })
}

//get specific active course
module.exports.getCourse = (req, res) => {
	return Course.findById(req.params.courseId).then(result =>{
		// if(result === 0){
		// 	return res.send("Cannot find course with the provided ID.")
		// }else {
		// 	return res.send(result);

		// }
			if(result === 0){
			return res.send("Cannot find course with the provided ID.")
		}else {
			if(result.isActive === "false"){
				return res.send("The course you are trying to access is not available");
			} else{
				return res.send(result);
			}

		}
	})
	.catch(error => res.send("Please enter a correct course ID"));
}

//Updating a Course (Admin Only)

module.exports.updateCourse = (req, res) =>{
	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	} 
	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) => {
		if(error){
			return res.send(false);

		}else{
			return res.send(true);
		}
	})

}

//Archive a Course (Admin Only)

module.exports.archiveCourse = (req, res) =>{
	let archivedCourse = {
		isActive: false
		// isActive: req.body.isActive
		
	} 
	return Course.findByIdAndUpdate(req.params.courseId, archivedCourse).then((course, error) => {
		if(error){
			return res.send(false);

		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
	

}

//Activate a Course (Admin Only)

module.exports.activateCourse = (req, res) =>{
	let activatedCourse = {
		isActive: true
		// isActive: req.body.isActive
		
	} 
	return Course.findByIdAndUpdate(req.params.courseId, activatedCourse).then((course, error) => {
		if(error){
			return res.send(false);

		}else{
			return res.send(true);
		}
	})
    .catch(error => res.send(error));
}

//get all archive course
module.exports.getAllArchive = (req, res)=>{
   return Course.find({isActive: false}).then(result =>{
   	if (result.lengt ===0){
   		return res.send("There is currently no archive course.")
   	}else{
   		return res.send(result);
   	}
   })
}

