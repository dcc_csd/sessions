
//[section] Dependencied and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');


require('dotenv').config();

//allows access to routes defined within our application
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course.js');


//[section] Environment Setup
const port = 4000;

//[section] Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Allows all resources to access our backend application
app.use(cors()); 

//[section] Database Connection
mongoose.connect('mongodb+srv://admin:admin123@cluster0.ztpawdb.mongodb.net/S43-48?retryWrites=true&w=majority',{
	useNewUrlParser : true, 
	useUnifiedTopology : true
  });

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

//[section]Backend Routes
app.use('/users', userRoutes);

//backend routes for course
app.use('/course', courseRoutes);

//[section] Server Gateway Respose

if(require.main === module){
	app.listen(process.env.PORT|| port, () => {
		console.log(`API is now online port ${process.env.PORT || port}`);
	});
};

module.exports =app;


