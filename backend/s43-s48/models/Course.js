//this is the dbase - table
//[section] Dependencies and Modules

const mongoose = require('mongoose');

//[section] Schema/Blueprint

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required :[true, 'Course is required!']
	      },

	description: { 
		type: String,
	    required:[true, 'Description is required!']
	       },
	price: {
		type: Number,
		required:[true, 'Price is required!']
	       },

	isActive: {
		type: Boolean,
	    default: true
	},
	createdOn :{
		type: Date,
		//The "new Date()" expression that instantiates a new "date" that store the current time and date whenever a course is created in our database
	    default: new Date()
	},
	enrollees : [
	{
		userId:{
			type: String,
			required:[true, 'UserId is required!']
	       },
        
        firstName: {
		type: String		
	      },

	    lastName: { 
		type: String
	    	       },

	    enrolledOn : {
			type: Date,
			default: new Date()
	       }

		}

	]

});



//[section] Model

module.exports = mongoose.model('Course', courseSchema);