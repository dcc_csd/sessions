//[section] Dependencies and Modules

const mongoose = require('mongoose');

//[section] Schema/Blueprint

const ObjectId = mongoose.Types.ObjectId;


const userSchema = new mongoose.Schema({
	
   	firstName: {
		type: String,
		required :[true, 'First name is required!']
	      },

	lastName: { 
		type: String,
	    required:[true, 'Last name is required!']
	       },

	email: { 
		type: String,
	    required:[true, 'Email is required!']
	       },

	password: { 
		type: String,
	    required:[true, 'Password is required!']
	       },


    isAdmin: {
		type: Boolean,
	    default: false
	       },

	// accountType:{
	// 	type: ["student", "teacher", "Dean", "guardian"]
	// } to be confirm

    mobileNo: { 
		type: String,
	    required:[true, 'Mobile No is required!']
	       },

	enrollments : [
	{
		courseId:{
			type: String,
			required:[true, 'Course ID is required!']
	       },

	    name: {
		type: String
		
	      },

	description: { 
		type: String
	    
	       },

	price: {
		type: Number
		
	       },
	    

	    enrolledOn : {
			type: Date,
			default: new Date()
	       },


	    status : {
			type: String,
			default: "Enrolled"
	       }

		}

	]

})





//[section] Model

module.exports = mongoose.model('User', userSchema);
