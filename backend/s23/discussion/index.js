// console.log("Hello World!");
// [Section]  - if, else if, else statement

let numA= -1;
 
 //if
console.log("1");

if (numA < 0){
  console.log("Hello");
}


//rejected false not less than sa 0 

console.log("Sample 2");

if (numA < -5){
  console.log("Hello");
}


console.log("Sample 3");

//true condition

if(numA <5){
  console.log("Hello");
}

//false condition - will not execute the code

console.log("Sample 4");
if(numA <-5){
  console.log("Hello");
}

//string case sensitive


console.log("Sample 5");

let city="New York";

if(city === "New York"){
  console.log("Welcome to New York City!");
}

console.log("Sample 6");


// else if mauuna muna si if

let numH = 1; 

if(numH < 0){
  console.log("Hello!");
}else if (numH > 0 ){
   console.log("World!");  
}


console.log("Sample 7");

city ="Tokyo";

if(city === "New York"){
  console.log("Welcome to New York City!");

}else if(city==="Tokyo"){
  console.log("Welcome to Tokyo Japan!")

}

console.log("Sample 8");

//else statement
let num1 =5;
if(num1 === 0){
  console.log("Hello");
}
else if (num1 === 3){
  console.log("World");
} 

else {
  console.log("Sorry all conditions are not met.")
}


console.log("Sample 9");
//if, else if, else with functions

function determineTyphoonIntensity(windSpeed){
  if(windSpeed < 30){
    return "Not a typhoon yet";

  }
  else if(windSpeed <= 61){
    return"Tropical Depression detected."
  }
  else if (windSpeed >= 62 && windSpeed <= 88){
    return "Tropical Storm detected."
  }
  else if(windSpeed >= 89 && windSpeed <= 117){
    return " Severe Tropical Storm detected.";
  }
  else{
    return "Typhoon Detected"
  }
}

let message = determineTyphoonIntensity(25);

message = determineTyphoonIntensity(85);
message = determineTyphoonIntensity(250);

console.log(message);

console.log("Sample 10");


//Truthy

if (true){
  console.log("truthy");
}

if (1){
  console.log("truthy");
}

if([]){
  console.log("truthy")
}

// Falsy

if(false){
  console.log("Falsy")
}


if(0){
  console.log("Falsy")
}

if(undefined){
  console.log("Falsy")
}

console.log("Sample 11");
//Single statement execution
//ternary statement
// let ternary =(1 < 18) ? "Yes" : "No"
// console.log(ternary);

let num2 = 10;
let ternary =(num2 < 18 || num2 !== "10") ? "Yes" : "No"
console.log(ternary);

//Multiple Statement

let name;

function isOfLegalAge(){
  name="John"
  return "You are of the legal age limit."
}

function isUnderAge() {
  name = "Jane"
  return "You are under the age limit."
}

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of ternary operator in functions:  "  +  legalAge  + ",  "  + name); 


console.log("Sample 12");

// switch - case statement


let day = prompt("What day of the week is it today?").toLowerCase()
console.log(day);

switch (day){
  case "monday" : 
   console.log("The color of the day is red.") 
   break;

 case "tuesday" :
  console.log("The color of the day is orange.")
  break;

case "wednesday" :
  console.log("The color of the day is yellow.")
  break;

case "thursday" :
  console.log("The color of the day is green.")
  break;

case "friday" :
  console.log("The color of the day is blue.")
  break;

case "saturday" :
  console.log("The color of the day is indigo.")
  break;

case "sunday" :
  console.log("The color of the day is violet.")
  break;

default:
  console.log("please enter a valid day!")
  break;
}

