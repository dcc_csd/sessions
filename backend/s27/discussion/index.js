// console.log("Hello World!");
//Arrays Mutators

let fruits  = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

//push();
//Add element in an array

console.log ("Current Array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log("Array after push method");
console.log(fruitsLength);
console.log(fruits);

//Push multiple elements
fruits.push("Avocado", "Guava");
console.log("Array after push method");
console.log(fruits);

function addFruit(fruit){
  //push parameter
  fruits.push(fruits);
  console.log(fruit);

}

addFruit("Pineapple");

//pop()
//removes element at the end of an array

fruits.pop();
fruits.pop();
console.log(fruits);

function removeFruit(){
  fruits.pop();
  console.log(fruits);
}

removeFruit();

//unshift()

//add element in the beginning of an array

fruits.unshift("Lime", "Banana");
console.log("Array after unshift method:")
console.log(fruits);

function unshiftFruit(fruit){
  fruits.unshift(fruit);
  console.log(fruits);
}
unshiftFruit("Calamansi");


//shift()
//remove an element in the beginning of an array

fruits.shift();
console.log("Array after shift method");
console.log(fruits);

function shiftFruit(){
  fruits.shift();
  console.log(fruits);
}

shiftFruit();

//splice()
// simulatenously removes elements from a splices index number and adds elements
//syntax : array.splice(startIndex, deleteCount, elementsToBeAdded);

fruits.splice(1, 2, "Avocado");
console.log("Array adter splice method");
console.log(fruits);


function spliceFruit(index, deleteCount, fruit){
  fruits.splice(index, deleteCount, fruit)
  console.log(fruits);
}

spliceFruit(1, 1, "Cherry");
spliceFruit(2, 1, "Durian");

//sort()
//arranges the elements in aplhanumeric order

fruits.sort()
console.log("Arrays after sort method");
console.log(fruits);

//reverse()

fruits.reverse()
console.log("Arrays after reverse method");
console.log(fruits);













