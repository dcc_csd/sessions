//[section] Dependencies and modules

const Token = require("../models/token");
const sendEmail = require("../utils/email");
const crypto = require("crypto");
const { User, validate } = require("../models/User");
const userController = require('../controllers/userController');
const auth = require('../utils/auth');
const cloudinary = require ('../utils/cloudinary');
const multer = require("../utils/multer");



const upload = multer.single("avatar");
const fs = require('fs');


const bcrypt = require('bcrypt');

const {verify, verifyAdmin} = require('../utils/auth');

const jwt = require('jsonwebtoken');


//Section 1 - Creating User - User Control
  
//a. signup 
    module.exports.signupUser = async (req, reqBody) => {
    try {

    const result = await cloudinary.uploader.upload(req.file.path);

    const { error } = validate(reqBody);
    if (error) {
      return { success: false, message: error.details[0].message };
    }

    let user = await User.findOne({ email: reqBody.email });
    if (user) {
      return { success: false, message: "User with this email already exists" };
    }

    let newUser = new User({
      name: reqBody.name,
      email: reqBody.email,
      phoneNumber: reqBody.phoneNumber,
      password: bcrypt.hashSync(reqBody.password, 10),
      avatar: {
        public_id: result.secure_url,
        url: result.public_id,
      },

    });

    await newUser.save();

    let token = new Token({
      userId: newUser._id,
      token: crypto.randomBytes(32).toString("hex"),
    });

    await token.save();

    const message = `${process.env.BASE_URL}/user/verify/${newUser.id}/${token.token}`;
    await sendEmail(newUser.email, "Verify Email", message);

    return { success: true, message: "User created successfully" };
    } catch (error) {
    console.error(error);
    return { success: false, message: "Internal Server Error" };
   }
  };


//b. activation by email
  
  module.exports.verifyEmail = async (req, res) => {
   try {
    const user = await User.findOne({ _id: req.params.id });
    if (!user) return res.status(400).send("Invalid link");

    const token = await Token.findOne({
      userId: user._id,
      token: req.params.token,
    });
    if (!token) return res.status(400).send("Invalid link");

    await User.updateOne({ _id: user._id }, { verified: true });
    await Token.findByIdAndRemove(token._id);

    res.send("Email verified successfully");
  
   } catch (error) {
    console.error(error);
    res.status(500).send("An error occurred");
   }
 };


//c. login user with authentication
  module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email }).then((result) => {
    console.log(result);

    if (result === null) {
      return res.status(403).send ("You cannot perform this action.");
   
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );

    console.log(isPasswordCorrect);

    if (isPasswordCorrect) {
    const accessToken = auth.createAccessToken(result);

    // Send token as a response
    return res.status(200).json({ access: accessToken });
      } else {
        return res.status(400).json({message: "User doesn't exists!"}); 
        }
    }
  }).catch((err) => res.send(err));
 };


//d. Update user password
  module.exports.resetPassword = async (req, res) => {
 
   try {
    const newPassword = req.body.newPassword;
    const userId = req.user.id;

    if (!newPassword) {
      return res.status(400).json({ message: "New password is required" });
     }

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Update the user's password in the db
    await User.findByIdAndUpdate(userId, {
      password: hashedPassword
     });

    res.status(200).json({ message: "Password reset successfully" });
   } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({ message: "Internal Server Error" });
    }
  };


//e. Update user profile including Avatar
 module.exports.updateUserAndAvatar = async (userId, reqBody, avatarFile) => {
  try {
    const { error } = reqBody;
    if (error) {
      return { success: false, message: error.details[0].message };
    }

    const user = await User.findById(userId);

    if (!user) {
      return { success: false, message: "User not found" };
    }

    // deletion
    const oldAvatarPublicId = user.avatar.public_id;

    // Update user details
    user.name = reqBody.name;
    user.email = reqBody.email;
    user.phoneNumber = reqBody.phoneNumber;
    user.country = reqBody.country;
    user.city = reqBody.city;
    user.address = reqBody.address;
    user.zipCode = reqBody.zipCode;

    // Upload new avatar
    if (avatarFile) {
      const result = await cloudinary.uploader.upload(avatarFile.path);

    // Update user's avatar URL
      user.avatar.public_id = result.public_id;
      user.avatar.url = result.secure_url;

    // Delete the old avatar
      if (oldAvatarPublicId) {
        await cloudinary.uploader.destroy(oldAvatarPublicId);
      }

    // Delete the local upload folder
      fs.unlinkSync(avatarFile.path);
    }

    // Save the updated user
    await user.save();
    return { success: true, message: "User details updated successfully" };
  } catch (error) {
    console.error(error);
    return { success: false, message: "Internal Server Error" };
  }
 };

  


//Section 2 - Admin Control

//a. Get identified user

   module.exports.getUser = async (req, res) => {
   try {
    if (!req.user.isAdmin) {
      return res.status(403).send("You cannot access this user's details.");
    }

    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).send("User doesn't exist.");
    }

    return res.status(200).json(user);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
 };


//b. Get initial user details - User ID


  module.exports.getByUserId = async (req, res) => {
   try {
     if (!req.user.isAdmin) {
       return res.status(403).send("You cannot access this user's details.");
     }

     const user = await User.findById(req.user.id)
      .select('name email phoneNumber verified isAdmin _id avatar.public_id')
      .exec();

     if (!user) {
       return res.status(404).send("User doesn't exist.");
     }

     const formattedUser = {
      name: user.name,
      email: user.email,
      phoneNumber: user.phoneNumber,
      verified: user.verified,
      isAdmin: user.isAdmin,
      id: user._id,
      avatarPublicUrl: user.avatar.public_id,
     };

     return res.status(200).json(formattedUser);
    } catch (error) {
     console.error(error);
     return res.status(500).json({ message: 'Internal Server Error' });
    }
  };


//c. get all active users
  module.exports.getAllUsers = async (req, res) => {
   try {
     if (req.user.isAdmin) {
       const users = await User.find({})
         .select('name email phoneNumber verified isAdmin _id avatar.public_id')
         .exec();

       if (users.length === 0) {
         return res.status(404).json({ message: 'No users found.' });
       }

       const formattedUsers = users.map(user => ({
        name: user.name,
        email: user.email,
        phoneNumber: user.phoneNumber,
        verified: user.verified,
        isAdmin: user.isAdmin,
        id: user._id,
        avatarPublicUrl: user.avatar.public_id,
      }));

       return res.status(200).json(formattedUsers);
     } else {
       return res.status(403).send("You do not have permission to access the list of all users.");
     }
   } catch (error) {
     console.error(error);
     return res.status(500).json({ message: 'Internal Server Error' });
   }
 };


//d. delete user profile

  module.exports.deleteUser = async (req, res, next) => {
    try {
     const userId = req.params.id;

     // Check if the user exists
     const user = await User.findById(userId);

     if (!user) {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }

    if (user.avatar && user.avatar.public_id) {
        const imageId = user.avatar.public_id;

      // Delete the image CL
       await cloudinary.uploader.destroy(imageId);
     }

      // Delete the user from the db
      await User.findByIdAndRemove(userId);

      return res.status(200).json({
       success: true,
       message: "User deleted successfully",
     });
    
    } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Internal Server Error",
    });
   }
  };


//e. Function to grant admin privileges
  module.exports.grantAdminPrivileges = async (req, res) => {
   try {
    const { userId } = req.params;

    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({
        success: false,
        message: 'User not found',
      });
    }
    if (!req.user.isAdmin) {
      return res.status(403).json({
        success: false,
        message: "You don't have permission to grant admin privileges.",
      });
    }

    user.isAdmin = true;
    await user.save();
    return res.status(200).json({
      success: true,
      message: 'Admin privileges granted successfully.',
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: 'Internal Server Error',
    });
  }
 };








