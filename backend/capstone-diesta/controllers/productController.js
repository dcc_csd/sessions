//[section] Dependencies and modules

const {Product, validate} = require('../models/Product');
// const Product = require('../models/Product');
const Token = require("../models/token");
const sendEmail = require("../utils/email");
const crypto = require("crypto");
// const { User, validate } = require("../models/User");
const User = require("../models/User");

const auth = require('../utils/auth');
const cloudinary = require('../utils/cloudinary');
const multer = require("../utils/multer");


const upload = multer.single("images");
const fs = require('fs');


const bcrypt = require('bcrypt');
const {verify, verifyAdmin} = require('../utils/auth');
const jwt = require('jsonwebtoken');



// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//        cb(null, "./uploads"); 
//   },
//   filename: (req, file, cb) => {
//     cb(null, file.originalname);
//   },
// });

// const upload = multer({ storage: storage });


module.exports.registerProduct = async (req, res) => {
  try {

  console.log('Received request body:', req.body);
  console.log('Received file:', req.file);


    const existingProduct = await Product.findOne({
      name: req.body.name,
      codeNo: req.body.codeNo,
    });

    if (existingProduct) {
      return res.status(400).json({
        message: 'A product with similar details already exists. Re-check your product details!',
      });
    }
    const {path}  = req.file
    const result = await cloudinary.uploader.upload(path);

    const newProduct = new Product({
      codeNo: req.body.codeNo,
      name: req.body.name,
      description: req.body.description,
      category: req.body.category,
      price: req.body.price,
      discountPrice: req.body.discountPrice,
      stockQty: req.body.stockQty,
      images: {
        public_id: result.public_id,
        url: result.secure_url,
      },
    });

    const savedProduct = await newProduct.save();
    fs.unlinkSync(req.file.path);


    return res.status(200).json(savedProduct);
    } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
    }
  };



//1. registration of product details

  module.exports.regProduct = async (req, res) => {
  try {
    const existingProduct = await Product.findOne({
      name: req.body.name,
      codeNo: req.body.codeNo,
    });

    if (existingProduct) {
      return res.status(400).json({
        message: 'A product with similar details already exists. Re-check your product details!',
      });
    }

    // Create a product document without the image
    const newProduct = new Product({
      codeNo: req.body.codeNo,
      name: req.body.name,
      description: req.body.description,
      category: req.body.category,
      price: req.body.price,
      discountPrice: req.body.discountPrice,
      stockQty: req.body.stockQty,

    });

    const savedProduct = await newProduct.save();
    return res.status(200).json(savedProduct);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
 };


 // 1b uploading of images
 module.exports.uploadImage = async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).json({ message: 'No image file provided.' });
    }

    // Upload the image to Cloudinary
    cloudinary.uploader.upload(req.file.path, async (error, result) => {
      if (result && result.secure_url) {
        // Assuming you have an existing product
        const productId = req.params.productId; // You should get the product ID from your route

        // Find the product by ID
        const existingProduct = await Product.findById(productId);

        if (!existingProduct) {
          return res.status(404).json({ message: 'Product not found.' });
        }

        // Add the image data to the product schema
        existingProduct.images.push({
          public_id: result.public_id,
          url: result.secure_url,
        });

        // Save the updated product
        const updatedProduct = await existingProduct.save();

        res.status(200).json({ imageURL: result.secure_url, product: updatedProduct });
      } else {
        console.error(error);
        res.status(500).json({ message: 'Image upload failed.' });
      }
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};




 module.exports.upload = async (req, res) => {
  try {

       const {path} = req.file
       const result = await cloudinary.uploader.upload(path)
       const image = new Image ({

           productId:req.product.id


       })
  } catch(error){
    console.log(error);

  }

};


//2. Get all products (archive and active products)
  // module.exports.getAllProduct = async (req, res) => {
  // try {
  //   if (req.user.isAdmin) {
  //     const allProducts = await Product.find({});
  //     const activeProducts = allProducts.filter(product => product.isActive);
  //     const archivedProducts = allProducts.filter(product => !product.isActive);
  //     const totalProducts = allProducts.length;

  //     return res.status(200).json({
  //       activeProductCount: activeProducts.length,
  //       archivedProductCount: archivedProducts.length,
  //       totalProductCount: totalProducts,
  //       products: allProducts,
  //     });
  //   } else {
  //     return res.status(403).send("You do not have permission to access the list of all products.");
  //   }
  //  } catch (error) {
  //   console.error(error);
  //   return res.status(500).json({ message: 'Internal Server Error' });
  //   }
  // };


//get all products
module.exports.getAllProduct = (req, res) => {
  Product.find({}).then(result => {
    if (result.length === 0) {
      return res.send("There are no products in the DB.");
    } else {
      // Extract the image data from the result and add them to an array
      const productWithImages = result.map(product => {
        return {
          _id: product._id,
          name: product.name,
          codeNo: product.codeNo,
          description: product.description,
          slug: product.slug,
          category: product.category,
          price: product.price,
          discountPrice: product.discountPrice,
          stockQty: product.stockQty,
          brand: product.brand,
          isActive: product.isActive,

        
        
          images: product.images, // Assuming this field contains the image data
        };
      });

      return res.json(productWithImages);
    }
  }).catch(err => {
    return res.status(500).json({ error: "An error occurred while fetching products." });
  });
}




// //3.Retrieve specific product using ID
   module.exports.getProductId = (req, res) => {
    	return Product.findById(req.params.productId).then(result =>{
				if(result === 0){
			return res.send("Cannot find product with the provided ID.")
		}else {
			if(result.isActive === "false"){
				return res.send("The product you are trying to access is not available");
			} else{
				return res.send(result);
			}

	    	}
	  })
	 .catch(error => res.send("Please enter a correct product ID"));
   }


//4.Update a specific product by ID

module.exports.updateProductById = async (req, res) => {
  try {
    const productId = req.params.productId;
    const existingProduct = await Product.findById(productId);

    if (!existingProduct) {
      return res.status(404).json({
        message: "Product not found.",
      });
    }

    // Update the product fields based on your requirements
    existingProduct.codeNo = req.body.codeNo;
    existingProduct.name = req.body.name;
    existingProduct.description = req.body.description;
    existingProduct.category = req.body.category;
    existingProduct.slug = req.body.slug;
    existingProduct.price = req.body.price;
    existingProduct.discountPrice = req.body.discountPrice;
    existingProduct.stockQty = req.body.stockQty;
    existingProduct.brand = req.body.brand;
    existingProduct.isActive = req.body.isActive;
    existingProduct.isFeatured = req.body.isFeatured;
    existingProduct.isSale = req.body.isSale;


    if (req.file) {
     
      const oldImagePublicId = existingProduct.images[0].public_id;
  
      const result = await cloudinary.uploader.upload(req.file.path);
     
      existingProduct.images[0] = {
        public_id: result.public_id,
        url: result.secure_url,
      };
  
      if (oldImagePublicId) {
        await cloudinary.uploader.destroy(oldImagePublicId);
      }
    }
   
    const updatedProduct = await existingProduct.save();

    return res.status(200).json(updatedProduct);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

// module.exports.updateProductById = async (req, res) => {
//   try {
//     const productId = req.params.productId;
//     const existingProduct = await Product.findById(productId);

//     if (!existingProduct) {
//       return res.status(404).json({
//         message: "Product not found.",
//       });
//     }

//     // Update the product fields based on your requirements
//     existingProduct.codeNo = req.body.codeNo;
//     existingProduct.name = req.body.name;
//     existingProduct.description = req.body.description;
//     existingProduct.category = req.body.category;
//     existingProduct.price = req.body.price;
//     existingProduct.discountPrice = req.body.discountPrice;
//     existingProduct.stockQty = req.body.stockQty;

//     // Save the updated product (without changing the image)
//     const updatedProduct = await existingProduct.save();

//     return res.status(200).json(updatedProduct);
//   } catch (error) {
//     console.error(error);
//     return res.status(500).json({ message: "Internal Server Error" });
//   }
// };


// //5. Archive specific Product

//   module.exports.archiveProduct = (req, res) => {
//   let archivedProduct = {
//     isActive: false,
//     archivedDate: new Date() 
//   };

//   Product.findByIdAndUpdate(req.params.productId, archivedProduct)
//     .then((product) => {
//       if (!product) {
//         return res.status(404).send("Product not found.");
//       } else {
//         return res.status(200).send("Product archived successfully.");
//       }
//     })
//     .catch((error) => res.status(500).send(error));
//   };


// //6. Re-activate Products
//   module.exports.reactivateProduct = async (req, res) => {
//   try {
//     const productId = req.params.productId;
//     const existingProduct = await Product.findById(productId);

//     if (!existingProduct) {
//       return res.status(404).send("Product not found.");
//     }
    
//     if (!existingProduct.isActive && existingProduct.archivedDate) {
    
//       existingProduct.isActive = true;
//       const archivedDate = existingProduct.archivedDate; 
    
//       existingProduct.archivedDate = null;
    
//       const updatedProduct = await existingProduct.save();

//       return res.status(200).send(`The product was archived on ${archivedDate}. The product was reactivaed successfully`);

//     } else if (existingProduct.isActive) {
//       return res.status(400).send("Product is already active.");

//     } else {
//       return res.status(400).send("Product has not been archived before.");
//     }

//   } catch (error) {
//     console.error(error);
//     return res.status(500).send("Internal Server Error");
//     }
//   };
  

// //7. Find all Archived Product and include no of Archive products
//   module.exports.findArchivedProducts = (req, res) => {
//   Product.find({ isActive: false })
//     .then((archivedProducts) => {
//       const count = archivedProducts.length;
//       const message = `${count} archived product(s) found.`;

//       return res.status(200).json({ message, archivedProducts });
//     })
//     .catch((error) => res.status(500).send(error));
//   };


// //.8 Retrieve all active product - client
//   module.exports.getAllActiveProducts = (req, res) => {
//   Product.find({ isActive: true })
//     .then((activeProducts) => {
//       if (activeProducts.length === 0) {
//         return res.status(404).send("No active products found.");
//       } else {
//         const result = activeProducts.map((product) => {
//           const productCopy = { ...product._doc };
          
          
//           if (!req.user || !req.user.isAdmin) {
//               delete productCopy.archivedDate;
//           }
        
//           return productCopy;
//         });

//         return res.status(200).json(result);
//       }
//     })
//     .catch((error) => res.status(500).send(error));
//  };


// //.9 upload multiple images to a specific product by ID
//  module.exports.addImagesToProduct = async (req, res) => {
//   try {
//     const productId = req.params.productId;
//     const existingProduct = await Product.findById(productId);

//     if (!existingProduct) {
//       return res.status(404).json({
//         message: "Product not found.",
//       });
//     }

//     if (req.files && req.files.length > 0) {
//       const newImages = [];

//       for (const file of req.files) {
//         const result = await cloudinary.uploader.upload(file.path);
//         newImages.push({
//           public_id: result.public_id,
//           url: result.secure_url,
//         });
//       }

//       // Add the new images to the product
//       existingProduct.images = [...existingProduct.images, ...newImages];

//       const updatedProduct = await existingProduct.save();

//       return res.status(200).json({
//         message: "Multiple images uploaded successfully.",
//         product: updatedProduct,
//       });
//     } else {
//       return res.status(400).json({
//         message: "No images were uploaded.",
//       });
//     }
//   } catch (error) {
//     console.error(error);
//     return res.status(500).json({ message: "Internal Server Error" });
//    }
//  };


 
