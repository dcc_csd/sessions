// [ SECTION ] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

const userRoutes = require('./routes/user'); // Allows access to routes defined within our application

const orderRoutes = require('./routes/orderRoute');
const categoryRoutes = require('./routes/category');
const productRoutes = require('./routes/productRoute');
const cartRoute = require('./routes/cart');
const courseRoute = require('./routes/course');




// [ SECTION ] Environment Setup
const port = 4000;

// [ SECTION ] Server Setup
const app = express();

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: true, limit:'50mb' }));

// Allows all resources to acces our backend application;
app.use(cors()); // CORS is a security feature implemented by web browsers to control requests to a different domain than the one the website came from.

// [ SECTION ] Database Connection
mongoose.connect('mongodb+srv://admin:admin123@cluster0.ztpawdb.mongodb.net/S43-48?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [ SECTION ] Backend Routes
app.use('/users', userRoutes);

// back end routes for course
app.use('/order', orderRoutes);
app.use('/product',productRoutes);
app.use('/category', categoryRoutes);
app.use('/carts', cartRoute);
app.use('/course', courseRoute);



// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${ process.env.PORT || port }`);
	});
};

module.exports = app;