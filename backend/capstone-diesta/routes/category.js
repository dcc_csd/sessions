const express = require('express');
const router = express.Router();

const categoryController = require('../controllers/category.js');



const { verify, verifyAdmin } = require('../utils/auth');


//1.  Create a category
      router.post('/', verify, verifyAdmin, categoryController.addCategory);



//2.  Get List of Category
      router.get('/', verify, verifyAdmin, categoryController.getCategories);



//3. Get Category by ID
      router.get('/:id',verify, verifyAdmin, categoryController.getCategoryById);



//4. Update an existing category
    
     router.put('/update/:id', categoryController.updateCategory);


//5. Delete a Category
     router.delete('/delete/:id', verify, verifyAdmin, categoryController.deleteCategory); 


module.exports = router;
