const express = require('express');
const User = require('../models/User');
const Token = require("../models/token");
const sendEmail = require("../utils/email");

const cloudinary = require ('../utils/cloudinary');
const multer = require("../utils/multer");

const upload = multer.single("avatar");


const auth = require("../utils/auth.js")


const sendMail = require("../utils/sendMail");
const router = express.Router();

const { verify, verifyAdmin } = require('../utils/auth');

const userController = require('../controllers/userController.js');

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const jtw = require('jsonwebtoken');

const nodemailer = require('nodemailer');




//[USER SECTION]
//A. User signup / registration
     router.post("/signup", upload, async (req, res) => {
     const resultFromController = await userController.signupUser(req, req.body);

      if (!resultFromController.success) {
      return res.status(400).json(resultFromController);
      }

     return res.status(200).json(resultFromController);
     });


//B. Activation - Verification 
     router.get("/verify/:id/:token", userController.verifyEmail);


//C. User login - authentication
     router.post('/login', userController.loginUser);


//D. Reset user password
     router.put("/update-user-password", verify, userController.resetPassword);



//E. Route to update user details
     router.put('/update-user-profile/:userId', verify, upload, async (req, res) => {
  
     const { userId } = req.params;
     const resultFromController = await userController.updateUserAndAvatar(userId, req.body, req.file);
     if (!resultFromController.success) {
     return res.status(400).json(resultFromController);
     }
     return res.status(200).json(resultFromController);
     });


//[ADMIN SECTION]
   

//A. Get user details
  router.get('/user-info', verify, verifyAdmin, userController.getUser);


//B.Get user details by params id
  router.get('/user-info/:userId', verify, verifyAdmin, userController.getByUserId );



//C.Get All User Info-details
  router.get("/users", verify, verifyAdmin, userController.getAllUsers );


//D. Delete User by ID - params
  router.delete("/delete-user/:id", verify, verifyAdmin, userController.deleteUser);


//E. Route to grant admin privileges
  router.put('/grant-admin/:userId', verify, verifyAdmin, userController.grantAdminPrivileges);


module.exports = router;








module.exports = router;

