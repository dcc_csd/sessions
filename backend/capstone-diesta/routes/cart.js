const Cart = require("../models/Cart");
const router = require("express").Router();
const {verify, verifyAdmin} = require('../utils/auth');
const Product = require('../models/Product'); 


//CREATE

router.post("/", verify, async (req, res) => {
  const newCart = new Cart(req.body);

  try {
    const savedCart = await newCart.save();
    res.status(200).json(savedCart);
  } catch (err) {
    res.status(500).json(err);
  }
});

//UPDATE
router.put("/:id", verify, async (req, res) => {
  try {
    const updatedCart = await Cart.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedCart);
  } catch (err) {
    res.status(500).json(err);
  }
});

//DELETE
router.delete("/:id", verify, async (req, res) => {
  try {
    await Cart.findByIdAndDelete(req.params.id);
    res.status(200).json("Cart has been deleted...");
  } catch (err) {
    res.status(500).json(err);
  }
});






//GET USER CART
router.get("/find/:userId", verify, async (req, res) => {
  try {
    const cart = await Cart.find({ userId: req.params.userId });
    res.status(200).json(cart);
  } catch (err) {
    res.status(500).json(err);
  }
});




// Route to display the user's cart
router.get('/:id', verify, async (req, res) => {
  if (!req.user) {

    return res.redirect('/login'); 
  }

  try {
    const userId = req.user.id; 
    
   
    const cartItems = await Cart.find({ userId }).populate('productId');

 
    res.render('cart', { cartItems });
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

module.exports = router;



//GET ALL

router.get("/", verify, async (req, res) => {
  try {
    const carts = await Cart.find();
    res.status(200).json(carts);
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;
