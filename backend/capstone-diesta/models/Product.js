const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const productSchema = new mongoose.Schema({

 name: {
    type: String,
    required: [true, "Please enter your product name!"],
  },

  codeNo: {
    type: String,
    
  },

  description: {
    type: String,
    required: [true, "Please enter your product description!"],
  },
  
  slug: {
    type: String,
    
  },

  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref:'Category',
    
  },

  isActive: {
    type: Boolean,
    default: true
  },

  isFeatured: {
    type: Boolean,
    default: false
  },

  isSale: {
    type: Boolean,
    default: false
  },


  price: {
    type: Number,
    required: true,
  },

  discountPrice: {
    type: Number ,
    default: 0
  },
  stockQty: {
    type: Number,
    default: 0,
    max:1000,  

  },

  images: [
    {
      public_id: {
        type: String,
        required: false,
      },
      url: {
        type: String,
        required: false,
      },
    },
  ],
  reviews: [
    {
      user: {
        type: Object,
      },
      rating: {
        type: Number,
      },
      comment: {
        type: String,
      },
      productId: {
        type: String,
      },
      createdAt:{
        type: Date,
        default: Date.now(),
      }
    },
  ],
  ratings: {
    type: Number,
  },
  
  brand: {
    type: String
    
  },
  sold_out: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  archivedDate: {
    type: Date,
    default: null,
  },

});

// productSchema.virtual('id').get(function () {
//    return this._id.toHexString();
// });

// productSchema.set('toJSON', {virtuals: true,});

module.exports = mongoose.model("Product", productSchema);


const Product = mongoose.model("Product", productSchema);

const validate = (product) => {
  const schema = Joi.object({
    name: Joi.string(),
    price: Joi.number(),

  });

  return schema.validate(product);
};

module.exports = { Product, validate };



