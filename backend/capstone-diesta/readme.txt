dependecies
const Token = require("../models/token");
const sendEmail = require("../utils/email");
const crypto = require("crypto");
const { User, validate } = require("../models/User");
const userController = require('../controllers/userController');
const auth = require('../utils/auth');
const cloudinary = require ('../utils/cloudinary');
const multer = require("../utils/multer");
const upload = multer.single("avatar");
const fs = require('fs');
const bcrypt = require('bcrypt');
const {verify, verifyAdmin} = require('../utils/auth');
const jwt = require('jsonwebtoken');

schema
 1. User
 2. Product
 3. Order
 4. category
 5. OrderItem
 6. token

 Routes and Controller
 1.category
 2.Order Routes/Controller
 3.Product Routes/Controller
 4.User Routes/Controller

 Date = 9/21/2023 - C.DIESTA - ECOMMERCE-BOOKING-CAPS2