const cloudinary = require('cloudinary').v2; // Import the Cloudinary library

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

async function cloudinaryUpload(base64EncodedImage) {
  try {
    const result = await cloudinary.uploader.upload(base64EncodedImage, {
      upload_preset: 'your_upload_preset',
    });

    return result.url; 
  } catch (error) {
    console.error('Error uploading image to Cloudinary:', error);
    throw error;
  }
}

module.exports = {
  cloudinaryUpload,
};
