//SECTION JS Synchronous vs Asynchronous
//By default JS is Synchronous -> only on estatement can be executed at a time

//JS Code Reading
//Top to Bottom, Left to Right

console.log("Hello World!");


console.log("Goodbye!");

// When certain statements take a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code  "blocking"

console.log("Hello World!"); //1

// for(let i = 0; 1 <= 1500; i++){ //3
// 	console.log(i);
// }

console.log("Hello Again!"); //2


// SECTION Getting all post
// fetch("URL");

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status));

//json() will convert response object/promise into JSON format
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then(json => console.log(json));

//"async" and "await" keyword is another approach that can be used to achieve asychronous code

async function fetchData(){
	let result = await  fetch("https://jsonplaceholder.typicode.com/posts");

	console.log(result);

	console.log(typeof result);

    //we can accesd the content of the "response" by directly accessing its body property

	console.log(result.body);

	let json = result.json();
	console.log(json);
}
fetchData();

//SECTION Getting a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response => response.json())
.then(json => console.log(json));

//SECTION Creating a post
/*
SYNTAX:
fetch("URL", options)
.then(response ->{})
.then(response ->{})

*/
fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST", 
	headers: {"Content-Type" : "application/json"}, 
	body: JSON.stringify({
		title: "New Post", 
		body: "Hello World!",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

//Updating a post 

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT", 
	headers: {"Content-Type" : "application/json"}, 
	body: JSON.stringify({
		title: "Updated Post", 
		body: "Hello Again!",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

//Deleting a post 

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE", 
})
//.then((res) => res.send("Data deleted"));

//
