// CRUD Operations (MongoDB)

// CRUD Operationsa re the heart of any backend app.

//Mastering CRUD Operations is essential for any developers

/*

C - Create / Insert
R - Retrieve / Read
U - Update
D - Delete

*/

//[SECTION] Creating documents

/*
Syntax:

db.users.insertOne({object})


SQL:

INSERT INTO table_name (column1, column 2, column3....)
VALUES (value1, value2, value3,....);

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789", 
		email: "janedoe@gmail.com"
	}, 
	courses: ["CSS", "JS", "Python"], 
	department: "none"
});

db.users.insertOne({
	firstName: "Cherry",
	lastName: "Diesta",
	age: 25,
	contact: {
		phone: "09123456789", 
		email: "csd0821@gmail.com"
	}, 
	courses: ["CSS", "JS", "Python"], 
	department: "none"
});

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 25,
	contact: {
		phone: "00000", 
		email: "test@gmail.com"
	}, 
	courses: [], 
	department: "none"
});


/*

SYNTAX

db.users.insertMany([{
	
})
*/

db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 25,
	contact: {
		phone: "09123456789", 
		email: "stehpehnhawking@gmail.com"
	}, 
	courses: ["PHP", "React", "Python"], 
	department: "none"
},
{
	firstName: "Neil;",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789", 
		email: "neilarmstrong@gmail.com"
	}, 
	courses: ["React", "Laravel", "SAS"], 
	department: "none"
} 
]);

//[SECTION] - Read / Retrieve

/* 

SYNTAX:

db.users.findOne();
db.users.findOne({field: value});



*/

db.users.findOne();

db.users.findOne({firstName: "Stephen"});

/* 

SYNTAX:

db.users.findMany();
db.users.findMany({field: value});
update

db.users.find({field: value});

*/

db.users.findMany();

db.users.find({department: "none"});

//multiple criteria

db.users.find({department: "none",  age: 82});

/*
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 21,
	contact: {
		phone: "0000", 
		email: "test@gmail.com"
	}, 
	courses: [], 
	department: "none"
});
*/

//[SECTION] Updating a data

