// console.log("Hello World");

//Objective 1
//Add code here
//Note: function name is numberLooper

let num = Number(prompt("Give me a number."));
 console.log("The number you provided is " + num);

 // Start of Loop Checker
 for (let i = num; i >= 0; i--) {
    //Divisible by 10
    if (i % 10 == 0 && i!== 50){
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }
    
    //Divisible by 5
    else if (i % 5 == 0 && i!== 50){
        console.log(i);
    }

    //Terminating Program at 50 and less value
    if (i <= 50) {
        console.log("The current value is at 50. Terminating the loop.");
        break;
    }

 }  // End of Loop Checker


let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

for(let i = 0; i < string.length; i++){
    if(string[i] === "a" ||
        string[i] === "e" ||
        string[i] === "i" ||
        string[i] === "o" ||
        string[i] === "u"){
        continue
    } else{
        filteredString = string[i] + filteredString;
    }
}

console.log(filteredString)

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {
        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}