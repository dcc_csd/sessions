// console.log("Hello World!");

//While loop
/*

Syntax:

while (condition/expression) {
  code block
}

*/

let count = 5;

while (count !== 0) {
  console.log("While: " + count);
  count--;
}

/* WHILE EXAMPLE FLOW
let count = 5;
while (count !== 0) { // TRUE SO PROCEED INSIDE WHILE LOOP
  console.log("While: " + count);
  count--;
}

let count = 4;
while (count !== 0) { // TRUE SO PROCEED INSIDE WHILE LOOP
  console.log("While: " + count);
  count--;
}

let count = 3;
while (count !== 0) { // TRUE SO PROCEED INSIDE WHILE LOOP
  console.log("While: " + count);
  count--;
}

let count = 2;
while (count !== 0) { // TRUE SO PROCEED INSIDE WHILE LOOP
  console.log("While: " + count);
  count--;
}

let count = 1;
while (count !== 0) { // TRUE SO PROCEED INSIDE WHILE LOOP
  console.log("While: " + count);
  count--;
}

let count = 0;
while (count !== 0) { // FALSE SO END OF LOOP HERE
  console.log("While: " + count);
  count--;
}
/*



// Do-While Loop
/*

do {
  code
} while (condition/expression)

*/


// let number = Number(prompt("Give me a number."));

// do {
//   console.log("Do-While: " + number);
//   number++;
// } while (number <= 10);



// For Loop
/*

for (initialization; condition; iterator) {
  code
}

*/


// for (let num = 0; num <= 20; num++){
//   console.log(num);
// }


// let myString = "alex";
// console.log(myString.length);
// // console.log(myString[0]);
// // console.log(myString[1]);
// // console.log(myString[2]);
// // console.log(myString[3]);

// for (let x = 0; x < myString.length; x++){
//   console.log(myString[x]);
// }

/* FOR EXAMPLE SHOW
for (let x = 0; 0 < 4; x++){
  console.log(myString[0]);
}

for (let x = 0; 1 < 4; x++){
  console.log(myString[1]);
}

for (let x = 0; 2 < 4; x++){
  console.log(myString[2]);
}

for (let x = 0; 3 < 4; x++){
  console.log(myString[3]);
}

for (let x = 0; 4 < 4; x++){ // FALSE CONDITION => LOOP ENDS HERE
  console.log(myString[4]);
}
*/


// let myName = "AlEx";

// for (let i = 0; i<myName.length; i++){
//   //console.log(myName[i].toLowerCase()); // Outputs lowercase alex

//   if (myName[i].toLowerCase() == "a" ||
//       myName[i].toLowerCase() == "e" ||
//       myName[i].toLowerCase() == "i" ||
//       myName[i].toLowerCase() == "o" ||
//       myName[i].toLowerCase() == "u"){

//       console.log("VOWEL HERE");
//   } else {
//       console.log(myName[i]);
//   }

// }

// Code below outputs AlEx
// for (let i = 0; i<myName.length; i++){
//   console.log(myName[i]);
// }


for (let count = 0; count <= 100; count++){
  if (count % 2 == 0){ //Disregards even numbers
    continue;
  }

  // else if (count > 10){ //Cut off na sa 10 na missing kasi kasama siya sa if structure sa itaas
  //   break;
  // }

  console.log("Continue and Break: " + count);

  if (count > 10){ //Cut off na sa output of 11 kasi magkaibang structure ang ifs 
    break;
  }
  
}


// let name = "alexandro";
// for(let i = 0; i < name.length; i++){
//   console.log(name[i]);

//   if (name[i].toLowerCase()=="a"){
//     console.log("Continue to the next iteration");
//     continue;
//   }

//   // console.log(name[i]); // Consoling here does not display "a"

//   if (name[i].toLowerCase()=="d"){
//     break;
//   }


// }

