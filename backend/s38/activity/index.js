let http = require('http');

let port = 4000;
let app= http.createServer(function (request, response){

// (1)Front End --> GET Method
// (2)Back End --> Read Method --> .find() 
// (3)DB 

// (1)Front End --> POST Method
// (2)Back End --> CREATE Method --> .Insert() 
// (3)DB

// (1)Front End --> PUT Method
// (2)Back End -->  UPDATE Method --> .Update() 
// (3)DB


// (1)Front End --> DELETE Method
// (2)Back End --> DELETE Method --> .Delete() 
// (3)DB




 if(request.url == "/" && request.method =="GET"){
    response.writeHead(200, {'Content-Type' : 'text/plain'});
    response.end('Welcome to Booking System')
 } 
 if(request.url == "/profile" && request.method == "GET"){
   	   response.writeHead(200, {'Content-Type' : 'text/plain'});
   	   response.end('Welcome to your profile!')
 }
 if(request.url == "/courses" && request.method == "GET"){
         response.writeHead(200, {'Content-Type' : 'text/plain'});
         response.end('Here\'s our courses available')
 }
 if(request.url == "/addCourse" && request.method == "POST"){
         response.writeHead(200, {'Content-Type' : 'text/plain'});
         response.end('Add a course to our resources')
 }
 if(request.url == "/updateCourse" && request.method == "PUT"){
         response.writeHead(200, {'Content-Type' : 'text/plain'});
         response.end('Update a course to our resources')
 }
if(request.url == "/archiveCourse" && request.method == "DELETE"){
         response.writeHead(200, {'Content-Type' : 'text/plain'});
         response.end('Archive courses to our resources')
 }



})




app.listen(port, () => console.log('Server is running at localhost:4000'));
