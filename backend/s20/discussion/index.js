// console.log("Hello World!");
console.log("Hello Wordl!");

//  [SECTION] Arithmetic Operators

let x = 50;
let y = 10;

let sum=x+y;
console.log("Result from addition operator:" +sum);

let difference=x-y;
console.log("Result from subtraction operator:" +difference);

let product=x*y;
console.log("Result from multiplication operator:" +product);

let quotient=x/y;
console.log("Result from division operator:" +quotient);


// modulo % 

let remainder=x%y;
console.log("Result from modulo operator:" +remainder);

// [SECTION] Assignement Operator
// Basic Assignment Operator (=)

let assignmentNumber=8;

assignmentNumber = assignmentNumber + 2;
console.log("addition assignment: " + assignmentNumber);

// Shorthand Method for Assignment Method
assignmentNumber +=2;
console.log("addition assignment: " + assignmentNumber);

assignmentNumber -=2;
console.log("subtraction assignment: " + assignmentNumber);

assignmentNumber *=2;
console.log("multiplication assignment: " + assignmentNumber);

assignmentNumber /=2;
console.log("division assignment: " + assignmentNumber);

// Multiple Operaotrs and Parenthesis
//    - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            // // - The operations were done in the following order:
            //     1. 3 * 4 = 12
            //     2. 12 / 5 = 2.4
            //     3. 1 + 2 = 3
            //     4. 3 - 2.4 = 0.


let mdas =1+2-3*4/5;
console.log("Results fron mdas: " +mdas);




let pemdas =1+ (2-3)*(4/5);
console.log("Results fron pemdas: " +pemdas);

// Increment and Decrement
// Incrementation -> ++
// Decrementation -> --

let z=1;
// pre-increment
let increment = ++z
console.log("pre-increment: "+ increment);
console.log("pre-increment: "+ z );


// post-increment
increment =z++;
console.log("post-increment: "+ increment);


let myNum1 =1;
let myNum2 =1;

let preIncrement = ++myNum1;
preIncrement = ++myNum1;
 console.log(preIncrement);


let postIncrement = ++myNum2;
postIncrement = ++myNum2;
 console.log(postIncrement);

 let myNumA =5;
 let myNumB =5;

 let preDecrement = --myNumA;
 preDecrement = --myNumA;
 console.log(preDecrement);

 let postDecrement =myNumB--;
 postDecrement = myNumB--;
 console.log(postDecrement);

 // [section] Type coercion
 // Automatic or implicit conversion of value from one

 let myNumC ="10";
 let myNumD =12;

  let coercion = myNumC + myNumD;
  console.log(coercion);
  console.log(typeof coercion);


  let myNum5 = true +1;
  console.log(myNum5);

    let myNum6 = false +1;
  console.log(myNum6);

// [section] comparison Operator
  let juan ="juan";

  // equality operator()
  console.log(juan == "juan");
  console.log(1 ==2);
  console.log(false==0);

// Inequality Operator (!=)
// ! -> NOT 

  console.log(juan != "juan");
  console.log(1 !=2);
  console.log(false!=0);

// Strict Equality Operator (===)
//Same data type, content or value
  console.log(1===1);
  console.log(false===0);

// Strict Inequality Operator (!==)
  console.log(1!==1);
  console.log(false!==0);

// [section] relational Operator
/* comparison operators whether one value
 is greater or less than to the other value.*/

  let a = 40;
  let b = 65;

  let example1 = a > b;
  console.log(example1);
  
  let example2 = a < b;
  console.log(example2);

  let example3 = a >= b;
  console.log(example3);

  let example4 = a <= b;
  console.log(example4);

// [section] logical Operator

// Logical "AND" (&& - Double Ampersand)

  let isLegalAge = true;
  let isRegistered = true;

// All conditions should be true
  let allRequirementsMet = isLegalAge && isRegistered;
  console.log("Logical And: " + allRequirementsMet);

// Logical "OR" (|| - Double Pipe)

  let isLegalAge2 = false;
  let isRegistered2 = false;

  let someRequirementsMet = isLegalAge2 || isRegistered2;
  console.log(someRequirementsMet);

// Logical Not (! - Exclamation Point)

  let isRegistered3 = false;

  let someRequirementsNotMet = !isRegistered3;
  console.log("Result from logical NOT: " + someRequirementsNotMet);

