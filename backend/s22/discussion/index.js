// console.log("Hello World!");
// Parameters and Arguments
function printName(name){
  console.log("My name is " + name);
}
// Calling or Invoking our Function "printName"
printName()

// Arguments -->Invoke
printName("Juana");
printName("Romenick");

let sampleVariable = "John"; 

printName(sampleVariable);


// Check Divisibility

function checkDivisibility(num){
  let remainder = num % 8;
  console.log("The remainder of  " + num + " divided by 8 is: " + remainder);
  let isDivisibleBy8 = remainder === 0; 
  console.log("Is " + num + " divisible by 8?");
  console.log(isDivisibleBy8);
}

//Invocation
checkDivisibility(64);



//Functions as Arguments
function argumentFunction() {
 console.log("This fuction was passed as an argument.")
}

function invokeFunction(argument){
  argumentFunction();
}

invokeFunction();


//Multiple Parameters
function createFullName(firstName, middleName, LastName){
  console.log(firstName +  "  "  + middleName +  "  " + LastName);
}  

//Multiple Arguments is an Invocation
createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "Hello");





//Function with Alert Message
function showSampleAlert(){
  alert("Hello User!");
}

// showSampleAlert();


// console.log("Testing");


//prompt() pwedeng mag enter ng valur
// let samplePrompt = prompt("Enter Your Name");
// console.log("Hello,  " + samplePrompt);

//Function with prompts

function printWelcomeMessage(){
let firstName = prompt(" Enter your First Name");
let LastName = prompt(" Enter your Last Name");

console.log("Hello, " + firstName + " " + LastName + "!");
console.log("Welcome to my page!");

}
printWelcomeMessage();


function greeting(){
  return "Hello, This is the return statement";
}


let greetingFunction =  greeting();
console.log (greetingFunction);


function greeting(){
  console.log("hi this is me");
  return "Hello, This is the return statement";

  //code below return statement will never work
  console.log("Hello this is me");
}

// let greetingFunction = greeting();
console.log (greetingFunction);

console.log(greeting());



