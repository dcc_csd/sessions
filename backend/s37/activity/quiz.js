// 1. What directive is used by Node.js in loading the modules it needs?
//Use the "require" directive to load Node.jS


// 2. What Node.js module contains a method for server creation?
 //"http module" contains the components needed to create a server.
//sample : let http = require("http"); or const http = require("http");


// 3. What is the method of the http object responsible for creating a server using Node.js?
// "createServer()" 
//sample: http.createServer(function(request, response){}


// 4. What method of the response object allows us to set status codes and content types?
    //"writeHead()" 
    //sample: response.writeHead (200, {"Content-Type" : "text/plain"});


// 5. Where will console.log() output its contents when run in Node.js?
// Output will be shown in the terminal  
//sample: console.log("Server is running at localhost:4000");


// 6. What property of the request object contains the address's endpoint?
//response.end("Hello World!");
