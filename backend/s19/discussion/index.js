console.log("halluw");

// [section] Syntax, statements and comments 
	//JS Statments usually end with semicolo(;)

	//Coments:

	//There are two types of comments:
		//1. The single-line comment denoted by //, double slash
    	//2. The multiple-line comment denoted by //**/, double and asterisk in the middle

// [section] Variables
 // It is used to contain data
 //any information that is used by an application is stored in what we call a 'memory stored in what we ca;; a "memory"
 
 //Declaring Variables

 //Declaring Variables - tells our devices that a variable name is created.

 let myVariableNumber; // A naming conversation
 console.log("myVariableNumber");
 console.log(myVariableNumber);

 //let myVariableNumber
 //let my-variable-number
 //let my_variable_number

 //Syntax
   //let/const/var variableName - number

 //Declaring and Initializing Variables
  let productName ="desktop computer";
  console.log(productName);	

let productPrice = 18999;
console.log(productPrice);


let productPrice1 = "18999";
console.log(productPrice1);

//const, impossible to reassign
const interest = 3.539; 

//Reassigning of variables value

productName = "Laptop"
console.log(productName);

//interest =4.239; //will return an error
//console.log(interest);

//reassigning variable vs initializing variable

//declare the variables - asigning output 
myVariableNumber =2023;
console.log(myVariableNumber);


//multiple variable declarations  - 2 value ang lalabas
let productCode ='DC017';
const productBrand ='Dell';

console.log(productCode,
	productBrand);

//[SECTION] Data Types
  //Strings

   let country = "Philippines";
   let province = 'Metro Manila';

   //Concatenation Strings - pagsamahin
   let fullAddress = province + " , " + country;
   console.log(fullAddress);
    //console.log("Metro Manila, Philippines");

   let greeting ="I live in the " + country
   console.log(greeting);

   let mailAddress = 'Metro Manila\n\nPhilippines'; //with space
       console.log(mailAddress)

   let message ="JOhn's employees went home early";
       console.log(message)

   //reassing the value
       message ='JOhn\'s employees went home early again!';
       console.log(message);


       //Numbers
       let headcount = 26;
       console.log(headcount);

       //Decimal Numbers/fractions

       let grade = 98.7;
       console .log(grade);


   //Exponential Notation
       let planetDistance = 2e10;
       console.log(planetDistance);


       //Combining text and strings
       console.log("john's grade last quarter is "+grade);

       //Boolean
       // returns true or false value
       let isMarried = false;
       let isGoodConduct = true;
       console.log("isMarried: " + isMarried);

       console.log("inGoodConduct: "+ isGoodConduct);


//Arrays - data
let grades =[98.7, 92.1, 90.2, 94.6]
console.log(grades);
console.log(grades[0]);
console.log(grades[1]);
console.log(grades[2]);
console.log(grades[3]);
console.log(grades[4]);

//Different data types

let details =["John", "Smith", 32, true];
console.log(details);
console.log(details[0]);
console.log(details[1]);
console.log(details[2]);
console.log(details[3]);
console.log(details[4]);



//Objects curly brace mimics real world detailed data
//Composed of "key/value pair"
let person = {
	fullName: "Juan Dela Cruz", age:45, isMarried: false, 
	contact:["09123456789","09987654321"], 
	address:{houseNumber:"345", city:"Manila"}
     };
	console.log(person); 
	console.log(person.fullName);
	console.log(person.age);
	console.log(person.isMarried);

	//We only use[] to call the index number of the data from the array


	console.log(person.contact[0]);
	console.log(person.contact[1]);
	console.log(person.address.houseNumber); 

	console.log(person.address.city);

	let array =[
		["hey", "hey1", "hey2"],
		["hey", "hey1", "hey2"],
		["hey", "hey1", "hey2"]];

   console.log(array);

   let myGrades = {
   	firdtGrading: 98.7,
   	secondGrading: 92.1,
   	thirdGrading: 90.2,
   	forthGrading: 94.6,
   };

   console.log(myGrades);
   
//type of operator
   // it will determine the type of the data.
   console.log(typeof myGrades);
      console.log(typeof arrays);
         console.log(typeof greeting);


         let spouse =null;
         let myNumber =0;
         let myString = '';

         console.log(spouse);
         console.log(myNumber);
         console.log(myString);

         //Undefined
         let fullName;
         console.log(fullName);




